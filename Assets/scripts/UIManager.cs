using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{  
    public GameObject cam;
    private Vector2 cameraOriginalPos;
    private float camOffsetY = 0;

    public World world;
    public Vector2 reference_res;
    private Vector2 current_res;

    public TMP_Text waterText;
    public TMP_Text nText;
    
    private Vector2 mousePos;
    private Vector2 oldMousePos;

    private Vector2Int selectedTile;

    public GameObject rootTile;
    public GameObject pumpTile;

    private GameObject bldToPlace;

    public GameObject gameOverScreen;
    public TMP_Text lvlReached;

    public GameObject infoBlock;
    public TMP_Text infoText;

    public Building root;
    public Building pump;
    public Building cannon;

    public RectTransform bldSelector;

    public RectTransform rootBtn;
    public RectTransform pumpBtn;
    public RectTransform cannonBtn;

    string infoBlockText = "";
    public GameObject infoBlockMiniBg;
    public GameObject infoBlockBg;

    string lastInfoBlock = "";

    public Animator warningAnim;
    public TMP_Text warningText;

    // Start is called before the first frame update
    void Start()
    {
        current_res = new Vector2(Screen.width, Screen.height);
        updateResolution();
        
        cameraOriginalPos = cam.transform.position;

        changeBldToPlace(rootTile);
        infoBlock.SetActive(false);
    }

    public void showFullInfoBlock() {
        infoBlockMiniBg.SetActive(false);
        infoBlockBg.SetActive(true);
        infoText.text = infoBlockText;
    }
    
    public void showInfoBlock(string thing) {
        showInfoBlock(thing, true);
    }

    public void warn(string warning) {
        warningAnim.Rebind();
        warningAnim.Update(0f);
        warningAnim.gameObject.SetActive(true);
        warningText.text = warning;
    }

    public void showInfoBlock(string thing, bool doFullInfoBlockStuff) {
        lastInfoBlock = thing;
        
        if (doFullInfoBlockStuff) {
            infoBlock.SetActive(true);
            infoBlockMiniBg.SetActive(true);
            infoBlockBg.SetActive(false);
            CancelInvoke("showFullInfoBlock");
        }

        infoBlockText = "";

        switch (thing) {
            case "hp": {
                infoBlockText = string.Format("Upgrade building HP");
                infoBlockText += string.Format("\nCost: {0} Nitrogen", world.upgradeCost);
                infoBlockText += string.Format("\n\nMultiplier: {0:F2}x -> {1:F2}x", world.buildingsHPmult, world.getNextUpgrade("hpMult"));
            } break;

            case "dmg": {
                infoBlockText = string.Format("Upgrade cannon damage");
                infoBlockText += string.Format("\nCost: {0} Nitrogen", world.upgradeCost);
                infoBlockText += string.Format("\n\nFlat bonus: {0:F2} -> {1:F2}", world.buildingsDmgBonus, world.getNextUpgrade("dmgBonus"));
                infoBlockText += string.Format("\nMultiplier: {0:F2}x -> {1:F2}x", world.buildingsDmgMult, world.getNextUpgrade("dmgMult"));
            } break;

            case "root": {
                world.upgradeBuildingAll(root);

                infoBlockText = string.Format("Build root (HP: {0})", root.maxHP);
                infoBlockText += string.Format("\nCosts: {0}N to build", root.nCost);
                infoBlockText += string.Format("\n\nRoots extract water from dark soil.");
                infoBlockText += string.Format("\nNew roots can only be built below previous roots.");
            } break;

            case "pump": {
                world.upgradeBuildingAll(pump);

                infoBlockText = string.Format("Build molecular pump (HP: {0})", pump.maxHP);
                infoBlockText += string.Format("\nCosts: {0}N to build, {1} water/s to operate", pump.nCost, pump.waterNeed);
                infoBlockText += string.Format("\n\nPumps extract Nitrogen from Nitrogen patches.");
                infoBlockText += string.Format("\nPumps need water to operate efficiently.");
            } break;

            case "cannon": {
                world.upgradeBuildingAll(cannon);

                infoBlockText = string.Format("Build cannon (HP: {0}, DMG: {1}, RANGE: {2})", cannon.maxHP, cannon.damage, cannon.maxRange);
                infoBlockText += string.Format("\nCosts: {0}N to build, {1} water per shot", cannon.nCost, cannon.waterNeed);
                infoBlockText += string.Format("\n\nCannons shoot at the closest enemy.");
                infoBlockText += string.Format("\nCannons need water to shoot at max damage.");
            } break;

            case "water": {
                infoBlockText = string.Format("You are making {0} water/s.", compressResourceText(world.waterPerS));
                infoBlockText += string.Format("\n{0} Machines are consuming {1} water/s.", world.waterConsumers.Count, compressResourceText(world.negWaterPerS));
                infoBlockText += string.Format("\n\nCannons require {0} water per shot.", cannon.waterNeed);
                infoBlockText += string.Format("\nPumps are working at {0}% efficiency.", (int) (world.pumpEfficiency * 100));
                infoBlockText += string.Format("\nYou make a baseline of 2 water/s");
            } break;

            case "nitrogen": {
                infoBlockText = string.Format("Your are making {0} Nitrogen/s", compressResourceText(world.nitrogenPerS));
                infoBlockText += string.Format("\n\nPumps extract {0} Nitrogen per ore", pump.extractionSpeed);
                infoBlockText += string.Format("\nYou make a baseline of 2 Nitrogen/s");
            } break;
        }
        
        string summary = infoBlockText;
        
        if (infoBlockText.IndexOf("\n\n") != -1) {
            summary = infoBlockText.Substring(0, infoBlockText.IndexOf("\n\n"));

            if (doFullInfoBlockStuff) {
                Invoke("showFullInfoBlock", 2);
            } else {
                if (infoBlockBg.activeSelf) {
                    summary = infoBlockText;
                }
            }
        }

        infoText.text = summary;
    }
    public void hideInfoBlock() {
        CancelInvoke("showFullInfoBlock");
        infoBlock.SetActive(false);
    }

    public void gameOver() {
        lvlReached.text = "Reached level: " + world.maxBldY;
        gameOverScreen.SetActive(true);
    }

    public void ResetGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Update is called once per frame
    void Update()
    {
        handleCanvasResizing();
        handleMousePositioning();
        handleMouseScrolling();
    }

    string compressResourceText(float val) {
        var neg = "";
        if (val < 0) {
            val = -val;
            neg = "-";
        }

        if (val >= 1000000) {
            return neg + ((float) val / 1000000).ToString("F1") + "m";
        } else if (val >= 1000) {
            return neg + ((float) val / 1000).ToString("F1") + "k";
        } else {
            return neg + val.ToString("F1") + "";
        }
    }

    public void updateResources() {
        waterText.text = string.Format("Water: {0}", compressResourceText(world.water));
        nText.text = string.Format("Nitrogen: {0}", compressResourceText(world.nitrogen));

        if (infoBlock.activeSelf) {
            showInfoBlock(lastInfoBlock, false);
        }
    }

    public void gameCanvasClicked() {
        world.placeBuilding(selectedTile.x, selectedTile.y, bldToPlace);
    }

    public void changeBldToPlace(GameObject bld) {
        bldToPlace = bld;

        switch (bld.GetComponent<Building>().bldName) {
            case "root": bldSelector.parent = rootBtn; break;
            case "pump": bldSelector.parent = pumpBtn; break;
            case "cannon": bldSelector.parent = cannonBtn; break;
        }

        bldSelector.localPosition = Vector3.zero;
        world.selectedBuilding = bldToPlace.GetComponent<Building>();
        world.updatePreview();
    }

    public void upgrade(string thing) {
        bool result = world.upgrade(thing);

        if (result) {
            hideInfoBlock();
        }
    }

    void handleMouseScrolling() {
        float scroll = -Input.mouseScrollDelta.y * 1.5f;

        if (scroll != 0) {
            var originalCamOffset = camOffsetY;
            camOffsetY += scroll;

            int maxCamOffset = world.maxBldY - 1;
            
            if (camOffsetY > maxCamOffset) {
                camOffsetY = maxCamOffset;
            }

            if (camOffsetY < 0) {
                camOffsetY = 0;
            }

            cam.transform.position = cameraOriginalPos + new Vector2(0, (int) -camOffsetY);
            world.generateMoreBackground((int) camOffsetY);
        }
    }

    void handleMousePositioning() {
        oldMousePos = mousePos;
        mousePos = Input.mousePosition;

        if (oldMousePos != null && oldMousePos != mousePos) {
            mousePos.y = Screen.height - mousePos.y; // for some reason it starts from the bottom

            mousePos.x = (mousePos.x / Screen.width) * reference_res.x;
            mousePos.y = (mousePos.y / Screen.height) * reference_res.y;
            selectedTile = new Vector2Int((int) (mousePos.x / 32), (int) (mousePos.y / 32));
            selectedTile.y += Mathf.Abs((int) camOffsetY);

            world.moveCursor(selectedTile.x, selectedTile.y);
        }
    }

    void handleCanvasResizing() {
        Vector2 old_res = current_res;
        current_res = new Vector2(Screen.width, Screen.height);

        if (old_res != null && current_res != old_res) {
            updateResolution();
        }
    }

    void updateResolution() {
        transform.localScale = new Vector3((float) current_res.x / reference_res.x, (float) current_res.y / reference_res.y, 1);
    }
}
