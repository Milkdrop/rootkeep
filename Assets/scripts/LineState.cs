using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineState : MonoBehaviour
{
    public float speed;
    public int chunks = 5;
    LineRenderer line;
    Vector2[] targetPoints;

    Vector2 origin;
    Vector2 offset;
    
    int pointIdx = 0;
    public bool finished = false;

    public void setTarget(Vector2 target) {
        line = GetComponent<LineRenderer>();
        targetPoints = new Vector2[chunks];
        line.SetPositions(new Vector3[chunks]);
        origin = new Vector2(0, 0);
        offset = new Vector2(0.5f, -0.5f);

        finished = false;
        line.positionCount = 1;
        line.SetPosition(0, origin + offset);

        targetPoints[0] = origin;
        print(origin);

        for (int i = 1; i < chunks - 1; i++) {
            var j = 0.1f;
            var jitter = new Vector2(Random.Range(-j, j), Random.Range(-j, j));
            targetPoints[i] = (target - origin) * ((float) i / chunks) + jitter;
            print(targetPoints[i]);
        }

        targetPoints[chunks - 1] = target;
        print(target);
    }

    public void stop() {
        //CancelInvoke("update");
    }
    
    public void update() {
        var currentPosRaw = line.GetPosition(line.positionCount - 1);
        var currentPos = new Vector2(currentPosRaw.x, currentPosRaw.y) - offset;

        if (currentPos == targetPoints[pointIdx]) {
            pointIdx += 1;

            if (pointIdx < chunks) {
                line.positionCount += 1;
            }
            //print("add pointIdx: " + pointIdx);
        }
        
        //print("render: " + pointIdx + " " + currentPos + " " + targetPoints[0]);
        if (pointIdx < chunks) {
            var originalPoint = targetPoints[pointIdx - 1];
            var target = targetPoints[pointIdx];
            currentPos += (target - originalPoint) * speed;

            if (Vector2.Distance(currentPos, target) < speed) {
                currentPos = target;
            }

            line.SetPosition(line.positionCount - 1, currentPos + offset);
        } else {
            finished = true;
            stop();
        }
    }
}
