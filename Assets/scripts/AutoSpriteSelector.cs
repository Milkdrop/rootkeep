using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSpriteSelector : MonoBehaviour
{
    public List<Sprite> sprites;

    void Start()
    {
        var s = gameObject.GetComponent<SpriteRenderer>();

        if (sprites.Count > 0) {
            s.sprite = sprites[Random.Range(0, sprites.Count)]; // random.range for ints is exclusive
        }
    }
}
