using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Building : MonoBehaviour
{
    [HideInInspector]
    public World world;

    public string bldName;
    public List<Vector2Int> bldShape;

    public int baseMaxHP;

    int hp;

    public int waterCost = 0;
    public int nCost = 0;
    public int pCost = 0;

    public int kCost = 0;

    public int baseDamage;


    [HideInInspector]
    public int x;
    [HideInInspector]
    public int y;

    bool isBeingDestroyed = false;

    [HideInInspector]
    public bool dontAutodestroy = false;

    Enemy closestEnemy;

    public float maxRange;

    public int extractionSpeed;
    
    public int waterNeed;

    [HideInInspector]
    public float waterAvailable;

    public int maxHP;
    public int damage;

    public Building connectedRoot;
    public Vector2 connectedPoint;

    public GameObject spriteAnchor;
    
    Quaternion targetSpriteRotation;

    public GameObject healthBarPrefab;
    Transform healthBar;
    Animator animHpBar;


    [HideInInspector]
    public bool neverWarned = true;
    [HideInInspector]
    public bool blockedOffWarning = false;

    void Start() {
        GameObject hpBarObj = Instantiate(healthBarPrefab, transform.position + new Vector3(0, -0.01f, 0), Quaternion.identity, transform);
        animHpBar = hpBarObj.GetComponent<Animator>();
        healthBar = hpBarObj.transform.GetChild(0).GetChild(0); // ugly...

        // If someone didnt already upgrade us
        if (maxHP == 0) {
            maxHP = baseMaxHP;
        }

        if (damage == 0) {
            damage = baseDamage;
        }

        hp = maxHP;

        if (bldName != "root") {
            InvokeRepeating("think", 0.25f, 0.25f);
        }

        targetSpriteRotation = Quaternion.identity;
    }

    public void renderRootConnections(int x, int y, Dictionary<Vector2Int, Building> bldMap) {
        var neighbours = new List<Vector2Int>();
        connectedPoint = new Vector2(x, y); 

        if (bldName == "root") {
            foreach (var offset in new Vector2Int[]{new Vector2Int(-1, -1), new Vector2Int(0, -1), new Vector2Int(1, -1)}) {
                var neighbour = new Vector2Int(x, y) + offset;
                if (bldMap.ContainsKey(neighbour)) {
                    if (bldMap[neighbour].bldName == "root") {
                        neighbours.Add(neighbour);
                    }
                }
            }
        } else {
            foreach (var shapeCoord in bldShape) {
                foreach (var offset in new Vector2Int[]{new Vector2Int(-1, 0), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(0, 1)}) {
                    var neighbour = new Vector2Int(x, y) + shapeCoord + offset;
                    if (bldMap.ContainsKey(neighbour)) {
                        if (bldMap[neighbour].bldName == "root") {
                            neighbours.Add(neighbour);
                            connectedPoint = new Vector2Int(x, y) + shapeCoord;
                        }
                    }
                }

            }
        }

        if (neighbours.Count > 0) {
            var b = bldMap[neighbours[Random.Range(0, neighbours.Count)]];
            connectedRoot = b; // only used when there's only 1 root connection
            var rootRenderer = b.GetComponent<rootRenderer>();
            rootRenderer.connectTo(connectedPoint);
        }
    }

    public void cutRootConnections(int x, int y, Dictionary<Vector2Int, Building> bldMap) {
        if (bldName == "root") {
            var neighbours = new List<Vector2Int>();

            foreach (var offset in new Vector2Int[]{new Vector2Int(-1, -1), new Vector2Int(0, -1), new Vector2Int(1, -1)}) {
                var neighbour = new Vector2Int(x, y) + offset;
                if (bldMap.ContainsKey(neighbour)) {
                    if (bldMap[neighbour].bldName == "root") {
                        bldMap[neighbour].GetComponent<rootRenderer>().cutConnection(new Vector2(x, y));
                    }
                }
            }
        } else {
            if (connectedRoot != null) {
                connectedRoot.GetComponent<rootRenderer>().cutConnection(connectedPoint);
            }
        }
    }

    public void upgradeHP(int newHP) {
        float ratio = (float) hp / maxHP;

        maxHP = newHP;
        hp = (int) ((float) maxHP * ratio);
    }

    public void upgradeDmg(int newDmg) {
        damage = newDmg;
    }

    void FixedUpdate() {
        if (spriteAnchor != null) {
            spriteAnchor.transform.rotation = Quaternion.RotateTowards(spriteAnchor.transform.rotation, targetSpriteRotation, 720 * Time.deltaTime);
        }
    }

    void think() {
        switch(bldName) {
            case "cannon": {
                findClosestEnemy();

                if (closestEnemy == null) {
                    return;
                }

                Vector2 posDiff = closestEnemy.transform.position - transform.position;
                var angle = Vector2.SignedAngle(new Vector2(1, 0), posDiff);

                targetSpriteRotation = Quaternion.Euler(0, 0, angle - 90);

                int currentDamage = damage;
                if (waterNeed > 0 && waterAvailable < waterNeed) {
                    currentDamage = (int) Mathf.Ceil((float) damage * (waterAvailable / waterNeed));
                }

                print("cannon used water: " + waterAvailable);
                world.waterDecrease(waterAvailable);

                closestEnemy.takeDamage(currentDamage);
            } break;
        }
    }

    void findClosestEnemy() {
        float minDist = Mathf.Infinity;

        foreach (var e in world.enemyList) {
            float dist = Vector2.Distance(transform.position, e.transform.position);
            //print("Checking bld " + transform.position + " " + coords + " " + dist);

            if (dist < minDist) {
                minDist = dist;
                closestEnemy = e;
            }
        }

        if (minDist >= maxRange) {
            closestEnemy = null;
        }
    }

    public Dictionary<string, float> getResourceGeneration(Dictionary<Vector2Int, Resource> bgMap, Dictionary<Vector2Int, Resource> oreMap, Dictionary<Vector2Int, Building> bldMap) {
        var position = new Vector2Int(x, y);
        var resources = new Dictionary<string, float>();
        
        switch(bldName) {
            // case "cannon": {
            //     resources["water"] = -1;
            //     if (waterAvailable < -1) {
            //         resources["water"] = -waterAvailable;
            //     }
            // } break;

            case "root": {
                if (!oreMap.ContainsKey(position)) {
                    bool free = false;
                    if (!bldMap.ContainsKey(position + new Vector2Int(-1, 0))) { free = true; }
                    if (!bldMap.ContainsKey(position + new Vector2Int(1, 0))) { free = true; }
                    if (!bldMap.ContainsKey(position + new Vector2Int(0, -1))) { free = true; }
                    if (!bldMap.ContainsKey(position + new Vector2Int(0, 1))) { free = true; }
                    
                    // TODO free bypass
                    if (free || true) {
                        resources["water"] = bgMap[position].stored;
                    } else {
                        blockedOffWarning = true;
                        print("root not free");
                    }
                }
            } break;

            case "pump": {
                resources["n"] = 0;

                foreach (var shapeCoords in bldShape) {
                    var coords = position + shapeCoords;

                    if (oreMap.ContainsKey(coords)) {
                        if (oreMap[coords].resName == "nitrogen") {
                            resources["n"] += extractionSpeed;
                            //oreMap[coords].stored -= extractionSpeed; // TODO Make resources deplete
                            
                        }
                    }
                }

                resources["water"] = -waterNeed;

                if (waterAvailable < waterNeed) {
                    float efficiency = (float) waterAvailable / waterNeed;

                    world.pumpEfficiency = efficiency;
                    resources["n"] = (float) resources["n"] * efficiency;
                    resources["water"] = -waterAvailable;
                }

            } break;
        }

        return resources;
    }

    void stopShowingHPBar() {
        animHpBar.SetBool("wake", false);
    }

    public void takeDamage(int damage) {
        hp -= damage;
        if (hp <= 0) {
            hp = 0;
        }

        print("building takes damage, hp: " + hp + " " + healthBar + " " + (float) hp / maxHP);

        healthBar.localScale = new Vector3((float) hp / maxHP, 1, 1);
        animHpBar.SetBool("wake", true);

        CancelInvoke("stopShowingHPBar");
        Invoke("stopShowingHPBar", 10);

        if (hp == 0) {
            destroy();
        }
    }

    public void destroy() {
        if (!isBeingDestroyed) {
            isBeingDestroyed = true;

            print("building destroyed!");
            world.buildingDestroyed(this);
            Destroy(this.gameObject);
        }
    }

    bool isConnectedToRoot(int x, int y, Dictionary<Vector2Int, Building> bldMap) {
        var checkCoords = new List<Vector2Int>();
        foreach (var coord in bldShape) {
            int ox = x + coord.x;
            int oy = y + coord.y;

            checkCoords.Add(new Vector2Int(ox - 1, oy));
            checkCoords.Add(new Vector2Int(ox + 1, oy));
            checkCoords.Add(new Vector2Int(ox, oy - 1));
            checkCoords.Add(new Vector2Int(ox, oy + 1));
        }

        var hasAdjacentRoot = false;

        foreach (var coord in checkCoords) {
            if (bldMap.ContainsKey(coord)) {
                var checkB = bldMap[coord];
                if (checkB.bldName == "root") {
                    hasAdjacentRoot = true;
                }
            }
        }

        return hasAdjacentRoot;
    }

    // Check if we have all conditions needed to build (root around us, etc)
    // also used when a building next to us got destroyed, to see if we can still stand
    public bool lifeCheck(int x, int y, Dictionary<Vector2Int, Resource> bgMap, Dictionary<Vector2Int, Resource> oreMap, Dictionary<Vector2Int, Building> bldMap) {
        var lifeCheck = false;

        switch (bldName) {
            case "root": {
                // Check if there's a root above us
                var checkCoords = new List<Vector2Int>();
                checkCoords.Add(new Vector2Int(x - 1, y - 1));
                checkCoords.Add(new Vector2Int(x, y - 1));
                checkCoords.Add(new Vector2Int(x + 1, y - 1));

                foreach (var coord in checkCoords) {
                    if (bldMap.ContainsKey(coord)) {
                        var topB = bldMap[coord];
                        if (topB.bldName == "root") {
                            lifeCheck = true;
                        }
                    }
                }
            } break;

            case "pump": {
                // Check if there's any adjacent root to connect to
                
                var isOnOre = false;

                foreach (var coord in bldShape) {
                    var checkCoord = new Vector2Int(x, y) + coord;
                    print("trying " + checkCoord);
                    if (oreMap.ContainsKey(checkCoord)) {
                        print("checking ore " + checkCoord);
                        var check = oreMap[checkCoord];
                        if (check.resName == "nitrogen") {
                            print("ok");
                            isOnOre = true;
                        }
                    }
                }

                lifeCheck = isConnectedToRoot(x, y, bldMap) && isOnOre;
            } break;

            case "cannon": {
                lifeCheck = isConnectedToRoot(x, y, bldMap);
            } break;
        }

        return lifeCheck;
    }

    public bool canPlace(int x, int y, Dictionary<Vector2Int, Resource> bgMap, Dictionary<Vector2Int, Resource> oreMap, Dictionary<Vector2Int, Building> bldMap) {
        var canPlace = false;

        var overlapsBld = false;
        foreach (var coord in bldShape) {
            var checkCoord = coord + new Vector2Int(x, y);
            print("checking " + checkCoord);
            if (bldMap.ContainsKey(checkCoord)) {
                overlapsBld = true;
            }
        }

        print(overlapsBld);

        if (!overlapsBld) {
            canPlace = lifeCheck(x, y, bgMap, oreMap, bldMap);
        }

        return canPlace;
    }
}
