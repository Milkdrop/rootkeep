using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public UIManager uiManager;

    public float water = 0;
    public float nitrogen = 100;

    [HideInInspector]
    public float waterPerS = 0;

    [HideInInspector]
    public float negWaterPerS = 0;

    [HideInInspector]
    public float nitrogenPerS = 0;

    public float resourceUpdateInterval = 0.05f;
    public int resourcePerSUpdateFrequency = 4;

    int resPerSUpdateCounter = 0;

    public Vector2Int screenGridSize;
    public GameObject cursor;
    public Transform backgroundParent;
    public Transform oresParent;
    public Transform buildingsParent;
    public Transform enemiesParent;
    private int lastBgLayerDrawn = 0;

    private Vector2 randomRange;

    public Vector2 bgPerlinScale;
    public Vector2 oreNPerlinScale;
    public Vector2 orePPerlinScale;

    public Vector2 oreKPerlinScale;

    public float oreNtreshold;

    private Vector2 bgPerlinOffset;
    private Vector2 oreNPerlinOffset;
    private Vector2 orePPerlinOffset;
    private Vector2 oreKPerlinOffset;

    public GameObject originalTile;
    public GameObject dirtTile;
    public GameObject oreNTile;
    public GameObject orePTile;
    public GameObject oreKTile;
    
    List<Building> bldList;

    [HideInInspector]
    public List<Building> waterConsumers;

    [HideInInspector]
    public List<Enemy> enemyList;

    Dictionary<Vector2Int, Resource> bgMap;
    Dictionary<Vector2Int, Resource> oreMap;
    Dictionary<Vector2Int, Building> bldMap;

    public Vector2 testPerlinOffset;
    public Vector2 testPerlinScale;
    
    [HideInInspector]
    public int maxBldY = 0;

    /*
    difficulty:
    0 - before any building, no chance
    1 - 

    */

    public float difficulty = 0;
    
    public float enemySpawnChance = 0;

    public float enemyHP = 1;

    public GameObject enemyBasic;

    int difficultyBuildingsPlaced = 0;
    
    [HideInInspector]
    public float buildingsHPmult = 1.0f;
    [HideInInspector]
    public float buildingsDmgBonus = 1.0f;
    [HideInInspector]
    public float buildingsDmgMult = 1.0f;

    [HideInInspector]
    public int upgradeCost = 100;

    [HideInInspector]
    public float pumpEfficiency = 1;

    public Transform previewParent;

    public GameObject previewObject;
    public GameObject previewObjectForbidden;

    [HideInInspector]
    public Building selectedBuilding;

    Vector2 oldCursorPos;

    bool oldCanAffordPreview = false;

    // Start is called before the first frame update
    void Start()
    {
        oldCursorPos = new Vector2(-1, -1);
        bldList = new List<Building>();
        waterConsumers = new List<Building>();

        bgMap = new Dictionary<Vector2Int, Resource>();
        oreMap = new Dictionary<Vector2Int, Resource>();
        bldMap = new Dictionary<Vector2Int, Building>();

        randomRange = new Vector2(100, 100000);
        bgPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));
        oreNPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));
        orePPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));
        oreKPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));

        // TODO: Increase ORE stuff as we keep digging down
        oreNPerlinScale = new Vector2(0.3f, 0.5f);
        oreNtreshold = 0.7f;

        buildBackground();

        var originalBld = placeBuilding((int) screenGridSize.x / 2, 0, originalTile, true);
        originalBld.GetComponent<Building>().dontAutodestroy = true; // make sure a failed lifecheck doesn't destroy us
        originalBld.GetComponent<rootRenderer>().autoconnect(new Vector2((int) screenGridSize.x / 2, -1));

        InvokeRepeating("resourceUpdate", resourceUpdateInterval, resourceUpdateInterval);
        uiManager.updateResources();

        InvokeRepeating("handleEnemies", 1, 1);
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) {
            uiManager.ResetGame();
        }
        
        //buildBackground();
        //perlinTest();
    }

    // void FixedUpdate() {
    //     // handleEnemies();
    // }
    
    public void enemyDestroyed(Enemy e) {
        enemyList.Remove(e);
    }

    public float getNextUpgrade(string thing) {
        switch (thing) {
            case "hpMult": return buildingsHPmult + buildingsHPmult * 0.25f;
            case "dmgBonus": return buildingsDmgBonus + 1;
            case "dmgMult": return buildingsDmgMult + buildingsDmgMult * 0.15f;
        }

        return 0;
    }

    public void upgradeBuildingAll(Building b) {
        upgradeBuilding(b, "hp");
        upgradeBuilding(b, "dmg");
    }

    void upgradeBuilding(Building b, string thing) {
        print("upgrading " + thing + " for " + b + " " + b.maxHP);

        switch (thing) {
            case "hp": {
                b.upgradeHP((int) (b.baseMaxHP * buildingsHPmult));
            } break;

            case "dmg": {
                b.upgradeDmg((int) ((buildingsDmgBonus + b.baseDamage) * buildingsDmgMult));
            } break;
        }
    }

    public bool upgrade(string thing) {
        if (nitrogen < upgradeCost) {
            uiManager.warn("You cannot afford that upgrade!\nYou need " + upgradeCost + " Nitrogen.");
            return false;
        }
        
        nitrogenDecrease(upgradeCost);

        print("upgrading: " + thing);
        switch(thing) {
            case "hp": {
                buildingsHPmult = getNextUpgrade("hpMult");
                foreach (var b in bldList) {
                    upgradeBuilding(b, "hp");
                }
            } break;

            case "dmg": {
                buildingsDmgBonus = getNextUpgrade("dmgBonus");
                buildingsDmgMult = getNextUpgrade("dmgMult");
                foreach (var b in bldList) {
                    upgradeBuilding(b, "dmg");
                }
            } break;
        }

        upgradeCost += 25;
        return true;
    }

    public void buildingDestroyed(Building b) {
        bldList.Remove(b);
        waterConsumers.Remove(b);
        Vector2Int coords = new Vector2Int(b.x, b.y);

        bool collateralDamageWarning = false;

        foreach (var coordShape in b.bldShape) {
            bldMap.Remove(coords + coordShape);
        }

        b.cutRootConnections(b.x, b.y, bldMap);

        // Check if any other buildings need to be destroyed
        var neighbours = new List<Building>();

        foreach (var coordShape in b.bldShape) {
            var chunkCoord = coords + coordShape;

            foreach (var ox in new int[]{-1, 0, 1}) {
                foreach (var oy in new int[]{-1, 0, 1}) {
                    var offset = new Vector2Int(ox, oy);
                    
                    if (bldMap.ContainsKey(chunkCoord + offset)) {
                        var neighbour = bldMap[chunkCoord + offset];

                        if (!neighbours.Contains(neighbour)) {
                            neighbours.Add(neighbour);
                        }
                    }
                }
            }

            foreach (var neighbour in neighbours) {
                if (!neighbour.lifeCheck(neighbour.x, neighbour.y, bgMap, oreMap, bldMap) && !neighbour.dontAutodestroy) {
                    neighbour.destroy();
                    collateralDamageWarning = true;
                }
            }
        }

        updatePreview();
        if (collateralDamageWarning) {
            uiManager.warn("All buildings no longer connected to the main root get destroyed.");
        }

        if (bldList.Count == 0) {
            uiManager.gameOver();
        }
    }

    // bool onlyoneenemy = false;
    bool debugSpawn = false;
    void handleEnemies() {
        var roll = Random.Range(0.0f, 1.0f);

        if (difficulty <= 1 && nitrogenPerS < 5) {
            if (!debugSpawn) {
                return;
            }
        }

        float enemyHPmultiplier = 1;
        float enemyDamagemultiplier = 1;
        
        if (difficulty >= 2) {
            difficulty += difficulty / 50; 
        }

        if (difficulty == 0) {
            enemySpawnChance = 0;
        } else if (difficulty <= 1) {
            enemySpawnChance = difficulty / 4;
        } else if (difficulty <= 2) {
            enemySpawnChance = difficulty / 2;
        } else if (difficulty <= 10) {
            enemySpawnChance = 1;

            enemyHPmultiplier = 1.5f + (difficulty / 2);
            enemyDamagemultiplier = 1.0f + (difficulty / 5);
        } else {
            enemySpawnChance = 1;

            enemyHPmultiplier = 2.0f + (difficulty / 2) ;
            enemyDamagemultiplier = 3.0f + (difficulty / 40);
        }

        if (enemySpawnChance == 1 && nitrogenPerS <= 50) {
            enemySpawnChance *= 1 - 0.9f * (50 - nitrogenPerS) / 50;
        }

        if (debugSpawn || roll < enemySpawnChance || enemySpawnChance == 1) {
            // onlyoneenemy = true;
            var coords = new Vector2(Random.Range(1.0f, screenGridSize.x - 1), - (maxBldY + 10));
            
            GameObject obj = Instantiate(enemyBasic, coords, Quaternion.identity, enemiesParent);
            var e = obj.GetComponent<Enemy>();
            e.bldMap = bldMap;
            e.world = this;
            e.multiplyStats(enemyHPmultiplier, enemyDamagemultiplier);

            enemyList.Add(e);
        }
    }

    private GameObject placeTile(int x, int y, GameObject buildingPrefab, Transform parent) {
        y = -y;
        return Instantiate(buildingPrefab, new Vector3(x, y), Quaternion.identity, parent);
    }

    public GameObject placeBuilding(int x, int y, GameObject buildingPrefab, bool force = false) {
        GameObject obj = null;
        var b = buildingPrefab.GetComponent<Building>();
        
        bool canPlace = b.canPlace(x, y, bgMap, oreMap, bldMap);

        if (force || canPlace) {
            // TODO add other costs
            if (force || (water >= b.waterCost && nitrogen >= b.nCost)) {
                if (!force) {
                    water -= b.waterCost;
                    nitrogen -= b.nCost;
                }

                obj = placeTile(x, y, buildingPrefab, buildingsParent);
                b = obj.GetComponent<Building>();

                b.x = x;
                b.y = y;
                b.world = this;
                upgradeBuildingAll(b);
                
                maxBldY = Mathf.Max(maxBldY, y);
                
                bldList.Add(b);
                foreach (var coord in b.bldShape) {
                    bldMap[new Vector2Int(x, y) + coord] = b;
                }
                
                b.renderRootConnections(x, y, bldMap);

                if (b.waterNeed > 0) {
                    waterConsumers.Add(b);
                }

                //difficultyBuildingsPlaced += 1;
                difficultyBuildingsPlaced = bldList.Count;

                if (difficulty <= 1 && !force) {
                    difficulty += 0.003f * difficultyBuildingsPlaced;
                }

                if (difficultyBuildingsPlaced >= 50) {
                    difficulty += 0.01f * (difficultyBuildingsPlaced / 50);
                }

                updatePreview();
            } else {
                uiManager.warn("You cannot afford that!\nYou need " + b.nCost + " Nitrogen.");
            }
        } else {
            print("nope");

            var info = "";
            switch (b.bldName) {
                case "root": {
                    info = "Roots must be placed underneath other roots (diagonally works too).";
                } break;

                case "pump": {
                    info = "Pumps need to be directly connected to a root and be placed on Nitrogen tiles.";
                } break;

                case "cannon": {
                    info = "Cannons need to be directly connected to a root.";
                } break;
            }
            
            if (info.Length > 0) {
                uiManager.warn(info);
            }
        }

        return obj;
    }

    // private void resPerSUpdate() {
    // }

    private void resourceUpdate() {
        resPerSUpdateCounter += 1;
        if (resPerSUpdateCounter >= resourcePerSUpdateFrequency) {
            resPerSUpdateCounter = 0;

            // we start with these res's
            waterPerS = 2;
            nitrogenPerS = 2;
            negWaterPerS = 0;

            // tell buildings how much water they have
            foreach (var b in waterConsumers) {
                b.waterAvailable = water / waterConsumers.Count;
            }

            foreach (var b in bldList) {
                var res = b.getResourceGeneration(bgMap, oreMap, bldMap);
                
                if (b.blockedOffWarning && b.neverWarned) {
                    b.neverWarned = false;
                    uiManager.warn("Roots need at least one empty space in either 4 directions to gather water.");
                }

                if (res.ContainsKey("water")) { 
                    if (res["water"] < 0) {
                        negWaterPerS += -res["water"];
                    } else {
                        waterPerS += res["water"];
                    }
                }
                if (res.ContainsKey("n")) { nitrogenPerS += res["n"]; }
            }

            if (nitrogenPerS >= 10) {
                difficulty += 0.0005f * (nitrogenPerS / 10);
            }
        }

        water += (waterPerS - negWaterPerS) * resourceUpdateInterval;
        nitrogen += nitrogenPerS * resourceUpdateInterval;
        
        if (water < 0) { water = 0; }
        if (nitrogen < 0) { nitrogen = 0; }
        
        if (oldCanAffordPreview != canAffordBuildingPreview()) {
            updatePreview();
        }

        uiManager.updateResources();
    }

    public void waterDecrease(float amount) {
        water -= amount;
        if (water < 0) { water = 0; }
        uiManager.updateResources();
    }

    public void nitrogenDecrease(float amount) {
        nitrogen -= amount;
        if (nitrogen < 0) { nitrogen = 0; }
        uiManager.updateResources();
    }

    // mostly used for debugging
    private void perlinTest() {
        for (int iy = 0; iy < screenGridSize.y; iy++) {
            for (int ix = 0; ix < screenGridSize.x; ix++) {
                Transform obj = backgroundParent.GetChild(iy * screenGridSize.x + ix);

                float noise = Mathf.PerlinNoise((float) ix * testPerlinScale.x + testPerlinOffset.x, (float) iy * testPerlinScale.y + testPerlinOffset.y);
                obj.GetComponent<SpriteRenderer>().color = new Color(noise, noise, noise);
            }
        }
    }

    private void buildBackground() {
        foreach (Transform child in backgroundParent) {
            Destroy(child.gameObject);
        }

        foreach (Transform child in oresParent) {
            Destroy(child.gameObject);
        }

        for (int iy = 0; iy < screenGridSize.y; iy++) {
            for (int ix = 0; ix < screenGridSize.x; ix++) {
                placeBackgroundTile(ix, iy);
            }
        }
        
        lastBgLayerDrawn = screenGridSize.y;
    }

    private void placeBackgroundTile(int x, int y) {
        GameObject obj = placeTile(x, y, dirtTile, backgroundParent);
        bgMap[new Vector2Int(x, y)] = obj.GetComponent<Resource>();

        float noise = Mathf.PerlinNoise((float) x * bgPerlinScale.x + bgPerlinOffset.x, (float) y * bgPerlinScale.y + bgPerlinOffset.y);
        //print(x + " " + y);
        //print(bgPerlinOffset);
        //print(bgPerlinScale);

        //print(noise + " " + ((float) x * bgPerlinScale.x + bgPerlinOffset.x) + " " + ((float) y * bgPerlinScale.y + bgPerlinOffset.y));
        
        int resource = (int) (noise * 4.99);
        //print(resource);
        obj.GetComponent<Resource>().stored = resource;

        float opacity = 1 - ((float) resource / 4) * 0.65f;
        obj.GetComponent<SpriteRenderer>().color = new Color(opacity, opacity, opacity);

        // Generate ore
        noise = Mathf.PerlinNoise((float) x * oreNPerlinScale.x + oreNPerlinOffset.x, (float) y * oreNPerlinScale.y + oreNPerlinOffset.y);

        float thresholdDecrease = ((float) maxBldY / 50) * 0.2f;
        print(thresholdDecrease);

        if (thresholdDecrease >= 0.2f) {
            thresholdDecrease = 0.2f;
        }

        if (noise >= oreNtreshold - thresholdDecrease) {
            obj = placeTile(x, y, oreNTile, oresParent);
            
            var opacityDecay = 1 - opacity;
            opacityDecay /= 2;
            opacity = 1 - opacityDecay;

            obj.GetComponent<SpriteRenderer>().color = new Color(opacity, opacity, opacity);
            var r = obj.GetComponent<Resource>();
            r.stored = Random.Range(500, 1000);

            oreMap[new Vector2Int(x, y)] = r;
        }
    }

    public void generateMoreBackground(float cameraOffset) {
        int maxVisibleLayer = screenGridSize.y + (int) Mathf.Ceil(cameraOffset);
        int layersToRender = maxVisibleLayer - lastBgLayerDrawn;
        
        if (layersToRender > 0) {
            //print("max y offset: " + maxYOffsetReached + ", new offset y: " + newOffsetY);
            //print("Rendering new block layers: " + layersToRender);

            for (int iy = 0; iy < layersToRender; iy++) {
                for (int ix = 0; ix < screenGridSize.x; ix++) {
                    placeBackgroundTile(ix, lastBgLayerDrawn + iy);
                }
                //print("Rendering at: " + (lastBgLayerDrawn + iy));
            }
            
            lastBgLayerDrawn = maxVisibleLayer;
        }
    }

    public void moveCursor(int x, int y) {
        y = -y;

        var cursorPos = new Vector2(x, y);
        cursor.transform.localPosition = cursorPos;
        
        if (cursorPos != oldCursorPos) {
            oldCursorPos = cursorPos;
            updatePreview();
        }
    }

    public bool canAffordBuildingPreview() {
        var b = selectedBuilding;
        return water >= b.waterCost && nitrogen >= b.nCost;
    }

    public void updatePreview() {
        // it can happen when we are at the very start of the game
        if (selectedBuilding == null) {
            return;
        }

        foreach (Transform child in previewParent) {
            Destroy(child.gameObject);
        }

        var x = (int) oldCursorPos.x;
        var y = (int) oldCursorPos.y;
        
        var b = selectedBuilding;
        bool canPlace = b.canPlace(x, -y, bgMap, oreMap, bldMap) && canAffordBuildingPreview();
        
        oldCanAffordPreview = canAffordBuildingPreview();

        //print(x + " " + y + " " + selectedBuilding + " " + canPlace);

        foreach (var shapeCoord in b.bldShape) {
            GameObject prefab = previewObject;

            if (!canPlace) {
                prefab = previewObjectForbidden;
            }

            var obj = Instantiate(prefab, oldCursorPos + new Vector2(shapeCoord.x, -shapeCoord.y), Quaternion.identity, previewParent);
        }
    }
}
