using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rootRenderer : MonoBehaviour
{

    public GameObject linePrefab;
    private int currentPosition;
    
    Dictionary<Vector2, LineState> lines;

    Vector2 autoconnectVec = new Vector2(0, 0);
    bool isAutoconnecting = false;
    
    public void autoconnect(Vector2 point) {
        isAutoconnecting = true;
        autoconnectVec = point;
    }

    void Start() {
        lines = new Dictionary<Vector2, LineState>();

        if (isAutoconnecting) {
            connectTo(autoconnectVec);
        }
    }

    public void connectionAnimation() {
        foreach (var ls in lines.Values) {
            if (!ls.finished) {
                ls.update();
            }
        }
    }

    public void connectTo(Vector2 point) {
        if (!lines.ContainsKey(point)) {
            var obj = Instantiate(linePrefab, transform.position, Quaternion.identity, transform);
            var ls = obj.GetComponent<LineState>();

            var target = point - new Vector2(transform.position.x, -transform.position.y);
            target.y = -target.y;
            lines[point] = ls;

            ls.setTarget(target);

            CancelInvoke("connectionAnimation");
            InvokeRepeating("connectionAnimation", 0.025f, 0.025f);

            //addPoint(line, new Vector2(0, 0));

            //print("connect: " + point + " " + transform.position + " " + new Vector2(transform.position.x, transform.position.y) + " " + (point - new Vector2(transform.position.x, transform.position.y)));
            //addPoint(line, point - new Vector2(transform.position.x, -transform.position.y));
        }
    }

    public void cutConnection(Vector2 point) {
        if (lines.ContainsKey(point)) {
            lines[point].stop();
            Destroy(lines[point].gameObject);
            lines.Remove(point);
        }
    }
}
