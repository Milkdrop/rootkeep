using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [HideInInspector]
    public World world;

    public Dictionary<Vector2Int, Building> bldMap;
    
    public int maxHP;
    int hp;

    public int damage;

    public float rotationSpeed = 0.1f;
    public float moveSpeed = 0.1f;
    
    int oldMapSize = 0;
    GameObject closestBuilding;

    Quaternion targetRotation;
    Vector2 targetPosition;
    Vector2 posSmoothDampState;

    bool reachedDestination = false;

    public GameObject healthBarPrefab;
    Transform healthBar;
    Animator animHpBar;

    public GameObject sprite;

    void Start() {
        GameObject hpBarObj = Instantiate(healthBarPrefab, transform.position + new Vector3(-0.5f, 0.65f, 0), Quaternion.identity, transform);
        animHpBar = hpBarObj.GetComponent<Animator>();
        healthBar = hpBarObj.transform.GetChild(0).GetChild(0); // ugly...

        hp = maxHP;
        InvokeRepeating("think", 0.25f, 0.25f);
    }

    void findClosestBuilding() {
        float minDist = Mathf.Infinity;

        foreach (var b in bldMap.Values) {
            foreach (var shapeCoord in b.bldShape) {
                Vector2 coords = new Vector2(b.x + shapeCoord.x, -(b.y + shapeCoord.y));
                float dist = Vector2.Distance(transform.position, coords);
                //print("Checking bld " + transform.position + " " + coords + " " + dist);

                if (dist < minDist) {
                    minDist = dist;
                    closestBuilding = b.gameObject;
                }
            }
        }
    }

    public void multiplyStats(float hpMult, float dmgMult) {
        maxHP = (int) ((float) maxHP * hpMult);
        damage = (int) ((float) damage * dmgMult);

        hp = maxHP;
    }

    public void takeDamage(int damage) {
        print("enemy hp: " + hp + " took damage: " + damage);
        hp -= damage;
        if (hp <= 0) {
            hp = 0;
        }

        healthBar.localScale = new Vector3((float) hp / maxHP, 1, 1);
        animHpBar.SetBool("wake", true); // always show hp bar in enemies

        if (hp == 0) {
            print("enemy dead!");
            world.enemyDestroyed(this);
            
            Destroy(this.gameObject);
        }
    }

    void think() {
        if (bldMap.Count != oldMapSize) {
            oldMapSize = bldMap.Count;
            findClosestBuilding();
        }

        if (closestBuilding == null) {
            findClosestBuilding();
            return;
        }

        var bldPos = closestBuilding.transform.position;
        var targetCoord = new Vector2(bldPos.x, bldPos.y) + new Vector2(0.5f, -0.5f); // Center on the building;

        Vector2 posDiff = targetCoord - new Vector2(transform.position.x, transform.position.y);
        var angle = Vector2.SignedAngle(new Vector2(1, 0), posDiff);

        targetRotation = Quaternion.Euler(0, 0, angle - 90);
        targetPosition = transform.position + sprite.transform.up * moveSpeed;

        reachedDestination = false;
        if (Vector2.Distance(transform.position, targetCoord) < 1.2f) {
            reachedDestination = true;
            targetPosition = transform.position;

            var b = closestBuilding.GetComponent<Building>();
            b.takeDamage(damage);
        }
    }

    void FixedUpdate() {
        if (!reachedDestination) {
            transform.position = Vector2.SmoothDamp(transform.position, targetPosition, ref posSmoothDampState, 0, moveSpeed);
            sprite.transform.rotation = Quaternion.RotateTowards(sprite.transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
    }
}
