using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    public string resName;

    [HideInInspector]
    public int stored = 0;
}
