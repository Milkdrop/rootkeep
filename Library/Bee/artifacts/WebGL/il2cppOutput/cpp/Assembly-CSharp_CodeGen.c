﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Building::Start()
extern void Building_Start_m0AF45A4F189A22A3E65906C321511C3B21970FDC (void);
// 0x00000002 System.Void Building::think()
extern void Building_think_m4D58534AA5E59080C57A4BB1880FA76058F514F2 (void);
// 0x00000003 System.Void Building::findClosestEnemy()
extern void Building_findClosestEnemy_m78B19B59C3D6D34967083BD8002C0E7491A55929 (void);
// 0x00000004 System.Collections.Generic.Dictionary`2<System.String,System.Int32> Building::getResourceGeneration(System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
extern void Building_getResourceGeneration_m0FF5325BAB3A3792F7B2D5F4FFDF33F19C818EA7 (void);
// 0x00000005 System.Void Building::takeDamage(System.Int32)
extern void Building_takeDamage_m6ECB33F272DFAFEACC917CB29402FD33D8888016 (void);
// 0x00000006 System.Void Building::destroy()
extern void Building_destroy_m1BDC7A8FBECE0B0B13C491541AE704D9AF47EF2A (void);
// 0x00000007 System.Boolean Building::isConnectedToRoot(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
extern void Building_isConnectedToRoot_m2531738249902042141A601B72611A949EE3B403 (void);
// 0x00000008 System.Boolean Building::lifeCheck(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
extern void Building_lifeCheck_mA5CC9AFDC20E50645D5DC250612391A16DAEE268 (void);
// 0x00000009 System.Boolean Building::canPlace(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
extern void Building_canPlace_m7EE85613F2F93E783FB18D6E537350C16158C9CF (void);
// 0x0000000A System.Void Building::.ctor()
extern void Building__ctor_m1966393898763C921095D832800266FB5532893F (void);
// 0x0000000B System.Void Enemy::Start()
extern void Enemy_Start_m8BBD9A5AE10A27ABDFCD9168B93CD9C69D229034 (void);
// 0x0000000C System.Void Enemy::findClosestBuilding()
extern void Enemy_findClosestBuilding_m922525EB6197C6DB29F06254584A361F3FFFEB53 (void);
// 0x0000000D System.Void Enemy::takeDamage(System.Int32)
extern void Enemy_takeDamage_m252CF45B4ADCFE8158C3CF5C8127C8A5C06B7A6F (void);
// 0x0000000E System.Void Enemy::think()
extern void Enemy_think_m3921B16C0F0728F075E82FC4AAA4566647B13A42 (void);
// 0x0000000F System.Void Enemy::FixedUpdate()
extern void Enemy_FixedUpdate_mC96E5B789335D23FC34546AD504EEBB0F054490B (void);
// 0x00000010 System.Void Enemy::.ctor()
extern void Enemy__ctor_mB6697627910F785A971C20C671DEFBA9D921D933 (void);
// 0x00000011 System.Void Resource::.ctor()
extern void Resource__ctor_mD9FC18750A7772A0360201303D4AEDF1B4CF3F11 (void);
// 0x00000012 System.Void UIManager::Start()
extern void UIManager_Start_m113F392674AB08A26877728CD36F06332E869080 (void);
// 0x00000013 System.Void UIManager::gameOver()
extern void UIManager_gameOver_m3B1C99259E72CD43C2586E28A93B855C39657E70 (void);
// 0x00000014 System.Void UIManager::ResetGame()
extern void UIManager_ResetGame_m87E2674E6E48B1D9A5D35C38FC77DD6C6FE756EF (void);
// 0x00000015 System.Void UIManager::Update()
extern void UIManager_Update_m95D2E80B8F461F15C1B9BD6DB0811F5CC18571AB (void);
// 0x00000016 System.String UIManager::compressResourceText(System.Int32)
extern void UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787 (void);
// 0x00000017 System.Void UIManager::updateResources()
extern void UIManager_updateResources_m1EBF6B83B71F6700912C6146A2E9828CC918D496 (void);
// 0x00000018 System.Void UIManager::gameCanvasClicked()
extern void UIManager_gameCanvasClicked_mCF5E1E7B77DB1A447609F7CF09684A78D91AC5C3 (void);
// 0x00000019 System.Void UIManager::changeBldToPlace(UnityEngine.GameObject)
extern void UIManager_changeBldToPlace_m3120C16C2DA7D62595DFD7B28CBD2AF5EC2FBF5A (void);
// 0x0000001A System.Void UIManager::handleMouseScrolling()
extern void UIManager_handleMouseScrolling_mB1B9E856F356252E3D197F35BB5784F1EB2638B2 (void);
// 0x0000001B System.Void UIManager::handleMousePositioning()
extern void UIManager_handleMousePositioning_m2C6C95E30F8DA4E45E71451997FC85742695FEB7 (void);
// 0x0000001C System.Void UIManager::handleCanvasResizing()
extern void UIManager_handleCanvasResizing_m6BCF100C3BEF15D5AAC823E069AE594F4E313CD0 (void);
// 0x0000001D System.Void UIManager::updateResolution()
extern void UIManager_updateResolution_m268488AC60B2A2A8282D9955A2613AB941C72839 (void);
// 0x0000001E System.Void UIManager::.ctor()
extern void UIManager__ctor_mC9DC2B8984E76F424E73C1860AD4BD3DEBF6573F (void);
// 0x0000001F System.Void World::Start()
extern void World_Start_mFF594A74C1DE2C5CDF4C18583DCA40EC7924F24F (void);
// 0x00000020 System.Void World::Update()
extern void World_Update_m19AA7041DF46C2C714D5E3499A3AF618979CFBCC (void);
// 0x00000021 System.Void World::enemyDestroyed(Enemy)
extern void World_enemyDestroyed_m3FB2E482EE7DF4CDC24070A2E0A4E5858EC8FB93 (void);
// 0x00000022 System.Void World::buildingDestroyed(Building)
extern void World_buildingDestroyed_mF07CB8463394DD626C024B5B66C935FD901BA8B1 (void);
// 0x00000023 System.Void World::handleEnemies()
extern void World_handleEnemies_mB61C1F9EC96BDE45520EE977D449BDED0C63C5FB (void);
// 0x00000024 UnityEngine.GameObject World::placeTile(System.Int32,System.Int32,UnityEngine.GameObject,UnityEngine.Transform)
extern void World_placeTile_m52587F1851930889B959CBB6053600F82237A511 (void);
// 0x00000025 UnityEngine.GameObject World::placeBuilding(System.Int32,System.Int32,UnityEngine.GameObject,System.Boolean)
extern void World_placeBuilding_m8694409FFA3866A65D502957536C52B7E9830098 (void);
// 0x00000026 System.Void World::resPerSUpdate()
extern void World_resPerSUpdate_mA57CE2A19904E03F3464CE96E6F429791806D772 (void);
// 0x00000027 System.Void World::resourceUpdate()
extern void World_resourceUpdate_m0F5E90EDEDF3CAE7AABBBBB8E96E303D18D34ADB (void);
// 0x00000028 System.Void World::perlinTest()
extern void World_perlinTest_m2D0B92E83071F3D3F565BAD57AB456C2839199D2 (void);
// 0x00000029 System.Void World::buildBackground()
extern void World_buildBackground_m9572A9FC3188862D4B440E02BA3F338A472B3B04 (void);
// 0x0000002A System.Void World::placeBackgroundTile(System.Int32,System.Int32)
extern void World_placeBackgroundTile_m60AD5694F95AB3385104B0A362D170FA08110CAE (void);
// 0x0000002B System.Void World::generateMoreBackground(System.Single)
extern void World_generateMoreBackground_m3D1A2A3203DFECB880EB4934579E241F8EBC30A0 (void);
// 0x0000002C System.Void World::moveCursor(System.Int32,System.Int32)
extern void World_moveCursor_mC133A148D958D0EC03ECE8A6BD12745873CCC3F1 (void);
// 0x0000002D System.Void World::.ctor()
extern void World__ctor_mAFAEE7FBF3A190572FB70B6BFCFEEC8655B5E354 (void);
static Il2CppMethodPointer s_methodPointers[45] = 
{
	Building_Start_m0AF45A4F189A22A3E65906C321511C3B21970FDC,
	Building_think_m4D58534AA5E59080C57A4BB1880FA76058F514F2,
	Building_findClosestEnemy_m78B19B59C3D6D34967083BD8002C0E7491A55929,
	Building_getResourceGeneration_m0FF5325BAB3A3792F7B2D5F4FFDF33F19C818EA7,
	Building_takeDamage_m6ECB33F272DFAFEACC917CB29402FD33D8888016,
	Building_destroy_m1BDC7A8FBECE0B0B13C491541AE704D9AF47EF2A,
	Building_isConnectedToRoot_m2531738249902042141A601B72611A949EE3B403,
	Building_lifeCheck_mA5CC9AFDC20E50645D5DC250612391A16DAEE268,
	Building_canPlace_m7EE85613F2F93E783FB18D6E537350C16158C9CF,
	Building__ctor_m1966393898763C921095D832800266FB5532893F,
	Enemy_Start_m8BBD9A5AE10A27ABDFCD9168B93CD9C69D229034,
	Enemy_findClosestBuilding_m922525EB6197C6DB29F06254584A361F3FFFEB53,
	Enemy_takeDamage_m252CF45B4ADCFE8158C3CF5C8127C8A5C06B7A6F,
	Enemy_think_m3921B16C0F0728F075E82FC4AAA4566647B13A42,
	Enemy_FixedUpdate_mC96E5B789335D23FC34546AD504EEBB0F054490B,
	Enemy__ctor_mB6697627910F785A971C20C671DEFBA9D921D933,
	Resource__ctor_mD9FC18750A7772A0360201303D4AEDF1B4CF3F11,
	UIManager_Start_m113F392674AB08A26877728CD36F06332E869080,
	UIManager_gameOver_m3B1C99259E72CD43C2586E28A93B855C39657E70,
	UIManager_ResetGame_m87E2674E6E48B1D9A5D35C38FC77DD6C6FE756EF,
	UIManager_Update_m95D2E80B8F461F15C1B9BD6DB0811F5CC18571AB,
	UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787,
	UIManager_updateResources_m1EBF6B83B71F6700912C6146A2E9828CC918D496,
	UIManager_gameCanvasClicked_mCF5E1E7B77DB1A447609F7CF09684A78D91AC5C3,
	UIManager_changeBldToPlace_m3120C16C2DA7D62595DFD7B28CBD2AF5EC2FBF5A,
	UIManager_handleMouseScrolling_mB1B9E856F356252E3D197F35BB5784F1EB2638B2,
	UIManager_handleMousePositioning_m2C6C95E30F8DA4E45E71451997FC85742695FEB7,
	UIManager_handleCanvasResizing_m6BCF100C3BEF15D5AAC823E069AE594F4E313CD0,
	UIManager_updateResolution_m268488AC60B2A2A8282D9955A2613AB941C72839,
	UIManager__ctor_mC9DC2B8984E76F424E73C1860AD4BD3DEBF6573F,
	World_Start_mFF594A74C1DE2C5CDF4C18583DCA40EC7924F24F,
	World_Update_m19AA7041DF46C2C714D5E3499A3AF618979CFBCC,
	World_enemyDestroyed_m3FB2E482EE7DF4CDC24070A2E0A4E5858EC8FB93,
	World_buildingDestroyed_mF07CB8463394DD626C024B5B66C935FD901BA8B1,
	World_handleEnemies_mB61C1F9EC96BDE45520EE977D449BDED0C63C5FB,
	World_placeTile_m52587F1851930889B959CBB6053600F82237A511,
	World_placeBuilding_m8694409FFA3866A65D502957536C52B7E9830098,
	World_resPerSUpdate_mA57CE2A19904E03F3464CE96E6F429791806D772,
	World_resourceUpdate_m0F5E90EDEDF3CAE7AABBBBB8E96E303D18D34ADB,
	World_perlinTest_m2D0B92E83071F3D3F565BAD57AB456C2839199D2,
	World_buildBackground_m9572A9FC3188862D4B440E02BA3F338A472B3B04,
	World_placeBackgroundTile_m60AD5694F95AB3385104B0A362D170FA08110CAE,
	World_generateMoreBackground_m3D1A2A3203DFECB880EB4934579E241F8EBC30A0,
	World_moveCursor_mC133A148D958D0EC03ECE8A6BD12745873CCC3F1,
	World__ctor_mAFAEE7FBF3A190572FB70B6BFCFEEC8655B5E354,
};
static const int32_t s_InvokerIndices[45] = 
{
	3426,
	3426,
	3426,
	711,
	2781,
	3426,
	620,
	174,
	174,
	3426,
	3426,
	3426,
	2781,
	3426,
	3426,
	3426,
	3426,
	3426,
	3426,
	3426,
	3426,
	2460,
	3426,
	3426,
	2798,
	3426,
	3426,
	3426,
	3426,
	3426,
	3426,
	3426,
	2798,
	2798,
	3426,
	471,
	470,
	3426,
	3426,
	3426,
	3426,
	1410,
	2829,
	1410,
	3426,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	45,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
