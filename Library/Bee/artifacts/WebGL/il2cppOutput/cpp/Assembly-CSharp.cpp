﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<TMPro.TMP_TextInfo>
struct Action_1_tB93AB717F9D419A1BEC832FF76E74EAA32184CC1;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t5C96F4B6841710A9013966F76224BAE01FB4B4D1;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>
struct Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>
struct Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>
struct Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB;
// System.Func`3<System.Int32,System.String,TMPro.TMP_FontAsset>
struct Func_3_tC721DF8CDD07ED66A4833A19A2ED2302608C906C;
// System.Func`3<System.Int32,System.String,TMPro.TMP_SpriteAsset>
struct Func_3_t6F6D9932638EA1A5A45303C6626C818C25D164E5;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2Int>
struct IEqualityComparer_1_t4275A3D7B86C2D3C66842AB0700881FB24837F2D;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Int32>
struct KeyCollection_tCC15D033281A6593E2488FAF5B205812A152AC03;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector2Int,Building>
struct KeyCollection_tA35CFE9C33E913BA7FA37F2A063CE6FAC6184D94;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Vector2Int,Resource>
struct KeyCollection_t48A5304428C23A3C7E731CB31F8198CAD7BF59C5;
// System.Collections.Generic.List`1<Building>
struct List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100;
// System.Collections.Generic.List`1<Enemy>
struct List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<UnityEngine.Vector2Int>
struct List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer>
struct UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Int32>
struct ValueCollection_tCE6BD704B9571C131E2D8C8CED569DDEC4AE042B;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,Building>
struct ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,System.Object>
struct ValueCollection_t3EAF721BFEA4055A9E02DCCA9461FB84CDAC2839;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,Resource>
struct ValueCollection_t2FB70E23E6F51D86AB4A0A064D41FCB745D2CBB9;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Int32>[]
struct EntryU5BU5D_tEA0133B78B9FF7045128C508FA50247E525A94D6;
// System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector2Int,Building>[]
struct EntryU5BU5D_t8239DE776726AEC827C58627573237A560C0796A;
// System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector2Int,Resource>[]
struct EntryU5BU5D_t8DEE136E8635E9F99394CED1940C96B2594B98F3;
// TMPro.TMP_TextProcessingStack`1<System.Int32>[]
struct TMP_TextProcessingStack_1U5BU5D_t08293E0BB072311BB96170F351D1083BCA97B9B2;
// Building[]
struct BuildingU5BU5D_t2224E8D46E2056B6FC47768923ABD833D25EA923;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.Color32[]
struct Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259;
// System.Decimal[]
struct DecimalU5BU5D_t93BA0C88FA80728F73B792EE1A5199D0C060B615;
// Enemy[]
struct EnemyU5BU5D_t17F646169909105322498EFD67B34945C9A08B6F;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t2A406B5BAB0DD0F06E7F1773DB062E4AF98067BA;
// TMPro.HighlightState[]
struct HighlightStateU5BU5D_tA878A0AF1F4F52882ACD29515AADC277EE135622;
// TMPro.HorizontalAlignmentOptions[]
struct HorizontalAlignmentOptionsU5BU5D_t4D185662282BFB910D8B9A8199E91578E9422658;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// UnityEngine.Material[]
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t7491D335AB3E3E13CE9C0F5E931F396F6A02E1F2;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// TMPro.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_t5816316EFD8F59DBC30B9F88E15828C564E47B6D;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t297D56FCF66DAA99D8FEA7C30F9F3926902C5B99;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2F65E8C42F268DFF33BB1392D94BCF5B5087308A;
// System.UInt32[]
struct UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector2Int[]
struct Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// TMPro.WordWrapState[]
struct WordWrapStateU5BU5D_t473D59C9DBCC949CE72EF1EB471CBA152A6CEAC9;
// TMPro.TMP_Text/UnicodeChar[]
struct UnicodeCharU5BU5D_t67F27D09F8EB28D2C42DFF16FE60054F157012F5;
// Building
struct Building_t950D5394E080624D7E96B158EF852EA16ADB3650;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// Enemy
struct Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// TMPro.ITextPreprocessor
struct ITextPreprocessor_tDBB49C8B68D7B80E8D233B9D9666C43981EFAAB9;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// Resource
struct Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B;
// System.String
struct String_t;
// TMPro.TMP_Character
struct TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2E0F016A61CA343E3222FF51E7CF0E53F9F256E4;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39;
// TMPro.TMP_Style
struct TMP_Style_tA9E5B1B35EBFE24EF980CEA03251B638282E120C;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t70C71699F5CB2D855C361DBB78A44C901236C859;
// TMPro.TMP_Text
struct TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9;
// TMPro.TMP_TextElement
struct TMP_TextElement_t262A55214F712D4274485ABE5676E5254B84D0A5;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UIManager
struct UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// World
struct World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;

IL2CPP_EXTERN_C RuntimeClass* Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0975C4D64F1CC305CF23880AA82B42D9ED95A9F5;
IL2CPP_EXTERN_C String_t* _stringLiteral176D6BC2760E16DD7F64BE45C450982B10B1D383;
IL2CPP_EXTERN_C String_t* _stringLiteral21B94FD503EEBB29CEFABCEE47FF2025A04C747F;
IL2CPP_EXTERN_C String_t* _stringLiteral2DB3A856213894658C6BE0C83169D791A76B711A;
IL2CPP_EXTERN_C String_t* _stringLiteral338B0F004B0215C76E248CD19D9093E8B98DF996;
IL2CPP_EXTERN_C String_t* _stringLiteral356499BC0CEB40A5F6AB5251F362330D1FDAFB9E;
IL2CPP_EXTERN_C String_t* _stringLiteral37A50091974FE11FAC57C870272F76245820AA18;
IL2CPP_EXTERN_C String_t* _stringLiteral3DFA7A0481C4EAE8561618B53082C3CB56ADBAC8;
IL2CPP_EXTERN_C String_t* _stringLiteral42068B0B535BEE0AD0CBD0E4D92D9B191EDBB05B;
IL2CPP_EXTERN_C String_t* _stringLiteral478FDC724A47DA9391A41A50CD1FC48B472B6D32;
IL2CPP_EXTERN_C String_t* _stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6;
IL2CPP_EXTERN_C String_t* _stringLiteral52423545FA29055BB661EA85311534CFBFB9D8BE;
IL2CPP_EXTERN_C String_t* _stringLiteral5E66CFA2AD0CCFEA2AEC0EB972244661E4D07044;
IL2CPP_EXTERN_C String_t* _stringLiteral60162A6CFC35BDA57A4CCB3D82374ABE78EEF722;
IL2CPP_EXTERN_C String_t* _stringLiteral66AB9EE3DACF3D9819CA98FB792066831612BC34;
IL2CPP_EXTERN_C String_t* _stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C;
IL2CPP_EXTERN_C String_t* _stringLiteralBB0AD22536D32FF7431173B3D5B6BAD1CDDD4854;
IL2CPP_EXTERN_C String_t* _stringLiteralBDD9E455CEE99D40D2BEDCEB2AE73A3D2753B9CD;
IL2CPP_EXTERN_C String_t* _stringLiteralBF6181E7BB6256EBF41D34F7C1FA1BA4F3C52B0B;
IL2CPP_EXTERN_C String_t* _stringLiteralC1771FD048FA0C5283A6D1085A6C3493F05C1302;
IL2CPP_EXTERN_C String_t* _stringLiteralC613D4D2FE3F5D74727D376F793286A2BCBB1391;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE5E7E6077E71E42D4DFCC7DD7E24819AAA53A939;
IL2CPP_EXTERN_C String_t* _stringLiteralF2A62543E7D1614AA84BFB81CDF2C39038A8A55A;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mAEDD6BBEE1B37BC5E1D803803352FBE4CF4D3D7E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_mB433D0E9F8D7BD254D407A727943F2E4AF06903A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m4AABA65DE32F60D9255F00B071978999DD6FDFF6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mA3C3860EDE2CDD08BBD68C389377BC89D029D968_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mAF6F73F9A891EF55C0B1EB63C20821FDFC88CD37_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Count_m692126D37315CC3D80C4DA48688E8F35AEC755B0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Values_m5A2A31F10CA4E1C998872AE5B7487A67D7F942E6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m18C98B2C5838EFF440390391FFCEB7AE47933032_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mCC2518456C91884B2443C580C017215822081CC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m5C98FB58633AC2356A6CF21086801ED99B68B8AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m9B53C3D04EDC545CD4D52D8F65FA2321BD32FB45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mA4B5C4A5DCF31EBE875E6B262275A1A311D7589C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF19A0C6189F7B91B8CCD0DDC457D66F2A161BA18_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m1AF923779EA798B6D54040FA67531B1F5F1EC323_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mE7CABDCD96578731F6D0B96B9CBD16A2DD495AA7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisEnemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB_m88CA17308ECE2A8FD72E8EABE0DA90718A0FFA2F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m5212A0E073717BF79B93BBA707AB20B862DEC2C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m2AE956CB99C7BB4884AEC1401F26720C40069AFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m0A8258727C1434280EB076545170B0CC4F0904C9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m84DCF90974973450ABD8110FD9B80101AD2FBDF4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_mE25E8E89562CD7120066447B2DC1F0D8DCBD26E5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m8B9D30B8AD9D2A8297CBE27DBB8144B406427EC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ValueCollection_GetEnumerator_m591A80270DBA6B1DBF9B407FD2F75CE11E5A8282_RuntimeMethod_var;

struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_tEA0133B78B9FF7045128C508FA50247E525A94D6* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tCC15D033281A6593E2488FAF5B205812A152AC03* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tCE6BD704B9571C131E2D8C8CED569DDEC4AE042B* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>
struct Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t8239DE776726AEC827C58627573237A560C0796A* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tA35CFE9C33E913BA7FA37F2A063CE6FAC6184D94* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>
struct Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t8DEE136E8635E9F99394CED1940C96B2594B98F3* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t48A5304428C23A3C7E731CB31F8198CAD7BF59C5* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t2FB70E23E6F51D86AB4A0A064D41FCB745D2CBB9* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<Building>
struct List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	BuildingU5BU5D_t2224E8D46E2056B6FC47768923ABD833D25EA923* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<Enemy>
struct List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	EnemyU5BU5D_t17F646169909105322498EFD67B34945C9A08B6F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<UnityEngine.Vector2Int>
struct List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,Building>
struct ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::_dictionary
	Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ____dictionary_0;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<Building>
struct Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<Enemy>
struct Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	RuntimeObject* ____current_3;
};

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,Building>
struct Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_dictionary
	Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ____dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_version
	int32_t ____version_2;
	// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_currentValue
	Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ____currentValue_3;
};

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,System.Object>
struct Enumerator_tB8107E6A622783453A40DD6097AB6AF6D76BEDC2 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_dictionary
	Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* ____dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_version
	int32_t ____version_2;
	// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_currentValue
	RuntimeObject* ____currentValue_3;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight>
struct TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	FontWeightU5BU5D_t2A406B5BAB0DD0F06E7F1773DB062E4AF98067BA* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions>
struct TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	HorizontalAlignmentOptionsU5BU5D_t4D185662282BFB910D8B9A8199E91578E9422658* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<System.Int32>
struct TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<System.Single>
struct TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	float ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient>
struct TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2F65E8C42F268DFF33BB1392D94BCF5B5087308A* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB* ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// TMPro.MaterialReference
struct MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B 
{
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;
};
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___spriteAsset_2;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___spriteAsset_2;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// UnityEngine.SceneManagement.Scene
struct Scene_tA1DC762B79745EB5140F054C884855B922318356 
{
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// TMPro.TMP_FontStyleStack
struct TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC 
{
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;
};

// TMPro.TMP_Offset
struct TMP_Offset_t2262BE4E87D9662487777FF8FFE1B17B0E4438C6 
{
	// System.Single TMPro.TMP_Offset::m_Left
	float ___m_Left_0;
	// System.Single TMPro.TMP_Offset::m_Right
	float ___m_Right_1;
	// System.Single TMPro.TMP_Offset::m_Top
	float ___m_Top_2;
	// System.Single TMPro.TMP_Offset::m_Bottom
	float ___m_Bottom_3;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector2Int
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A 
{
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777 
{
	// TMPro.TMP_Character TMPro.TMP_Text/SpecialCharacter::character
	TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35* ___character_0;
	// TMPro.TMP_FontAsset TMPro.TMP_Text/SpecialCharacter::fontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	// UnityEngine.Material TMPro.TMP_Text/SpecialCharacter::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	// System.Int32 TMPro.TMP_Text/SpecialCharacter::materialIndex
	int32_t ___materialIndex_3;
};
// Native definition for P/Invoke marshalling of TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777_marshaled_pinvoke
{
	TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35* ___character_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	int32_t ___materialIndex_3;
};
// Native definition for COM marshalling of TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777_marshaled_com
{
	TMP_Character_t7D37A55EF1A9FF6D0BFE6D50E86A00F80E7FAF35* ___character_0;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___fontAsset_1;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	int32_t ___materialIndex_3;
};

// TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361 
{
	// System.UInt32[] TMPro.TMP_Text/TextBackingContainer::m_Array
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___m_Array_0;
	// System.Int32 TMPro.TMP_Text/TextBackingContainer::m_Count
	int32_t ___m_Count_1;
};
// Native definition for P/Invoke marshalling of TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ___m_Array_0;
	int32_t ___m_Count_1;
};
// Native definition for COM marshalling of TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ___m_Array_0;
	int32_t ___m_Count_1;
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>
struct Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ____current_3;
};

// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32>
struct TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference>
struct TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	MaterialReferenceU5BU5D_t7491D335AB3E3E13CE9C0F5E931F396F6A02E1F2* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	MaterialReference_tFD98FFFBBDF168028E637446C6676507186F4D0B ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// TMPro.Extents
struct Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 
{
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___min_2;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___max_3;
};

// TMPro.HighlightState
struct HighlightState_tE4F50287E5E2E91D42AB77DEA281D88D3AD6A28B 
{
	// UnityEngine.Color32 TMPro.HighlightState::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_0;
	// TMPro.TMP_Offset TMPro.HighlightState::padding
	TMP_Offset_t2262BE4E87D9662487777FF8FFE1B17B0E4438C6 ___padding_1;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;
};

// TMPro.VertexGradient
struct VertexGradient_t2C057B53C0EA6E987C2B7BAB0305E686DA1C9A8F 
{
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___bottomRight_3;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState>
struct TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	HighlightStateU5BU5D_tA878A0AF1F4F52882ACD29515AADC277EE135622* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	HighlightState_tE4F50287E5E2E91D42AB77DEA281D88D3AD6A28B ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// TMPro.TMP_LineInfo
struct TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 
{
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___lineExtents_19;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// TMPro.WordWrapState
struct WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A 
{
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::startOfLineAscender
	float ___startOfLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_14;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_15;
	// System.Single TMPro.WordWrapState::pageAscender
	float ___pageAscender_16;
	// TMPro.HorizontalAlignmentOptions TMPro.WordWrapState::horizontalAlignment
	int32_t ___horizontalAlignment_17;
	// System.Single TMPro.WordWrapState::marginLeft
	float ___marginLeft_18;
	// System.Single TMPro.WordWrapState::marginRight
	float ___marginRight_19;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_20;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_21;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_22;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_23;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_24;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_25;
	// System.Int32 TMPro.WordWrapState::italicAngle
	int32_t ___italicAngle_26;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_27;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_28;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_29;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_30;
	// System.Boolean TMPro.WordWrapState::isDrivenLineSpacing
	bool ___isDrivenLineSpacing_31;
	// System.Single TMPro.WordWrapState::glyphHorizontalAdvanceAdjustment
	float ___glyphHorizontalAdvanceAdjustment_32;
	// System.Single TMPro.WordWrapState::cSpace
	float ___cSpace_33;
	// System.Single TMPro.WordWrapState::mSpace
	float ___mSpace_34;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___textInfo_35;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 ___lineInfo_36;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_37;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_38;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_39;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_40;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___basicStyleStack_41;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::italicAngleStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___italicAngleStack_42;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___colorStack_43;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___underlineColorStack_44;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___strikethroughColorStack_45;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___highlightColorStack_46;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState> TMPro.WordWrapState::highlightStateStack
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___highlightStateStack_47;
	// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___colorGradientStack_48;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___sizeStack_49;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___indentStack_50;
	// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___fontWeightStack_51;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___styleStack_52;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___baselineStack_53;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___actionStack_54;
	// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___materialReferenceStack_55;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___lineJustificationStack_56;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_57;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___currentFontAsset_58;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___currentSpriteAsset_59;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_60;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_61;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___meshExtents_62;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_63;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_64;
};
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___startOfLineAscender_13;
	float ___maxLineAscender_14;
	float ___maxLineDescender_15;
	float ___pageAscender_16;
	int32_t ___horizontalAlignment_17;
	float ___marginLeft_18;
	float ___marginRight_19;
	float ___xAdvance_20;
	float ___preferredWidth_21;
	float ___preferredHeight_22;
	float ___previousLineScale_23;
	int32_t ___wordCount_24;
	int32_t ___fontStyle_25;
	int32_t ___italicAngle_26;
	float ___fontScaleMultiplier_27;
	float ___currentFontSize_28;
	float ___baselineOffset_29;
	float ___lineOffset_30;
	int32_t ___isDrivenLineSpacing_31;
	float ___glyphHorizontalAdvanceAdjustment_32;
	float ___cSpace_33;
	float ___mSpace_34;
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___textInfo_35;
	TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 ___lineInfo_36;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_37;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_38;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_39;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_40;
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___basicStyleStack_41;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___italicAngleStack_42;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___colorStack_43;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___underlineColorStack_44;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___strikethroughColorStack_45;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___highlightColorStack_46;
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___highlightStateStack_47;
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___colorGradientStack_48;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___sizeStack_49;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___indentStack_50;
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___fontWeightStack_51;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___styleStack_52;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___baselineStack_53;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___actionStack_54;
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___materialReferenceStack_55;
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___lineJustificationStack_56;
	int32_t ___spriteAnimationID_57;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___currentFontAsset_58;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___currentSpriteAsset_59;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_60;
	int32_t ___currentMaterialIndex_61;
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___meshExtents_62;
	int32_t ___tagNoParsing_63;
	int32_t ___isNonBreakingSpace_64;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___startOfLineAscender_13;
	float ___maxLineAscender_14;
	float ___maxLineDescender_15;
	float ___pageAscender_16;
	int32_t ___horizontalAlignment_17;
	float ___marginLeft_18;
	float ___marginRight_19;
	float ___xAdvance_20;
	float ___preferredWidth_21;
	float ___preferredHeight_22;
	float ___previousLineScale_23;
	int32_t ___wordCount_24;
	int32_t ___fontStyle_25;
	int32_t ___italicAngle_26;
	float ___fontScaleMultiplier_27;
	float ___currentFontSize_28;
	float ___baselineOffset_29;
	float ___lineOffset_30;
	int32_t ___isDrivenLineSpacing_31;
	float ___glyphHorizontalAdvanceAdjustment_32;
	float ___cSpace_33;
	float ___mSpace_34;
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___textInfo_35;
	TMP_LineInfo_tB75C1965B58DB7B3A046C8CA55AD6AB92B6B17B3 ___lineInfo_36;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_37;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_38;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_39;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_40;
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___basicStyleStack_41;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___italicAngleStack_42;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___colorStack_43;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___underlineColorStack_44;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___strikethroughColorStack_45;
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___highlightColorStack_46;
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___highlightStateStack_47;
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___colorGradientStack_48;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___sizeStack_49;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___indentStack_50;
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___fontWeightStack_51;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___styleStack_52;
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___baselineStack_53;
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___actionStack_54;
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___materialReferenceStack_55;
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___lineJustificationStack_56;
	int32_t ___spriteAnimationID_57;
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___currentFontAsset_58;
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___currentSpriteAsset_59;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_60;
	int32_t ___currentMaterialIndex_61;
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___meshExtents_62;
	int32_t ___tagNoParsing_63;
	int32_t ___isNonBreakingSpace_64;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.WordWrapState>
struct TMP_TextProcessingStack_1_t2DDA00FFC64AF6E3AFD475AB2086D16C34787E0F 
{
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	WordWrapStateU5BU5D_t473D59C9DBCC949CE72EF1EB471CBA152A6CEAC9* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
	// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer> UnityEngine.SpriteRenderer::m_SpriteChangeEvent
	UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E* ___m_SpriteChangeEvent_4;
};

// Building
struct Building_t950D5394E080624D7E96B158EF852EA16ADB3650  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// World Building::world
	World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* ___world_4;
	// System.String Building::bldName
	String_t* ___bldName_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector2Int> Building::bldShape
	List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* ___bldShape_6;
	// System.Int32 Building::maxHP
	int32_t ___maxHP_7;
	// System.Int32 Building::hp
	int32_t ___hp_8;
	// System.Int32 Building::waterCost
	int32_t ___waterCost_9;
	// System.Int32 Building::nCost
	int32_t ___nCost_10;
	// System.Int32 Building::pCost
	int32_t ___pCost_11;
	// System.Int32 Building::kCost
	int32_t ___kCost_12;
	// System.Int32 Building::damage
	int32_t ___damage_13;
	// System.Int32 Building::x
	int32_t ___x_14;
	// System.Int32 Building::y
	int32_t ___y_15;
	// System.Boolean Building::isBeingDestroyed
	bool ___isBeingDestroyed_16;
	// System.Boolean Building::dontAutodestroy
	bool ___dontAutodestroy_17;
	// Enemy Building::closestEnemy
	Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* ___closestEnemy_18;
	// System.Single Building::maxRange
	float ___maxRange_19;
	// System.Int32 Building::extractionSpeed
	int32_t ___extractionSpeed_20;
	// System.Int32 Building::waterNeed
	int32_t ___waterNeed_21;
};

// Enemy
struct Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// World Enemy::world
	World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* ___world_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building> Enemy::bldMap
	Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___bldMap_5;
	// System.Int32 Enemy::maxHP
	int32_t ___maxHP_6;
	// System.Int32 Enemy::hp
	int32_t ___hp_7;
	// System.Int32 Enemy::damage
	int32_t ___damage_8;
	// System.Single Enemy::rotationSpeed
	float ___rotationSpeed_9;
	// System.Single Enemy::moveSpeed
	float ___moveSpeed_10;
	// System.Int32 Enemy::oldMapSize
	int32_t ___oldMapSize_11;
	// UnityEngine.GameObject Enemy::closestBuilding
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___closestBuilding_12;
	// UnityEngine.Quaternion Enemy::targetRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___targetRotation_13;
	// UnityEngine.Vector2 Enemy::targetPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___targetPosition_14;
	// UnityEngine.Vector2 Enemy::posSmoothDampState
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___posSmoothDampState_15;
	// System.Boolean Enemy::reachedDestination
	bool ___reachedDestination_16;
};

// Resource
struct Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String Resource::resName
	String_t* ___resName_4;
	// System.Int32 Resource::stored
	int32_t ___stored_5;
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UIManager
struct UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject UIManager::cam
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cam_4;
	// UnityEngine.Vector2 UIManager::cameraOriginalPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___cameraOriginalPos_5;
	// System.Single UIManager::camOffsetY
	float ___camOffsetY_6;
	// World UIManager::world
	World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* ___world_7;
	// UnityEngine.Vector2 UIManager::reference_res
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___reference_res_8;
	// UnityEngine.Vector2 UIManager::current_res
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___current_res_9;
	// TMPro.TMP_Text UIManager::waterText
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___waterText_10;
	// TMPro.TMP_Text UIManager::nText
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___nText_11;
	// UnityEngine.Vector2 UIManager::mousePos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___mousePos_12;
	// UnityEngine.Vector2 UIManager::oldMousePos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oldMousePos_13;
	// UnityEngine.Vector2Int UIManager::selectedTile
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___selectedTile_14;
	// UnityEngine.GameObject UIManager::rootTile
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___rootTile_15;
	// UnityEngine.GameObject UIManager::pumpTile
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___pumpTile_16;
	// UnityEngine.GameObject UIManager::bldToPlace
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___bldToPlace_17;
	// UnityEngine.GameObject UIManager::gameOverScreen
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameOverScreen_18;
};

// World
struct World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UIManager World::uiManager
	UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* ___uiManager_4;
	// System.Int32 World::water
	int32_t ___water_5;
	// System.Int32 World::nitrogen
	int32_t ___nitrogen_6;
	// System.Int32 World::waterPerS
	int32_t ___waterPerS_7;
	// System.Int32 World::nitrogenPerS
	int32_t ___nitrogenPerS_8;
	// UnityEngine.Vector2Int World::screenGridSize
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___screenGridSize_9;
	// UnityEngine.GameObject World::cursor
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cursor_10;
	// UnityEngine.Transform World::backgroundParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___backgroundParent_11;
	// UnityEngine.Transform World::oresParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___oresParent_12;
	// UnityEngine.Transform World::buildingsParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___buildingsParent_13;
	// UnityEngine.Transform World::enemiesParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___enemiesParent_14;
	// System.Int32 World::lastBgLayerDrawn
	int32_t ___lastBgLayerDrawn_15;
	// UnityEngine.Vector2 World::randomRange
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___randomRange_16;
	// UnityEngine.Vector2 World::bgPerlinScale
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bgPerlinScale_17;
	// UnityEngine.Vector2 World::oreNPerlinScale
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oreNPerlinScale_18;
	// UnityEngine.Vector2 World::orePPerlinScale
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___orePPerlinScale_19;
	// UnityEngine.Vector2 World::oreKPerlinScale
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oreKPerlinScale_20;
	// System.Single World::oreNtreshold
	float ___oreNtreshold_21;
	// UnityEngine.Vector2 World::bgPerlinOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bgPerlinOffset_22;
	// UnityEngine.Vector2 World::oreNPerlinOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oreNPerlinOffset_23;
	// UnityEngine.Vector2 World::orePPerlinOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___orePPerlinOffset_24;
	// UnityEngine.Vector2 World::oreKPerlinOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oreKPerlinOffset_25;
	// UnityEngine.GameObject World::originalTile
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___originalTile_26;
	// UnityEngine.GameObject World::dirtTile
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___dirtTile_27;
	// UnityEngine.GameObject World::oreNTile
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___oreNTile_28;
	// UnityEngine.GameObject World::orePTile
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___orePTile_29;
	// UnityEngine.GameObject World::oreKTile
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___oreKTile_30;
	// System.Collections.Generic.List`1<Building> World::bldList
	List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* ___bldList_31;
	// System.Collections.Generic.List`1<Enemy> World::enemyList
	List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* ___enemyList_32;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource> World::bgMap
	Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___bgMap_33;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource> World::oreMap
	Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___oreMap_34;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building> World::bldMap
	Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___bldMap_35;
	// UnityEngine.Vector2 World::testPerlinOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___testPerlinOffset_36;
	// UnityEngine.Vector2 World::testPerlinScale
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___testPerlinScale_37;
	// System.Int32 World::maxBldY
	int32_t ___maxBldY_38;
	// System.Single World::enemySpawnChance
	float ___enemySpawnChance_39;
	// UnityEngine.GameObject World::enemyBasic
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___enemyBasic_40;
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTargetCache
	bool ___m_RaycastTargetCache_11;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_12;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_13;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_14;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_15;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_16;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_19;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_20;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_23;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_24;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_25;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_26;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_27;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_28;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_31;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_32;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_33;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_34;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_35;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_36;
};

// TMPro.TMP_Text
struct TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_37;
	// System.Boolean TMPro.TMP_Text::m_IsTextBackingStringDirty
	bool ___m_IsTextBackingStringDirty_38;
	// TMPro.ITextPreprocessor TMPro.TMP_Text::m_TextPreprocessor
	RuntimeObject* ___m_TextPreprocessor_39;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_40;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___m_fontAsset_41;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t923BF2F78D7C5AC36376E168A1193B7CB4855160* ___m_currentFontAsset_42;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_43;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_sharedMaterial_44;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_currentMaterial_45;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_49;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___m_fontSharedMaterials_50;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_fontMaterial_51;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___m_fontMaterials_52;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_53;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_fontColor32_54;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_fontColor_55;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_underlineColor_57;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_strikethroughColor_58;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_59;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_60;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t2C057B53C0EA6E987C2B7BAB0305E686DA1C9A8F ___m_fontColorGradient_61;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB* ___m_fontColorGradientPreset_62;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___m_spriteAsset_63;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_64;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_65;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_spriteColor_66;
	// TMPro.TMP_StyleSheet TMPro.TMP_Text::m_StyleSheet
	TMP_StyleSheet_t70C71699F5CB2D855C361DBB78A44C901236C859* ___m_StyleSheet_67;
	// TMPro.TMP_Style TMPro.TMP_Text::m_TextStyle
	TMP_Style_tA9E5B1B35EBFE24EF980CEA03251B638282E120C* ___m_TextStyle_68;
	// System.Int32 TMPro.TMP_Text::m_TextStyleHashCode
	int32_t ___m_TextStyleHashCode_69;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_70;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_faceColor_71;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_outlineColor_72;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_73;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_74;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_75;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_76;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___m_sizeStack_77;
	// TMPro.FontWeight TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_78;
	// TMPro.FontWeight TMPro.TMP_Text::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_79;
	// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight> TMPro.TMP_Text::m_FontWeightStack
	TMP_TextProcessingStack_1_tA5C8CED87DD9E73F6359E23B334FFB5B6F813FD4 ___m_FontWeightStack_80;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_81;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_82;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_83;
	// System.Int32 TMPro.TMP_Text::m_AutoSizeIterationCount
	int32_t ___m_AutoSizeIterationCount_84;
	// System.Int32 TMPro.TMP_Text::m_AutoSizeMaxIterationCount
	int32_t ___m_AutoSizeMaxIterationCount_85;
	// System.Boolean TMPro.TMP_Text::m_IsAutoSizePointSizeSet
	bool ___m_IsAutoSizePointSizeSet_86;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_87;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_88;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_89;
	// TMPro.FontStyles TMPro.TMP_Text::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_90;
	// TMPro.TMP_FontStyleStack TMPro.TMP_Text::m_fontStyleStack
	TMP_FontStyleStack_t52885F172FADBC21346C835B5302167BDA8020DC ___m_fontStyleStack_91;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_92;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_Text::m_HorizontalAlignment
	int32_t ___m_HorizontalAlignment_93;
	// TMPro.VerticalAlignmentOptions TMPro.TMP_Text::m_VerticalAlignment
	int32_t ___m_VerticalAlignment_94;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_95;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_96;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_TextProcessingStack_1_t243EA1B5D7FD2295D6533B953F0BBE8F52EFB8A0 ___m_lineJustificationStack_97;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_textContainerLocalCorners_98;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_99;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_100;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_101;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_102;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_103;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_104;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_105;
	// System.Boolean TMPro.TMP_Text::m_IsDrivenLineSpacing
	bool ___m_IsDrivenLineSpacing_106;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_107;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_108;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_109;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_110;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_111;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_112;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_113;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_114;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_115;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_116;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_117;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___m_linkedTextComponent_118;
	// TMPro.TMP_Text TMPro.TMP_Text::parentLinkedComponent
	TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* ___parentLinkedComponent_119;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_120;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_121;
	// System.Single TMPro.TMP_Text::m_GlyphHorizontalAdvanceAdjustment
	float ___m_GlyphHorizontalAdvanceAdjustment_122;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_123;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_124;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_125;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_126;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_127;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_128;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_129;
	// System.Boolean TMPro.TMP_Text::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_130;
	// System.Boolean TMPro.TMP_Text::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_131;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_132;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_133;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_134;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_135;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_136;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_137;
	// System.Boolean TMPro.TMP_Text::m_IsTextObjectScaleStatic
	bool ___m_IsTextObjectScaleStatic_138;
	// System.Boolean TMPro.TMP_Text::m_VertexBufferAutoSizeReduction
	bool ___m_VertexBufferAutoSizeReduction_139;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_140;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_141;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_142;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_143;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_144;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_145;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_146;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_margin_147;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_148;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_149;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_150;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_151;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_152;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t09A8E906329422C3F0C059876801DD695B8D524D* ___m_textInfo_153;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_154;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_155;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___m_transform_156;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_rectTransform_157;
	// UnityEngine.Vector2 TMPro.TMP_Text::m_PreviousRectTransformSize
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PreviousRectTransformSize_158;
	// UnityEngine.Vector2 TMPro.TMP_Text::m_PreviousPivotPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PreviousPivotPosition_159;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_160;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_161;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_mesh_162;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_163;
	// System.Action`1<TMPro.TMP_TextInfo> TMPro.TMP_Text::OnPreRenderText
	Action_1_tB93AB717F9D419A1BEC832FF76E74EAA32184CC1* ___OnPreRenderText_166;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2E0F016A61CA343E3222FF51E7CF0E53F9F256E4* ___m_spriteAnimator_167;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_168;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_169;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_170;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_171;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_172;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_173;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___m_LayoutElement_174;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_175;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_176;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_177;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_178;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_179;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_180;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_181;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_182;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_183;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_184;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_185;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_186;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_187;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_191;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_192;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___m_indentStack_193;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_194;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_195;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_FXMatrix_196;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_197;
	// TMPro.TMP_Text/UnicodeChar[] TMPro.TMP_Text::m_TextProcessingArray
	UnicodeCharU5BU5D_t67F27D09F8EB28D2C42DFF16FE60054F157012F5* ___m_TextProcessingArray_198;
	// System.Int32 TMPro.TMP_Text::m_InternalTextProcessingArraySize
	int32_t ___m_InternalTextProcessingArraySize_199;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t297D56FCF66DAA99D8FEA7C30F9F3926902C5B99* ___m_internalCharacterInfo_200;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_201;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_208;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_209;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_210;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_211;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_212;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_213;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_214;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_215;
	// System.Single TMPro.TMP_Text::m_PageAscender
	float ___m_PageAscender_216;
	// System.Single TMPro.TMP_Text::m_maxTextAscender
	float ___m_maxTextAscender_217;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_218;
	// System.Single TMPro.TMP_Text::m_ElementAscender
	float ___m_ElementAscender_219;
	// System.Single TMPro.TMP_Text::m_ElementDescender
	float ___m_ElementDescender_220;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_221;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_222;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_223;
	// System.Single TMPro.TMP_Text::m_startOfLineDescender
	float ___m_startOfLineDescender_224;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_225;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tA2D2F95811D0A18CB7AC3570D2D8F8CD3AF4C4A8 ___m_meshExtents_226;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_htmlColor_227;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___m_colorStack_228;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___m_underlineColorStack_229;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_TextProcessingStack_1_tF2CD5BE59E5EB22EA9E3EE3043A004EA918C4BB3 ___m_strikethroughColorStack_230;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState> TMPro.TMP_Text::m_HighlightStateStack
	TMP_TextProcessingStack_1_t57AECDCC936A7FF1D6CF66CA11560B28A675648D ___m_HighlightStateStack_231;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t17B51752B4E9499A1FF7D875DCEC1D15A0F4AEBB* ___m_colorGradientPreset_232;
	// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_TextProcessingStack_1_tC8FAEB17246D3B171EFD11165A5761AE39B40D0C ___m_colorGradientStack_233;
	// System.Boolean TMPro.TMP_Text::m_colorGradientPresetIsTinted
	bool ___m_colorGradientPresetIsTinted_234;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_235;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_236;
	// TMPro.TMP_TextProcessingStack`1<System.Int32>[] TMPro.TMP_Text::m_TextStyleStacks
	TMP_TextProcessingStack_1U5BU5D_t08293E0BB072311BB96170F351D1083BCA97B9B2* ___m_TextStyleStacks_237;
	// System.Int32 TMPro.TMP_Text::m_TextStyleStackDepth
	int32_t ___m_TextStyleStackDepth_238;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.TMP_Text::m_ItalicAngleStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___m_ItalicAngleStack_239;
	// System.Int32 TMPro.TMP_Text::m_ItalicAngle
	int32_t ___m_ItalicAngle_240;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_TextProcessingStack_1_tFBA719426D68CE1F2B5849D97AF5E5D65846290C ___m_actionStack_241;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_242;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_243;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_TextProcessingStack_1_t138EC06BE7F101AA0A3C8D2DC951E55AACE085E9 ___m_baselineOffsetStack_244;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_245;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_246;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t262A55214F712D4274485ABE5676E5254B84D0A5* ___m_cached_TextElement_247;
	// TMPro.TMP_Text/SpecialCharacter TMPro.TMP_Text::m_Ellipsis
	SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777 ___m_Ellipsis_248;
	// TMPro.TMP_Text/SpecialCharacter TMPro.TMP_Text::m_Underline
	SpecialCharacter_t6C1DBE8C490706D1620899BAB7F0B8091AD26777 ___m_Underline_249;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___m_defaultSpriteAsset_250;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t81F779E6F705CE190DC0D1F93A954CB8B1774B39* ___m_currentSpriteAsset_251;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_252;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_253;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_254;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_257;
	// TMPro.TMP_Text/TextBackingContainer TMPro.TMP_Text::m_TextBackingArray
	TextBackingContainer_t33D1CE628E7B26C45EDAC1D87BEF2DD22A5C6361 ___m_TextBackingArray_258;
	// System.Decimal[] TMPro.TMP_Text::k_Power
	DecimalU5BU5D_t93BA0C88FA80728F73B792EE1A5199D0C060B615* ___k_Power_259;
};

// <Module>

// <Module>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>

// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>

// System.Collections.Generic.List`1<Building>
struct List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	BuildingU5BU5D_t2224E8D46E2056B6FC47768923ABD833D25EA923* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<Building>

// System.Collections.Generic.List`1<Enemy>
struct List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	EnemyU5BU5D_t17F646169909105322498EFD67B34945C9A08B6F* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<Enemy>

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>

// System.Collections.Generic.List`1<UnityEngine.Vector2Int>
struct List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.Vector2Int>

// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,Building>

// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,Building>

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.Collections.Generic.List`1/Enumerator<Building>

// System.Collections.Generic.List`1/Enumerator<Building>

// System.Collections.Generic.List`1/Enumerator<Enemy>

// System.Collections.Generic.List`1/Enumerator<Enemy>

// System.Collections.Generic.List`1/Enumerator<System.Object>

// System.Collections.Generic.List`1/Enumerator<System.Object>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,Building>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,Building>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,System.Object>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,System.Object>

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// UnityEngine.Color

// UnityEngine.Color

// System.Double

// System.Double

// System.Int32

// System.Int32

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Quaternion

// UnityEngine.SceneManagement.Scene

// UnityEngine.SceneManagement.Scene

// System.Single

// System.Single

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// UnityEngine.Vector2Int
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_StaticFields
{
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Right_7;
};

// UnityEngine.Vector2Int

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// System.Void

// System.Void

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// UnityEngine.Component

// UnityEngine.Component

// UnityEngine.GameObject

// UnityEngine.GameObject

// UnityEngine.Transform

// UnityEngine.Transform

// UnityEngine.MonoBehaviour

// UnityEngine.MonoBehaviour

// UnityEngine.SpriteRenderer

// UnityEngine.SpriteRenderer

// Building

// Building

// Enemy

// Enemy

// Resource

// Resource

// UIManager

// UIManager

// World

// World

// TMPro.TMP_Text
struct TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9_StaticFields
{
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t7491D335AB3E3E13CE9C0F5E931F396F6A02E1F2* ___m_materialReferences_46;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ___m_materialReferenceIndexLookup_47;
	// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_TextProcessingStack_1_tB03E08F69415B281A5A81138F09E49EE58402DF9 ___m_materialReferenceStack_48;
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_colorWhite_56;
	// System.Func`3<System.Int32,System.String,TMPro.TMP_FontAsset> TMPro.TMP_Text::OnFontAssetRequest
	Func_3_tC721DF8CDD07ED66A4833A19A2ED2302608C906C* ___OnFontAssetRequest_164;
	// System.Func`3<System.Int32,System.String,TMPro.TMP_SpriteAsset> TMPro.TMP_Text::OnSpriteAssetRequest
	Func_3_t6F6D9932638EA1A5A45303C6626C818C25D164E5* ___OnSpriteAssetRequest_165;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_htmlTag_188;
	// TMPro.RichTextTagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	RichTextTagAttributeU5BU5D_t5816316EFD8F59DBC30B9F88E15828C564E47B6D* ___m_xmlAttribute_189;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___m_attributeParameterValues_190;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedWordWrapState_202;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedLineState_203;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedEllipsisState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedEllipsisState_204;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLastValidState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedLastValidState_205;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedSoftLineBreakState
	WordWrapState_t80F67D8CAA9B1A0A3D5266521E23A9F3100EDD0A ___m_SavedSoftLineBreakState_206;
	// TMPro.TMP_TextProcessingStack`1<TMPro.WordWrapState> TMPro.TMP_Text::m_EllipsisInsertionCandidateStack
	TMP_TextProcessingStack_1_t2DDA00FFC64AF6E3AFD475AB2086D16C34787E0F ___m_EllipsisInsertionCandidateStack_207;
	// Unity.Profiling.ProfilerMarker TMPro.TMP_Text::k_ParseTextMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_ParseTextMarker_255;
	// Unity.Profiling.ProfilerMarker TMPro.TMP_Text::k_InsertNewLineMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_InsertNewLineMarker_256;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___k_LargePositiveVector2_260;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___k_LargeNegativeVector2_261;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_262;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_263;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_264;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_265;
};

// TMPro.TMP_Text
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2Int[]
struct Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A m_Items[1];

	inline Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m517E7F9D104FEAE6646EABDDC9C852510E86077C_gshared (Dictionary_2_t5C96F4B6841710A9013966F76224BAE01FB4B4D1* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>::ContainsKey(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m79B18A0589BCFE1AB0C51AC7109CA7DA9899371C_gshared (Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>::get_Item(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Dictionary_2_get_Item_m1A1540A3C6915FC270E142FE5402E7B0B45D9F2A_gshared (Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m72CC2F1213D1C1B8ABEDE31082D07B67EC873B13_gshared (Dictionary_2_t5C96F4B6841710A9013966F76224BAE01FB4B4D1* __this, RuntimeObject* ___0_key, int32_t ___1_value, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2Int>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_gshared (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_gshared (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_gshared_inline (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258* __this, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Item_mA019F7A495B48EF2A6E5D36977DB3EA09A47ECDB_gshared (Dictionary_2_t5C96F4B6841710A9013966F76224BAE01FB4B4D1* __this, RuntimeObject* ___0_key, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_gshared (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2Int>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF_gshared (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2Int>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_gshared_inline (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_item, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>::get_Values()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueCollection_t3EAF721BFEA4055A9E02DCCA9461FB84CDAC2839* Dictionary_2_get_Values_mAECD12AB4B2CA570DDCE7082FE55700209E980FD_gshared (Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB8107E6A622783453A40DD6097AB6AF6D76BEDC2 ValueCollection_GetEnumerator_m48A7815F3940780D12F4058217B9B9F5363681E3_gshared (ValueCollection_t3EAF721BFEA4055A9E02DCCA9461FB84CDAC2839* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mD119854EFDDE1B94B4AF75CF966A0835365CB683_gshared (Enumerator_tB8107E6A622783453A40DD6097AB6AF6D76BEDC2* __this, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mBE60E91E0A30EA49AB81231986E0D5C3A5A94B98_gshared_inline (Enumerator_tB8107E6A622783453A40DD6097AB6AF6D76BEDC2* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m80450D57A5E2149E8F567D5B4A3C6E14A599D0C6_gshared (Enumerator_tB8107E6A622783453A40DD6097AB6AF6D76BEDC2* __this, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_m0BF0D9886A59612D34D7513953B126CDE321EE85_gshared (Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mBA9343523B9FC2B73E1296469B8E4777EF816CFA_gshared (Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>::Remove(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_mDA0670941EFD4847344D5656EF43A63D3E52F766_gshared (Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_Instantiate_TisRuntimeObject_m5F38AE6B74636F569647D545E365C5579E5F59CE_gshared (RuntimeObject* ___0_original, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_position, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___2_rotation, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___3_parent, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,System.Object>::set_Item(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_mD6FA021D0D92DC50FF21204FC428639799DCF3A6_gshared (Dictionary_2_t9960A3ACE6FAE1073261A9154F09FA1C2AEEA832* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m1087B74B4FF5004CBB6CC864FF1C87B6DB138505_gshared (Dictionary_2_t5C96F4B6841710A9013966F76224BAE01FB4B4D1* __this, RuntimeObject* ___0_key, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;

// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m8C940F3CFC42866709D7CA931B3D77B4BE94BCB6 (String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_InvokeRepeating_mF208501E0E4918F9168BBBA5FC50D8F80D01514D (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, String_t* ___0_methodName, float ___1_time, float ___2_repeatRate, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1 (String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) ;
// System.Void Building::findClosestEnemy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_findClosestEnemy_m78B19B59C3D6D34967083BD8002C0E7491A55929 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_x, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_y, const RuntimeMethod* method) ;
// System.Void Enemy::takeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_takeDamage_m252CF45B4ADCFE8158C3CF5C8127C8A5C06B7A6F (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, int32_t ___0_damage, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Enemy>::GetEnumerator()
inline Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8 List_1_GetEnumerator_m0A8258727C1434280EB076545170B0CC4F0904C9 (List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8 (*) (List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Enemy>::Dispose()
inline void Enumerator_Dispose_m9B53C3D04EDC545CD4D52D8F65FA2321BD32FB45 (Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<Enemy>::get_Current()
inline Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* Enumerator_get_Current_m1AF923779EA798B6D54040FA67531B1F5F1EC323_inline (Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8* __this, const RuntimeMethod* method)
{
	return ((  Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* (*) (Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_v, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Distance_mBACBB1609E1894D68F882D86A93519E311810C89_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<Enemy>::MoveNext()
inline bool Enumerator_MoveNext_mF19A0C6189F7B91B8CCD0DDC457D66F2A161BA18 (Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Void UnityEngine.Vector2Int::.ctor(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, int32_t ___0_x, int32_t ___1_y, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor()
inline void Dictionary_2__ctor_mA3C3860EDE2CDD08BBD68C389377BC89D029D968 (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588*, const RuntimeMethod*))Dictionary_2__ctor_m517E7F9D104FEAE6646EABDDC9C852510E86077C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, const RuntimeMethod*))Dictionary_2_ContainsKey_m79B18A0589BCFE1AB0C51AC7109CA7DA9899371C_gshared)(__this, ___0_key, method);
}
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>::get_Item(TKey)
inline Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method)
{
	return ((  Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* (*) (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, const RuntimeMethod*))Dictionary_2_get_Item_m1A1540A3C6915FC270E142FE5402E7B0B45D9F2A_gshared)(__this, ___0_key, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2 (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* __this, String_t* ___0_key, int32_t ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588*, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_set_Item_m72CC2F1213D1C1B8ABEDE31082D07B67EC873B13_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2Int>::GetEnumerator()
inline Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321 (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 (*) (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D*, const RuntimeMethod*))List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>::Dispose()
inline void Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258*, const RuntimeMethod*))Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>::get_Current()
inline Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258* __this, const RuntimeMethod* method)
{
	return ((  Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A (*) (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258*, const RuntimeMethod*))Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_gshared_inline)(__this, method);
}
// UnityEngine.Vector2Int UnityEngine.Vector2Int::op_Addition(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_a, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___1_b, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Int32>::get_Item(TKey)
inline int32_t Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592 (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* __this, String_t* ___0_key, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_mA019F7A495B48EF2A6E5D36977DB3EA09A47ECDB_gshared)(__this, ___0_key, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2Int>::MoveNext()
inline bool Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5 (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258*, const RuntimeMethod*))Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6 (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.Void Building::destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_destroy_m1BDC7A8FBECE0B0B13C491541AE704D9AF47EF2A (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, const RuntimeMethod* method) ;
// System.Void World::buildingDestroyed(Building)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_buildingDestroyed_mF07CB8463394DD626C024B5B66C935FD901BA8B1 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ___0_b, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_obj, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2Int>::.ctor()
inline void List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D*, const RuntimeMethod*))List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF_gshared)(__this, method);
}
// System.Int32 UnityEngine.Vector2Int::get_x()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Vector2Int::get_y()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2Int>::Add(T)
inline void List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, const RuntimeMethod*))List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_gshared_inline)(__this, ___0_item, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, const RuntimeMethod*))Dictionary_2_ContainsKey_m79B18A0589BCFE1AB0C51AC7109CA7DA9899371C_gshared)(__this, ___0_key, method);
}
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>::get_Item(TKey)
inline Building_t950D5394E080624D7E96B158EF852EA16ADB3650* Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014 (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method)
{
	return ((  Building_t950D5394E080624D7E96B158EF852EA16ADB3650* (*) (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, const RuntimeMethod*))Dictionary_2_get_Item_m1A1540A3C6915FC270E142FE5402E7B0B45D9F2A_gshared)(__this, ___0_key, method);
}
// System.String UnityEngine.Vector2Int::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Vector2Int_ToString_m6F7E9B9B45A473FED501EB8B8B25BA1FE26DD5D4 (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9E3155FB84015C823606188F53B47CB44C444991 (String_t* ___0_str0, String_t* ___1_str1, const RuntimeMethod* method) ;
// System.Boolean Building::isConnectedToRoot(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Building_isConnectedToRoot_m2531738249902042141A601B72611A949EE3B403 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_x, int32_t ___1_y, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___2_bldMap, const RuntimeMethod* method) ;
// System.Boolean Building::lifeCheck(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Building_lifeCheck_mA5CC9AFDC20E50645D5DC250612391A16DAEE268 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_x, int32_t ___1_y, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___2_bgMap, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___3_oreMap, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___4_bldMap, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>::get_Values()
inline ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA* Dictionary_2_get_Values_m5A2A31F10CA4E1C998872AE5B7487A67D7F942E6 (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* __this, const RuntimeMethod* method)
{
	return ((  ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA* (*) (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*, const RuntimeMethod*))Dictionary_2_get_Values_mAECD12AB4B2CA570DDCE7082FE55700209E980FD_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Vector2Int,Building>::GetEnumerator()
inline Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8 ValueCollection_GetEnumerator_m591A80270DBA6B1DBF9B407FD2F75CE11E5A8282 (ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8 (*) (ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA*, const RuntimeMethod*))ValueCollection_GetEnumerator_m48A7815F3940780D12F4058217B9B9F5363681E3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,Building>::Dispose()
inline void Enumerator_Dispose_m5C98FB58633AC2356A6CF21086801ED99B68B8AA (Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8*, const RuntimeMethod*))Enumerator_Dispose_mD119854EFDDE1B94B4AF75CF966A0835365CB683_gshared)(__this, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,Building>::get_Current()
inline Building_t950D5394E080624D7E96B158EF852EA16ADB3650* Enumerator_get_Current_mE7CABDCD96578731F6D0B96B9CBD16A2DD495AA7_inline (Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8* __this, const RuntimeMethod* method)
{
	return ((  Building_t950D5394E080624D7E96B158EF852EA16ADB3650* (*) (Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8*, const RuntimeMethod*))Enumerator_get_Current_mBE60E91E0A30EA49AB81231986E0D5C3A5A94B98_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector2Int,Building>::MoveNext()
inline bool Enumerator_MoveNext_mA4B5C4A5DCF31EBE875E6B262275A1A311D7589C (Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8*, const RuntimeMethod*))Enumerator_MoveNext_m80450D57A5E2149E8F567D5B4A3C6E14A599D0C6_gshared)(__this, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5 (int32_t* __this, const RuntimeMethod* method) ;
// System.Void World::enemyDestroyed(Enemy)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_enemyDestroyed_m3FB2E482EE7DF4CDC24070A2E0A4E5858EC8FB93 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* ___0_e, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>::get_Count()
inline int32_t Dictionary_2_get_Count_m692126D37315CC3D80C4DA48688E8F35AEC755B0 (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*, const RuntimeMethod*))Dictionary_2_get_Count_m0BF0D9886A59612D34D7513953B126CDE321EE85_gshared)(__this, method);
}
// System.Void Enemy::findClosestBuilding()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_findClosestBuilding_m922525EB6197C6DB29F06254584A361F3FFFEB53 (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Subtraction_m44475FCDAD2DA2F98D78A6625EC2DCDFE8803837_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::SignedAngle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_SignedAngle_mAE9940DA6BC6B2182BA95C299B2EC19967B7D438_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_from, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_to, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m9262AB29E3E9CE94EF71051F38A28E82AEC73F90_inline (float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_up_mE47A9D9D96422224DD0539AA5524DA5440145BB2 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<Building>()
inline Building_t950D5394E080624D7E96B158EF852EA16ADB3650* GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Building_t950D5394E080624D7E96B158EF852EA16ADB3650* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void Building::takeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_takeDamage_m6ECB33F272DFAFEACC917CB29402FD33D8888016 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_damage, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::SmoothDamp(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2&,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_SmoothDamp_m305987D454BC6672AC765DB56A2843D4951865AD_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_current, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_target, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___2_currentVelocity, float ___3_smoothTime, float ___4_maxSpeed, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_v, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865 (const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_RotateTowards_m50EF9D609C80CD423CDA856EA3481DE2004633A3_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_from, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_to, float ___2_maxDegreesDelta, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9 (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9 (const RuntimeMethod* method) ;
// System.Void UIManager::updateResolution()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_updateResolution_m268488AC60B2A2A8282D9955A2613AB941C72839 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___0_value, const RuntimeMethod* method) ;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_tA1DC762B79745EB5140F054C884855B922318356 SceneManager_GetActiveScene_m0B320EC4302F51A71495D1CCD1A0FF9C2ED1FDC8 (const RuntimeMethod* method) ;
// System.String UnityEngine.SceneManagement.Scene::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Scene_get_name_m3C818DFA663E159274DAD823B780C7616C5E2A8C (Scene_tA1DC762B79745EB5140F054C884855B922318356* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E (String_t* ___0_sceneName, const RuntimeMethod* method) ;
// System.Void UIManager::handleCanvasResizing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_handleCanvasResizing_m6BCF100C3BEF15D5AAC823E069AE594F4E313CD0 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) ;
// System.Void UIManager::handleMousePositioning()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_handleMousePositioning_m2C6C95E30F8DA4E45E71451997FC85742695FEB7 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) ;
// System.Void UIManager::handleMouseScrolling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_handleMouseScrolling_mB1B9E856F356252E3D197F35BB5784F1EB2638B2 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) ;
// System.String System.Single::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m3F2C4433B6ADFA5ED8E3F14ED19CD23014E5179D (float* __this, String_t* ___0_format, const RuntimeMethod* method) ;
// System.String UIManager::compressResourceText(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, int32_t ___0_val, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987 (String_t* ___0_format, RuntimeObject* ___1_arg0, RuntimeObject* ___2_arg1, const RuntimeMethod* method) ;
// UnityEngine.GameObject World::placeBuilding(System.Int32,System.Int32,UnityEngine.GameObject,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* World_placeBuilding_m8694409FFA3866A65D502957536C52B7E9830098 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___2_buildingPrefab, bool ___3_force, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Input_get_mouseScrollDelta_mD112408E9182AA0F529179FF31E21D8DCD5CFA74 (const RuntimeMethod* method) ;
// System.Void World::generateMoreBackground(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_generateMoreBackground_m3D1A2A3203DFECB880EB4934579E241F8EBC30A0 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, float ___0_cameraOffset, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Input_get_mousePosition_mFF21FBD2647DAE2A23BD4C45571CA95D05A0A42C (const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mBEA93B5A0E954FEFB863DC61CB209119980EC713_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Abs_mD945EDDEA0D62D21BFDBAB7B1C0F18DFF1CEC905_inline (int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2Int::set_y(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int_set_y_mF81881204EEE272BA409728C7EBFDE3A979DDF6A_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void World::moveCursor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_moveCursor_mC133A148D958D0EC03ECE8A6BD12745873CCC3F1 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<Building>::.ctor()
inline void List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2 (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>::.ctor()
inline void Dictionary_2__ctor_mAF6F73F9A891EF55C0B1EB63C20821FDFC88CD37 (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB*, const RuntimeMethod*))Dictionary_2__ctor_mBA9343523B9FC2B73E1296469B8E4777EF816CFA_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>::.ctor()
inline void Dictionary_2__ctor_m4AABA65DE32F60D9255F00B071978999DD6FDFF6 (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*, const RuntimeMethod*))Dictionary_2__ctor_mBA9343523B9FC2B73E1296469B8E4777EF816CFA_gshared)(__this, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494 (float ___0_minInclusive, float ___1_maxInclusive, const RuntimeMethod* method) ;
// System.Void World::buildBackground()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_buildBackground_m9572A9FC3188862D4B440E02BA3F338A472B3B04 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) ;
// System.Void UIManager::updateResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_updateResources_m1EBF6B83B71F6700912C6146A2E9828CC918D496 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_mB237DEA6244132670D38990BAB77D813FBB028D2 (int32_t ___0_key, const RuntimeMethod* method) ;
// System.Void UIManager::ResetGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_ResetGame_m87E2674E6E48B1D9A5D35C38FC77DD6C6FE756EF (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<Enemy>::Remove(T)
inline bool List_1_Remove_m84DCF90974973450ABD8110FD9B80101AD2FBDF4 (List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* __this, Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* ___0_item, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B*, Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___0_item, method);
}
// System.Boolean System.Collections.Generic.List`1<Building>::Remove(T)
inline bool List_1_Remove_mE25E8E89562CD7120066447B2DC1F0D8DCBD26E5 (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* __this, Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ___0_item, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*, Building_t950D5394E080624D7E96B158EF852EA16ADB3650*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___0_item, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>::Remove(TKey)
inline bool Dictionary_2_Remove_mB433D0E9F8D7BD254D407A727943F2E4AF06903A (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, const RuntimeMethod*))Dictionary_2_Remove_mDA0670941EFD4847344D5656EF43A63D3E52F766_gshared)(__this, ___0_key, method);
}
// System.Boolean System.Collections.Generic.List`1<Building>::Contains(T)
inline bool List_1_Contains_m2AE956CB99C7BB4884AEC1401F26720C40069AFE (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* __this, Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ___0_item, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*, Building_t950D5394E080624D7E96B158EF852EA16ADB3650*, const RuntimeMethod*))List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared)(__this, ___0_item, method);
}
// System.Void System.Collections.Generic.List`1<Building>::Add(T)
inline void List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_inline (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* __this, Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*, Building_t950D5394E080624D7E96B158EF852EA16ADB3650*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___0_item, method);
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Building>::GetEnumerator()
inline Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC (*) (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Building>::Dispose()
inline void Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C (Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<Building>::get_Current()
inline Building_t950D5394E080624D7E96B158EF852EA16ADB3650* Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_inline (Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC* __this, const RuntimeMethod* method)
{
	return ((  Building_t950D5394E080624D7E96B158EF852EA16ADB3650* (*) (Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Building>::MoveNext()
inline bool Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3 (Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<Building>::get_Count()
inline int32_t List_1_get_Count_m8B9D30B8AD9D2A8297CBE27DBB8144B406427EC5_inline (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Void UIManager::gameOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_gameOver_m3B1C99259E72CD43C2586E28A93B855C39657E70 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline (const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<UnityEngine.GameObject>(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___0_original, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_position, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___2_rotation, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___3_parent, const RuntimeMethod* method)
{
	return ((  GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m5F38AE6B74636F569647D545E365C5579E5F59CE_gshared)(___0_original, ___1_position, ___2_rotation, ___3_parent, method);
}
// T UnityEngine.GameObject::GetComponent<Enemy>()
inline Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* GameObject_GetComponent_TisEnemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB_m88CA17308ECE2A8FD72E8EABE0DA90718A0FFA2F (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Enemy>::Add(T)
inline void List_1_Add_m5212A0E073717BF79B93BBA707AB20B862DEC2C0_inline (List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* __this, Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B*, Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___0_item, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m5F87930F9B0828E5652E2D9D01ED907C01122C86_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) ;
// System.Boolean Building::canPlace(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Building_canPlace_m7EE85613F2F93E783FB18D6E537350C16158C9CF (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_x, int32_t ___1_y, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___2_bgMap, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___3_oreMap, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___4_bldMap, const RuntimeMethod* method) ;
// UnityEngine.GameObject World::placeTile(System.Int32,System.Int32,UnityEngine.GameObject,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* World_placeTile_m52587F1851930889B959CBB6053600F82237A511 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___2_buildingPrefab, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___3_parent, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m18C98B2C5838EFF440390391FFCEB7AE47933032 (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, Building_t950D5394E080624D7E96B158EF852EA16ADB3650*, const RuntimeMethod*))Dictionary_2_set_Item_mD6FA021D0D92DC50FF21204FC428639799DCF3A6_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Void World::resPerSUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_resPerSUpdate_mA57CE2A19904E03F3464CE96E6F429791806D772 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Building::getResourceGeneration(System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* Building_getResourceGeneration_m0FF5325BAB3A3792F7B2D5F4FFDF33F19C818EA7 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___0_bgMap, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___1_oreMap, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___2_bldMap, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_mAEDD6BBEE1B37BC5E1D803803352FBE4CF4D3D7E (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* __this, String_t* ___0_key, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m1087B74B4FF5004CBB6CC864FF1C87B6DB138505_gshared)(__this, ___0_key, method);
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Transform_GetChild_mE686DF0C7AAC1F7AEF356967B1C04D8B8E240EAF (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_PerlinNoise_mAB0E53C29FE95469CF303364910AD0D8662A9A6A (float ___0_x, float ___1_y, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, const RuntimeMethod* method) ;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546 (SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) ;
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void World::placeBackgroundTile(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_placeBackgroundTile_m60AD5694F95AB3385104B0A362D170FA08110CAE (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<Resource>()
inline Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_mCC2518456C91884B2443C580C017215822081CC7 (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_key, Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C*, const RuntimeMethod*))Dictionary_2_set_Item_mD6FA021D0D92DC50FF21204FC428639799DCF3A6_gshared)(__this, ___0_key, ___1_value, method);
}
// T UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m6763D9767F033357F88B6637F048F4ACA4123B68 (int32_t ___0_minInclusive, int32_t ___1_maxExclusive, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Angle_mD94AAEA690169FE5882D60F8489C8BF63300C221_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_from, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_to, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Sign(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Sign_m42EE1F0BC041AF14F89DED7F762BE996E2C50D8A_inline (float ___0_f, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Internal_FromEulerRad_m66D4475341F53949471E6870FB5C5E4A5E9BA93E (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_euler, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::SmoothDamp(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2&,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_SmoothDamp_m6294700C7D9CDEACDB21858E25AC703B92CED4CC (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_current, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_target, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___2_currentVelocity, float ___3_smoothTime, float ___4_maxSpeed, float ___5_deltaTime, const RuntimeMethod* method) ;
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Quaternion_Angle_mAADDBB3C30736B4C7B75CF3A241C1CF5E0386C26_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_a, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_b, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Min_m747CA71A9483CDB394B13BD0AD048EE17E48FFE4_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_SlerpUnclamped_mAE7F4DF2F239831CCAA1DFB52F313E5AED52D32D (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_a, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_b, float ___2_t, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m6F2E069A50E787D131261E5CB25FC9E03F95B5E1_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Dot_mC1E68FDB4FB462A279A303C043B8FD0AC11C8458_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline (float ___0_value, float ___1_min, float ___2_max, const RuntimeMethod* method) ;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Quaternion_Dot_mF9D3BE33940A47979DADA7E81650AEB356D5D12B_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_a, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_b, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Quaternion::IsEqualUsingDot(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Quaternion_IsEqualUsingDot_m9C672201C918C2D1E739F559DBE4406F95997CBD_inline (float ___0_dot, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Building::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_Start_m0AF45A4F189A22A3E65906C321511C3B21970FDC (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral478FDC724A47DA9391A41A50CD1FC48B472B6D32);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hp = maxHP;
		int32_t L_0 = __this->___maxHP_7;
		__this->___hp_8 = L_0;
		// if (bldName != "root") {
		String_t* L_1 = __this->___bldName_5;
		bool L_2;
		L_2 = String_op_Inequality_m8C940F3CFC42866709D7CA931B3D77B4BE94BCB6(L_1, _stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C, NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		// InvokeRepeating("think", 0.25f, 0.25f);
		MonoBehaviour_InvokeRepeating_mF208501E0E4918F9168BBBA5FC50D8F80D01514D(__this, _stringLiteral478FDC724A47DA9391A41A50CD1FC48B472B6D32, (0.25f), (0.25f), NULL);
	}

IL_0033:
	{
		// }
		return;
	}
}
// System.Void Building::think()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_think_m4D58534AA5E59080C57A4BB1880FA76058F514F2 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5E66CFA2AD0CCFEA2AEC0EB972244661E4D07044);
		s_Il2CppMethodInitialized = true;
	}
	{
		// switch(bldName) {
		String_t* L_0 = __this->___bldName_5;
		bool L_1;
		L_1 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_0, _stringLiteral5E66CFA2AD0CCFEA2AEC0EB972244661E4D07044, NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		// findClosestEnemy();
		Building_findClosestEnemy_m78B19B59C3D6D34967083BD8002C0E7491A55929(__this, NULL);
		// if (closestEnemy == null) {
		Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_2 = __this->___closestEnemy_18;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return;
		return;
	}

IL_0027:
	{
		// closestEnemy.takeDamage(damage);
		Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_4 = __this->___closestEnemy_18;
		int32_t L_5 = __this->___damage_13;
		Enemy_takeDamage_m252CF45B4ADCFE8158C3CF5C8127C8A5C06B7A6F(L_4, L_5, NULL);
	}

IL_0038:
	{
		// }
		return;
	}
}
// System.Void Building::findClosestEnemy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_findClosestEnemy_m78B19B59C3D6D34967083BD8002C0E7491A55929 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m9B53C3D04EDC545CD4D52D8F65FA2321BD32FB45_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF19A0C6189F7B91B8CCD0DDC457D66F2A161BA18_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m1AF923779EA798B6D54040FA67531B1F5F1EC323_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m0A8258727C1434280EB076545170B0CC4F0904C9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* V_2 = NULL;
	float V_3 = 0.0f;
	{
		// float minDist = Mathf.Infinity;
		V_0 = (std::numeric_limits<float>::infinity());
		// foreach (var e in world.enemyList) {
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_0 = __this->___world_4;
		List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* L_1 = L_0->___enemyList_32;
		Enumerator_tCE299E8B29D68978C6B43A2689675634A0604AF8 L_2;
		L_2 = List_1_GetEnumerator_m0A8258727C1434280EB076545170B0CC4F0904C9(L_1, List_1_GetEnumerator_m0A8258727C1434280EB076545170B0CC4F0904C9_RuntimeMethod_var);
		V_1 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_005f:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m9B53C3D04EDC545CD4D52D8F65FA2321BD32FB45((&V_1), Enumerator_Dispose_m9B53C3D04EDC545CD4D52D8F65FA2321BD32FB45_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0054_1;
			}

IL_0019_1:
			{
				// foreach (var e in world.enemyList) {
				Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_3;
				L_3 = Enumerator_get_Current_m1AF923779EA798B6D54040FA67531B1F5F1EC323_inline((&V_1), Enumerator_get_Current_m1AF923779EA798B6D54040FA67531B1F5F1EC323_RuntimeMethod_var);
				V_2 = L_3;
				// float dist = Vector2.Distance(transform.position, e.transform.position);
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
				L_4 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
				L_5 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_4, NULL);
				Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
				L_6 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_5, NULL);
				Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_7 = V_2;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8;
				L_8 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_7, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
				L_9 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_8, NULL);
				Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10;
				L_10 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_9, NULL);
				float L_11;
				L_11 = Vector2_Distance_mBACBB1609E1894D68F882D86A93519E311810C89_inline(L_6, L_10, NULL);
				V_3 = L_11;
				// if (dist < minDist) {
				float L_12 = V_3;
				float L_13 = V_0;
				if ((!(((float)L_12) < ((float)L_13))))
				{
					goto IL_0054_1;
				}
			}
			{
				// minDist = dist;
				float L_14 = V_3;
				V_0 = L_14;
				// closestEnemy = e;
				Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_15 = V_2;
				__this->___closestEnemy_18 = L_15;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___closestEnemy_18), (void*)L_15);
			}

IL_0054_1:
			{
				// foreach (var e in world.enemyList) {
				bool L_16;
				L_16 = Enumerator_MoveNext_mF19A0C6189F7B91B8CCD0DDC457D66F2A161BA18((&V_1), Enumerator_MoveNext_mF19A0C6189F7B91B8CCD0DDC457D66F2A161BA18_RuntimeMethod_var);
				if (L_16)
				{
					goto IL_0019_1;
				}
			}
			{
				goto IL_006d;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_006d:
	{
		// if (minDist >= maxRange) {
		float L_17 = V_0;
		float L_18 = __this->___maxRange_19;
		if ((!(((float)L_17) >= ((float)L_18))))
		{
			goto IL_007d;
		}
	}
	{
		// closestEnemy = null;
		__this->___closestEnemy_18 = (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___closestEnemy_18), (void*)(Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB*)NULL);
	}

IL_007d:
	{
		// }
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Building::getResourceGeneration(System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* Building_getResourceGeneration_m0FF5325BAB3A3792F7B2D5F4FFDF33F19C818EA7 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___0_bgMap, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___1_oreMap, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___2_bldMap, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA3C3860EDE2CDD08BBD68C389377BC89D029D968_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0975C4D64F1CC305CF23880AA82B42D9ED95A9F5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral42068B0B535BEE0AD0CBD0E4D92D9B191EDBB05B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBF6181E7BB6256EBF41D34F7C1FA1BA4F3C52B0B);
		s_Il2CppMethodInitialized = true;
	}
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_0;
	memset((&V_0), 0, sizeof(V_0));
	Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* V_1 = NULL;
	String_t* V_2 = NULL;
	Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_5;
	memset((&V_5), 0, sizeof(V_5));
	Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* V_6 = NULL;
	{
		// var position = new Vector2Int(x, y);
		int32_t L_0 = __this->___x_14;
		int32_t L_1 = __this->___y_15;
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&V_0), L_0, L_1, NULL);
		// var resources = new Dictionary<string, int>();
		Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_2 = (Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588*)il2cpp_codegen_object_new(Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mA3C3860EDE2CDD08BBD68C389377BC89D029D968(L_2, Dictionary_2__ctor_mA3C3860EDE2CDD08BBD68C389377BC89D029D968_RuntimeMethod_var);
		V_1 = L_2;
		// switch(bldName) {
		String_t* L_3 = __this->___bldName_5;
		V_2 = L_3;
		String_t* L_4 = V_2;
		bool L_5;
		L_5 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_4, _stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C, NULL);
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_6 = V_2;
		bool L_7;
		L_7 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_6, _stringLiteralBF6181E7BB6256EBF41D34F7C1FA1BA4F3C52B0B, NULL);
		if (L_7)
		{
			goto IL_0067;
		}
	}
	{
		goto IL_00f2;
	}

IL_003f:
	{
		// if (!oreMap.ContainsKey(position)) {
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_8 = ___1_oreMap;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_9 = V_0;
		bool L_10;
		L_10 = Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE(L_8, L_9, Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE_RuntimeMethod_var);
		if (L_10)
		{
			goto IL_00f2;
		}
	}
	{
		// resources["water"] = bgMap[position].stored;
		Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_11 = V_1;
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_12 = ___0_bgMap;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_13 = V_0;
		Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_14;
		L_14 = Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB(L_12, L_13, Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB_RuntimeMethod_var);
		int32_t L_15 = L_14->___stored_5;
		Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2(L_11, _stringLiteral42068B0B535BEE0AD0CBD0E4D92D9B191EDBB05B, L_15, Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2_RuntimeMethod_var);
		// } break;
		goto IL_00f2;
	}

IL_0067:
	{
		// resources["n"] = 0;
		Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_16 = V_1;
		Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2(L_16, _stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6, 0, Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2_RuntimeMethod_var);
		// foreach (var shapeCoords in bldShape) {
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_17 = __this->___bldShape_6;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_18;
		L_18 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_17, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_3 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00e4:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_3), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00d9_1;
			}

IL_0081_1:
			{
				// foreach (var shapeCoords in bldShape) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_19;
				L_19 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_3), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_4 = L_19;
				// var coords = position + shapeCoords;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_20 = V_0;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_21 = V_4;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_22;
				L_22 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_20, L_21, NULL);
				V_5 = L_22;
				// if (oreMap.ContainsKey(coords)) {
				Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_23 = ___1_oreMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_24 = V_5;
				bool L_25;
				L_25 = Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE(L_23, L_24, Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE_RuntimeMethod_var);
				if (!L_25)
				{
					goto IL_00d9_1;
				}
			}
			{
				// if (oreMap[coords].resName == "nitrogen") {
				Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_26 = ___1_oreMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_27 = V_5;
				Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_28;
				L_28 = Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB(L_26, L_27, Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB_RuntimeMethod_var);
				String_t* L_29 = L_28->___resName_4;
				bool L_30;
				L_30 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_29, _stringLiteral0975C4D64F1CC305CF23880AA82B42D9ED95A9F5, NULL);
				if (!L_30)
				{
					goto IL_00d9_1;
				}
			}
			{
				// resources["n"] += extractionSpeed;
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_31 = V_1;
				V_6 = L_31;
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_32 = V_6;
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_33 = V_6;
				int32_t L_34;
				L_34 = Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592(L_33, _stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6, Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592_RuntimeMethod_var);
				int32_t L_35 = __this->___extractionSpeed_20;
				Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2(L_32, _stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6, ((int32_t)il2cpp_codegen_add(L_34, L_35)), Dictionary_2_set_Item_m038480C0EC13713DBD89A53BE69FF0359501B4C2_RuntimeMethod_var);
			}

IL_00d9_1:
			{
				// foreach (var shapeCoords in bldShape) {
				bool L_36;
				L_36 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_3), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_36)
				{
					goto IL_0081_1;
				}
			}
			{
				goto IL_00f2;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00f2:
	{
		// return resources;
		Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_37 = V_1;
		return L_37;
	}
}
// System.Void Building::takeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_takeDamage_m6ECB33F272DFAFEACC917CB29402FD33D8888016 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_damage, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hp -= damage;
		int32_t L_0 = __this->___hp_8;
		int32_t L_1 = ___0_damage;
		__this->___hp_8 = ((int32_t)il2cpp_codegen_subtract(L_0, L_1));
		// print(hp);
		int32_t L_2 = __this->___hp_8;
		int32_t L_3 = L_2;
		RuntimeObject* L_4 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_3);
		MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(L_4, NULL);
		// if (hp <= 0) {
		int32_t L_5 = __this->___hp_8;
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		// hp = 0;
		__this->___hp_8 = 0;
		// destroy();
		Building_destroy_m1BDC7A8FBECE0B0B13C491541AE704D9AF47EF2A(__this, NULL);
	}

IL_0034:
	{
		// }
		return;
	}
}
// System.Void Building::destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building_destroy_m1BDC7A8FBECE0B0B13C491541AE704D9AF47EF2A (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2DB3A856213894658C6BE0C83169D791A76B711A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!isBeingDestroyed) {
		bool L_0 = __this->___isBeingDestroyed_16;
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		// isBeingDestroyed = true;
		__this->___isBeingDestroyed_16 = (bool)1;
		// print("building destroyed!");
		MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(_stringLiteral2DB3A856213894658C6BE0C83169D791A76B711A, NULL);
		// world.buildingDestroyed(this);
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_1 = __this->___world_4;
		World_buildingDestroyed_mF07CB8463394DD626C024B5B66C935FD901BA8B1(L_1, __this, NULL);
		// Destroy(this.gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(L_2, NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Boolean Building::isConnectedToRoot(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Building_isConnectedToRoot_m2531738249902042141A601B72611A949EE3B403 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_x, int32_t ___1_y, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___2_bldMap, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* V_0 = NULL;
	bool V_1 = false;
	Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		// var checkCoords = new List<Vector2Int>();
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_0 = (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D*)il2cpp_codegen_object_new(List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D_il2cpp_TypeInfo_var);
		List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF(L_0, List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF_RuntimeMethod_var);
		V_0 = L_0;
		// foreach (var coord in bldShape) {
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_1 = __this->___bldShape_6;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_2;
		L_2 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_1, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_2 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0081:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_2), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0076_1;
			}

IL_0014_1:
			{
				// foreach (var coord in bldShape) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_3;
				L_3 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_2), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_3 = L_3;
				// int ox = x + coord.x;
				int32_t L_4 = ___0_x;
				int32_t L_5;
				L_5 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline((&V_3), NULL);
				V_4 = ((int32_t)il2cpp_codegen_add(L_4, L_5));
				// int oy = y + coord.y;
				int32_t L_6 = ___1_y;
				int32_t L_7;
				L_7 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline((&V_3), NULL);
				V_5 = ((int32_t)il2cpp_codegen_add(L_6, L_7));
				// checkCoords.Add(new Vector2Int(ox - 1, oy));
				List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_8 = V_0;
				int32_t L_9 = V_4;
				int32_t L_10 = V_5;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_11;
				memset((&L_11), 0, sizeof(L_11));
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_11), ((int32_t)il2cpp_codegen_subtract(L_9, 1)), L_10, /*hidden argument*/NULL);
				List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline(L_8, L_11, List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
				// checkCoords.Add(new Vector2Int(ox + 1, oy));
				List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_12 = V_0;
				int32_t L_13 = V_4;
				int32_t L_14 = V_5;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_15;
				memset((&L_15), 0, sizeof(L_15));
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_15), ((int32_t)il2cpp_codegen_add(L_13, 1)), L_14, /*hidden argument*/NULL);
				List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline(L_12, L_15, List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
				// checkCoords.Add(new Vector2Int(ox, oy - 1));
				List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_16 = V_0;
				int32_t L_17 = V_4;
				int32_t L_18 = V_5;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_19;
				memset((&L_19), 0, sizeof(L_19));
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_19), L_17, ((int32_t)il2cpp_codegen_subtract(L_18, 1)), /*hidden argument*/NULL);
				List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline(L_16, L_19, List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
				// checkCoords.Add(new Vector2Int(ox, oy + 1));
				List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_20 = V_0;
				int32_t L_21 = V_4;
				int32_t L_22 = V_5;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_23;
				memset((&L_23), 0, sizeof(L_23));
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_23), L_21, ((int32_t)il2cpp_codegen_add(L_22, 1)), /*hidden argument*/NULL);
				List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline(L_20, L_23, List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
			}

IL_0076_1:
			{
				// foreach (var coord in bldShape) {
				bool L_24;
				L_24 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_2), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_24)
				{
					goto IL_0014_1;
				}
			}
			{
				goto IL_008f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_008f:
	{
		// var hasAdjacentRoot = false;
		V_1 = (bool)0;
		// foreach (var coord in checkCoords) {
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_25 = V_0;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_26;
		L_26 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_25, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_2 = L_26;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00d3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_2), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00c8_1;
			}

IL_009a_1:
			{
				// foreach (var coord in checkCoords) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_27;
				L_27 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_2), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_6 = L_27;
				// if (bldMap.ContainsKey(coord)) {
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_28 = ___2_bldMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_29 = V_6;
				bool L_30;
				L_30 = Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C(L_28, L_29, Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
				if (!L_30)
				{
					goto IL_00c8_1;
				}
			}
			{
				// var checkB = bldMap[coord];
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_31 = ___2_bldMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_32 = V_6;
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_33;
				L_33 = Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014(L_31, L_32, Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014_RuntimeMethod_var);
				// if (checkB.bldName == "root") {
				String_t* L_34 = L_33->___bldName_5;
				bool L_35;
				L_35 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_34, _stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C, NULL);
				if (!L_35)
				{
					goto IL_00c8_1;
				}
			}
			{
				// hasAdjacentRoot = true;
				V_1 = (bool)1;
			}

IL_00c8_1:
			{
				// foreach (var coord in checkCoords) {
				bool L_36;
				L_36 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_2), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_36)
				{
					goto IL_009a_1;
				}
			}
			{
				goto IL_00e1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00e1:
	{
		// return hasAdjacentRoot;
		bool L_37 = V_1;
		return L_37;
	}
}
// System.Boolean Building::lifeCheck(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Building_lifeCheck_mA5CC9AFDC20E50645D5DC250612391A16DAEE268 (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_x, int32_t ___1_y, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___2_bgMap, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___3_oreMap, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___4_bldMap, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0975C4D64F1CC305CF23880AA82B42D9ED95A9F5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral338B0F004B0215C76E248CD19D9093E8B98DF996);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3DFA7A0481C4EAE8561618B53082C3CB56ADBAC8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5E66CFA2AD0CCFEA2AEC0EB972244661E4D07044);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBB0AD22536D32FF7431173B3D5B6BAD1CDDD4854);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBF6181E7BB6256EBF41D34F7C1FA1BA4F3C52B0B);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_3;
	memset((&V_3), 0, sizeof(V_3));
	bool V_4 = false;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_7;
	memset((&V_7), 0, sizeof(V_7));
	{
		// var lifeCheck = false;
		V_0 = (bool)0;
		// switch (bldName) {
		String_t* L_0 = __this->___bldName_5;
		V_1 = L_0;
		String_t* L_1 = V_1;
		bool L_2;
		L_2 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, _stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C, NULL);
		if (L_2)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_3 = V_1;
		bool L_4;
		L_4 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_3, _stringLiteralBF6181E7BB6256EBF41D34F7C1FA1BA4F3C52B0B, NULL);
		if (L_4)
		{
			goto IL_00c2;
		}
	}
	{
		String_t* L_5 = V_1;
		bool L_6;
		L_6 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_5, _stringLiteral5E66CFA2AD0CCFEA2AEC0EB972244661E4D07044, NULL);
		if (L_6)
		{
			goto IL_018d;
		}
	}
	{
		goto IL_0198;
	}

IL_003b:
	{
		// var checkCoords = new List<Vector2Int>();
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_7 = (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D*)il2cpp_codegen_object_new(List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D_il2cpp_TypeInfo_var);
		List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF(L_7, List_1__ctor_mE1D9FD9DA1EF2CAC4F99EF4E013F05BB8C3507EF_RuntimeMethod_var);
		// checkCoords.Add(new Vector2Int(x - 1, y - 1));
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_8 = L_7;
		int32_t L_9 = ___0_x;
		int32_t L_10 = ___1_y;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_11), ((int32_t)il2cpp_codegen_subtract(L_9, 1)), ((int32_t)il2cpp_codegen_subtract(L_10, 1)), /*hidden argument*/NULL);
		List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline(L_8, L_11, List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
		// checkCoords.Add(new Vector2Int(x, y - 1));
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_12 = L_8;
		int32_t L_13 = ___0_x;
		int32_t L_14 = ___1_y;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_15), L_13, ((int32_t)il2cpp_codegen_subtract(L_14, 1)), /*hidden argument*/NULL);
		List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline(L_12, L_15, List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
		// checkCoords.Add(new Vector2Int(x + 1, y - 1));
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_16 = L_12;
		int32_t L_17 = ___0_x;
		int32_t L_18 = ___1_y;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_19;
		memset((&L_19), 0, sizeof(L_19));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_19), ((int32_t)il2cpp_codegen_add(L_17, 1)), ((int32_t)il2cpp_codegen_subtract(L_18, 1)), /*hidden argument*/NULL);
		List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_inline(L_16, L_19, List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_RuntimeMethod_var);
		// foreach (var coord in checkCoords) {
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_20;
		L_20 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_16, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_2 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00b4:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_2), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00a6_1;
			}

IL_0079_1:
			{
				// foreach (var coord in checkCoords) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_21;
				L_21 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_2), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_3 = L_21;
				// if (bldMap.ContainsKey(coord)) {
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_22 = ___4_bldMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_23 = V_3;
				bool L_24;
				L_24 = Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C(L_22, L_23, Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
				if (!L_24)
				{
					goto IL_00a6_1;
				}
			}
			{
				// var topB = bldMap[coord];
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_25 = ___4_bldMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_26 = V_3;
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_27;
				L_27 = Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014(L_25, L_26, Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014_RuntimeMethod_var);
				// if (topB.bldName == "root") {
				String_t* L_28 = L_27->___bldName_5;
				bool L_29;
				L_29 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_28, _stringLiteralA2E138AD319A0E08FFC4A185CE05933BF5C01D5C, NULL);
				if (!L_29)
				{
					goto IL_00a6_1;
				}
			}
			{
				// lifeCheck = true;
				V_0 = (bool)1;
			}

IL_00a6_1:
			{
				// foreach (var coord in checkCoords) {
				bool L_30;
				L_30 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_2), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_30)
				{
					goto IL_0079_1;
				}
			}
			{
				goto IL_0198;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00c2:
	{
		// var isOnOre = false;
		V_4 = (bool)0;
		// foreach (var coord in bldShape) {
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_31 = __this->___bldShape_6;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_32;
		L_32 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_31, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_2 = L_32;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_016f:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_2), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0161_1;
			}

IL_00d6_1:
			{
				// foreach (var coord in bldShape) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_33;
				L_33 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_2), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_5 = L_33;
				// var checkCoord = new Vector2Int(x, y) + coord;
				int32_t L_34 = ___0_x;
				int32_t L_35 = ___1_y;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_36;
				memset((&L_36), 0, sizeof(L_36));
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_36), L_34, L_35, /*hidden argument*/NULL);
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_37 = V_5;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_38;
				L_38 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_36, L_37, NULL);
				V_6 = L_38;
				// print("trying " + checkCoord);
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_39 = V_6;
				V_7 = L_39;
				String_t* L_40;
				L_40 = Vector2Int_ToString_m6F7E9B9B45A473FED501EB8B8B25BA1FE26DD5D4((&V_7), NULL);
				String_t* L_41;
				L_41 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(_stringLiteralBB0AD22536D32FF7431173B3D5B6BAD1CDDD4854, L_40, NULL);
				MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(L_41, NULL);
				// if (oreMap.ContainsKey(checkCoord)) {
				Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_42 = ___3_oreMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_43 = V_6;
				bool L_44;
				L_44 = Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE(L_42, L_43, Dictionary_2_ContainsKey_mEBBE373EE7F0E7E7411807812730C2DCC7AA68AE_RuntimeMethod_var);
				if (!L_44)
				{
					goto IL_0161_1;
				}
			}
			{
				// print("checking ore " + checkCoord);
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_45 = V_6;
				V_7 = L_45;
				String_t* L_46;
				L_46 = Vector2Int_ToString_m6F7E9B9B45A473FED501EB8B8B25BA1FE26DD5D4((&V_7), NULL);
				String_t* L_47;
				L_47 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(_stringLiteral3DFA7A0481C4EAE8561618B53082C3CB56ADBAC8, L_46, NULL);
				MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(L_47, NULL);
				// var check = oreMap[checkCoord];
				Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_48 = ___3_oreMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_49 = V_6;
				Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_50;
				L_50 = Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB(L_48, L_49, Dictionary_2_get_Item_mD5A758B3DEAAF563549A5D245B304B8BB01D6DFB_RuntimeMethod_var);
				// if (check.resName == "nitrogen") {
				String_t* L_51 = L_50->___resName_4;
				bool L_52;
				L_52 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_51, _stringLiteral0975C4D64F1CC305CF23880AA82B42D9ED95A9F5, NULL);
				if (!L_52)
				{
					goto IL_0161_1;
				}
			}
			{
				// print("ok");
				MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(_stringLiteral338B0F004B0215C76E248CD19D9093E8B98DF996, NULL);
				// isOnOre = true;
				V_4 = (bool)1;
			}

IL_0161_1:
			{
				// foreach (var coord in bldShape) {
				bool L_53;
				L_53 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_2), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_53)
				{
					goto IL_00d6_1;
				}
			}
			{
				goto IL_017d;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_017d:
	{
		// lifeCheck = isConnectedToRoot(x, y, bldMap) && isOnOre;
		int32_t L_54 = ___0_x;
		int32_t L_55 = ___1_y;
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_56 = ___4_bldMap;
		bool L_57;
		L_57 = Building_isConnectedToRoot_m2531738249902042141A601B72611A949EE3B403(__this, L_54, L_55, L_56, NULL);
		bool L_58 = V_4;
		V_0 = (bool)((int32_t)((int32_t)L_57&(int32_t)L_58));
		// } break;
		goto IL_0198;
	}

IL_018d:
	{
		// lifeCheck = isConnectedToRoot(x, y, bldMap);
		int32_t L_59 = ___0_x;
		int32_t L_60 = ___1_y;
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_61 = ___4_bldMap;
		bool L_62;
		L_62 = Building_isConnectedToRoot_m2531738249902042141A601B72611A949EE3B403(__this, L_59, L_60, L_61, NULL);
		V_0 = L_62;
	}

IL_0198:
	{
		// return lifeCheck;
		bool L_63 = V_0;
		return L_63;
	}
}
// System.Boolean Building::canPlace(System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Resource>,System.Collections.Generic.Dictionary`2<UnityEngine.Vector2Int,Building>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Building_canPlace_m7EE85613F2F93E783FB18D6E537350C16158C9CF (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, int32_t ___0_x, int32_t ___1_y, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___2_bgMap, Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* ___3_oreMap, Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* ___4_bldMap, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral356499BC0CEB40A5F6AB5251F362330D1FDAFB9E);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// var canPlace = false;
		V_0 = (bool)0;
		// var overlapsBld = false;
		V_1 = (bool)0;
		// foreach (var coord in bldShape) {
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_0 = __this->___bldShape_6;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_1;
		L_1 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_0, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_2 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_005c:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_2), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0051_1;
			}

IL_0012_1:
			{
				// foreach (var coord in bldShape) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_2;
				L_2 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_2), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				// var checkCoord = coord + new Vector2Int(x, y);
				int32_t L_3 = ___0_x;
				int32_t L_4 = ___1_y;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_5;
				memset((&L_5), 0, sizeof(L_5));
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_5), L_3, L_4, /*hidden argument*/NULL);
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_6;
				L_6 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_2, L_5, NULL);
				V_3 = L_6;
				// print("checking " + checkCoord);
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_7 = V_3;
				V_4 = L_7;
				String_t* L_8;
				L_8 = Vector2Int_ToString_m6F7E9B9B45A473FED501EB8B8B25BA1FE26DD5D4((&V_4), NULL);
				String_t* L_9;
				L_9 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(_stringLiteral356499BC0CEB40A5F6AB5251F362330D1FDAFB9E, L_8, NULL);
				MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(L_9, NULL);
				// if (bldMap.ContainsKey(checkCoord)) {
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_10 = ___4_bldMap;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_11 = V_3;
				bool L_12;
				L_12 = Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C(L_10, L_11, Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
				if (!L_12)
				{
					goto IL_0051_1;
				}
			}
			{
				// overlapsBld = true;
				V_1 = (bool)1;
			}

IL_0051_1:
			{
				// foreach (var coord in bldShape) {
				bool L_13;
				L_13 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_2), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_13)
				{
					goto IL_0012_1;
				}
			}
			{
				goto IL_006a;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_006a:
	{
		// print(overlapsBld);
		bool L_14 = V_1;
		bool L_15 = L_14;
		RuntimeObject* L_16 = Box(Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_il2cpp_TypeInfo_var, &L_15);
		MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(L_16, NULL);
		// if (!overlapsBld) {
		bool L_17 = V_1;
		if (L_17)
		{
			goto IL_0086;
		}
	}
	{
		// canPlace = lifeCheck(x, y, bgMap, oreMap, bldMap);
		int32_t L_18 = ___0_x;
		int32_t L_19 = ___1_y;
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_20 = ___2_bgMap;
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_21 = ___3_oreMap;
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_22 = ___4_bldMap;
		bool L_23;
		L_23 = Building_lifeCheck_mA5CC9AFDC20E50645D5DC250612391A16DAEE268(__this, L_18, L_19, L_20, L_21, L_22, NULL);
		V_0 = L_23;
	}

IL_0086:
	{
		// return canPlace;
		bool L_24 = V_0;
		return L_24;
	}
}
// System.Void Building::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Building__ctor_m1966393898763C921095D832800266FB5532893F (Building_t950D5394E080624D7E96B158EF852EA16ADB3650* __this, const RuntimeMethod* method) 
{
	{
		// public float maxRange = 5;
		__this->___maxRange_19 = (5.0f);
		// public int waterNeed = 10;
		__this->___waterNeed_21 = ((int32_t)10);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Enemy::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Start_m8BBD9A5AE10A27ABDFCD9168B93CD9C69D229034 (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral478FDC724A47DA9391A41A50CD1FC48B472B6D32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hp = maxHP;
		int32_t L_0 = __this->___maxHP_6;
		__this->___hp_7 = L_0;
		// InvokeRepeating("think", 0.25f, 0.25f);
		MonoBehaviour_InvokeRepeating_mF208501E0E4918F9168BBBA5FC50D8F80D01514D(__this, _stringLiteral478FDC724A47DA9391A41A50CD1FC48B472B6D32, (0.25f), (0.25f), NULL);
		// }
		return;
	}
}
// System.Void Enemy::findClosestBuilding()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_findClosestBuilding_m922525EB6197C6DB29F06254584A361F3FFFEB53 (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_m5A2A31F10CA4E1C998872AE5B7487A67D7F942E6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m5C98FB58633AC2356A6CF21086801ED99B68B8AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mA4B5C4A5DCF31EBE875E6B262275A1A311D7589C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mE7CABDCD96578731F6D0B96B9CBD16A2DD495AA7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ValueCollection_GetEnumerator_m591A80270DBA6B1DBF9B407FD2F75CE11E5A8282_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Building_t950D5394E080624D7E96B158EF852EA16ADB3650* V_2 = NULL;
	Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_5;
	memset((&V_5), 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		// float minDist = Mathf.Infinity;
		V_0 = (std::numeric_limits<float>::infinity());
		// foreach (var b in bldMap.Values) {
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_0 = __this->___bldMap_5;
		ValueCollection_t19149491C3F3B201F7ED0CB8490C188EDC1C24AA* L_1;
		L_1 = Dictionary_2_get_Values_m5A2A31F10CA4E1C998872AE5B7487A67D7F942E6(L_0, Dictionary_2_get_Values_m5A2A31F10CA4E1C998872AE5B7487A67D7F942E6_RuntimeMethod_var);
		Enumerator_t23D5F16F45FEDE6FE539043C21654326A14A71A8 L_2;
		L_2 = ValueCollection_GetEnumerator_m591A80270DBA6B1DBF9B407FD2F75CE11E5A8282(L_1, ValueCollection_GetEnumerator_m591A80270DBA6B1DBF9B407FD2F75CE11E5A8282_RuntimeMethod_var);
		V_1 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00b5:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m5C98FB58633AC2356A6CF21086801ED99B68B8AA((&V_1), Enumerator_Dispose_m5C98FB58633AC2356A6CF21086801ED99B68B8AA_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00a7_1;
			}

IL_001c_1:
			{
				// foreach (var b in bldMap.Values) {
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_3;
				L_3 = Enumerator_get_Current_mE7CABDCD96578731F6D0B96B9CBD16A2DD495AA7_inline((&V_1), Enumerator_get_Current_mE7CABDCD96578731F6D0B96B9CBD16A2DD495AA7_RuntimeMethod_var);
				V_2 = L_3;
				// foreach (var shapeCoord in b.bldShape) {
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_4 = V_2;
				List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_5 = L_4->___bldShape_6;
				Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_6;
				L_6 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_5, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
				V_3 = L_6;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_0099_1:
					{// begin finally (depth: 2)
						Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_3), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
						return;
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						goto IL_008e_2;
					}

IL_0032_2:
					{
						// foreach (var shapeCoord in b.bldShape) {
						Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_7;
						L_7 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_3), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
						V_4 = L_7;
						// Vector2 coords = new Vector2(b.x + shapeCoord.x, -(b.y + shapeCoord.y));
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_8 = V_2;
						int32_t L_9 = L_8->___x_14;
						int32_t L_10;
						L_10 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline((&V_4), NULL);
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_11 = V_2;
						int32_t L_12 = L_11->___y_15;
						int32_t L_13;
						L_13 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline((&V_4), NULL);
						Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_5), ((float)((int32_t)il2cpp_codegen_add(L_9, L_10))), ((float)((-((int32_t)il2cpp_codegen_add(L_12, L_13))))), NULL);
						// float dist = Vector2.Distance(transform.position, coords);
						Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_14;
						L_14 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
						Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
						L_15 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_14, NULL);
						Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16;
						L_16 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_15, NULL);
						Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_17 = V_5;
						float L_18;
						L_18 = Vector2_Distance_mBACBB1609E1894D68F882D86A93519E311810C89_inline(L_16, L_17, NULL);
						V_6 = L_18;
						// if (dist < minDist) {
						float L_19 = V_6;
						float L_20 = V_0;
						if ((!(((float)L_19) < ((float)L_20))))
						{
							goto IL_008e_2;
						}
					}
					{
						// minDist = dist;
						float L_21 = V_6;
						V_0 = L_21;
						// closestBuilding = b.gameObject;
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_22 = V_2;
						GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23;
						L_23 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_22, NULL);
						__this->___closestBuilding_12 = L_23;
						Il2CppCodeGenWriteBarrier((void**)(&__this->___closestBuilding_12), (void*)L_23);
					}

IL_008e_2:
					{
						// foreach (var shapeCoord in b.bldShape) {
						bool L_24;
						L_24 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_3), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
						if (L_24)
						{
							goto IL_0032_2;
						}
					}
					{
						goto IL_00a7_1;
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_00a7_1:
			{
				// foreach (var b in bldMap.Values) {
				bool L_25;
				L_25 = Enumerator_MoveNext_mA4B5C4A5DCF31EBE875E6B262275A1A311D7589C((&V_1), Enumerator_MoveNext_mA4B5C4A5DCF31EBE875E6B262275A1A311D7589C_RuntimeMethod_var);
				if (L_25)
				{
					goto IL_001c_1;
				}
			}
			{
				goto IL_00c3;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00c3:
	{
		// }
		return;
	}
}
// System.Void Enemy::takeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_takeDamage_m252CF45B4ADCFE8158C3CF5C8127C8A5C06B7A6F (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, int32_t ___0_damage, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral176D6BC2760E16DD7F64BE45C450982B10B1D383);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral52423545FA29055BB661EA85311534CFBFB9D8BE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hp -= damage;
		int32_t L_0 = __this->___hp_7;
		int32_t L_1 = ___0_damage;
		__this->___hp_7 = ((int32_t)il2cpp_codegen_subtract(L_0, L_1));
		// print("enemy hp: " + hp);
		int32_t* L_2 = (&__this->___hp_7);
		String_t* L_3;
		L_3 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(_stringLiteral52423545FA29055BB661EA85311534CFBFB9D8BE, L_3, NULL);
		MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(L_4, NULL);
		// if (hp <= 0) {
		int32_t L_5 = __this->___hp_7;
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		// hp = 0;
		__this->___hp_7 = 0;
		// print("enemy dead!");
		MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(_stringLiteral176D6BC2760E16DD7F64BE45C450982B10B1D383, NULL);
		// world.enemyDestroyed(this);
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_6 = __this->___world_4;
		World_enemyDestroyed_m3FB2E482EE7DF4CDC24070A2E0A4E5858EC8FB93(L_6, __this, NULL);
		// Destroy(this.gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(L_7, NULL);
	}

IL_0059:
	{
		// }
		return;
	}
}
// System.Void Enemy::think()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_think_m3921B16C0F0728F075E82FC4AAA4566647B13A42 (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Count_m692126D37315CC3D80C4DA48688E8F35AEC755B0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		// if (bldMap.Count != oldMapSize) {
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_0 = __this->___bldMap_5;
		int32_t L_1;
		L_1 = Dictionary_2_get_Count_m692126D37315CC3D80C4DA48688E8F35AEC755B0(L_0, Dictionary_2_get_Count_m692126D37315CC3D80C4DA48688E8F35AEC755B0_RuntimeMethod_var);
		int32_t L_2 = __this->___oldMapSize_11;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002a;
		}
	}
	{
		// oldMapSize = bldMap.Count;
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_3 = __this->___bldMap_5;
		int32_t L_4;
		L_4 = Dictionary_2_get_Count_m692126D37315CC3D80C4DA48688E8F35AEC755B0(L_3, Dictionary_2_get_Count_m692126D37315CC3D80C4DA48688E8F35AEC755B0_RuntimeMethod_var);
		__this->___oldMapSize_11 = L_4;
		// findClosestBuilding();
		Enemy_findClosestBuilding_m922525EB6197C6DB29F06254584A361F3FFFEB53(__this, NULL);
	}

IL_002a:
	{
		// if (closestBuilding == null) {
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___closestBuilding_12;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_5, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		// findClosestBuilding();
		Enemy_findClosestBuilding_m922525EB6197C6DB29F06254584A361F3FFFEB53(__this, NULL);
		// return;
		return;
	}

IL_003f:
	{
		// var bldPos = closestBuilding.transform.position;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___closestBuilding_12;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8;
		L_8 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_7, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_8, NULL);
		V_0 = L_9;
		// var targetCoord = new Vector2(bldPos.x, bldPos.y) + new Vector2(0.5f, -0.5f); // Center on the building;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		float L_11 = L_10.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = V_0;
		float L_13 = L_12.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_14), L_11, L_13, /*hidden argument*/NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_15), (0.5f), (-0.5f), /*hidden argument*/NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16;
		L_16 = Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline(L_14, L_15, NULL);
		V_1 = L_16;
		// Vector2 posDiff = targetCoord - new Vector2(transform.position.x, transform.position.y);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_17 = V_1;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_18;
		L_18 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19;
		L_19 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_18, NULL);
		float L_20 = L_19.___x_2;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_21;
		L_21 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22;
		L_22 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_21, NULL);
		float L_23 = L_22.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_24), L_20, L_23, /*hidden argument*/NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_25;
		L_25 = Vector2_op_Subtraction_m44475FCDAD2DA2F98D78A6625EC2DCDFE8803837_inline(L_17, L_24, NULL);
		V_2 = L_25;
		// var angle = Vector2.SignedAngle(new Vector2(1, 0), posDiff);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_26;
		memset((&L_26), 0, sizeof(L_26));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_26), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_27 = V_2;
		float L_28;
		L_28 = Vector2_SignedAngle_mAE9940DA6BC6B2182BA95C299B2EC19967B7D438_inline(L_26, L_27, NULL);
		V_3 = L_28;
		// targetRotation = Quaternion.Euler(0, 0, angle - 90);
		float L_29 = V_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_30;
		L_30 = Quaternion_Euler_m9262AB29E3E9CE94EF71051F38A28E82AEC73F90_inline((0.0f), (0.0f), ((float)il2cpp_codegen_subtract(L_29, (90.0f))), NULL);
		__this->___targetRotation_13 = L_30;
		// targetPosition = transform.position + transform.up * moveSpeed;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_31;
		L_31 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
		L_32 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_31, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_33;
		L_33 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34;
		L_34 = Transform_get_up_mE47A9D9D96422224DD0539AA5524DA5440145BB2(L_33, NULL);
		float L_35 = __this->___moveSpeed_10;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_36;
		L_36 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_34, L_35, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37;
		L_37 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_32, L_36, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_38;
		L_38 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_37, NULL);
		__this->___targetPosition_14 = L_38;
		// reachedDestination = false;
		__this->___reachedDestination_16 = (bool)0;
		// if (Vector2.Distance(transform.position, targetCoord) < 1.2f) {
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_39;
		L_39 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_40;
		L_40 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_39, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_41;
		L_41 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_40, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_42 = V_1;
		float L_43;
		L_43 = Vector2_Distance_mBACBB1609E1894D68F882D86A93519E311810C89_inline(L_41, L_42, NULL);
		if ((!(((float)L_43) < ((float)(1.20000005f)))))
		{
			goto IL_015c;
		}
	}
	{
		// reachedDestination = true;
		__this->___reachedDestination_16 = (bool)1;
		// targetPosition = transform.position;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_44;
		L_44 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_45;
		L_45 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_44, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_46;
		L_46 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_45, NULL);
		__this->___targetPosition_14 = L_46;
		// var b = closestBuilding.GetComponent<Building>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = __this->___closestBuilding_12;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_48;
		L_48 = GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C(L_47, GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var);
		// b.takeDamage(damage);
		int32_t L_49 = __this->___damage_8;
		Building_takeDamage_m6ECB33F272DFAFEACC917CB29402FD33D8888016(L_48, L_49, NULL);
	}

IL_015c:
	{
		// }
		return;
	}
}
// System.Void Enemy::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_FixedUpdate_mC96E5B789335D23FC34546AD504EEBB0F054490B (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, const RuntimeMethod* method) 
{
	{
		// if (!reachedDestination) {
		bool L_0 = __this->___reachedDestination_16;
		if (L_0)
		{
			goto IL_0071;
		}
	}
	{
		// transform.position = Vector2.SmoothDamp(transform.position, targetPosition, ref posSmoothDampState, 0, moveSpeed);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2;
		L_2 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_2, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		L_4 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_3, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = __this->___targetPosition_14;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_6 = (&__this->___posSmoothDampState_15);
		float L_7 = __this->___moveSpeed_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		L_8 = Vector2_SmoothDamp_m305987D454BC6672AC765DB56A2843D4951865AD_inline(L_4, L_5, L_6, (0.0f), L_7, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline(L_8, NULL);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_1, L_9, NULL);
		// transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_10;
		L_10 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_11;
		L_11 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_12;
		L_12 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_11, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_13 = __this->___targetRotation_13;
		float L_14 = __this->___rotationSpeed_9;
		float L_15;
		L_15 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_16;
		L_16 = Quaternion_RotateTowards_m50EF9D609C80CD423CDA856EA3481DE2004633A3_inline(L_12, L_13, ((float)il2cpp_codegen_multiply(L_14, L_15)), NULL);
		Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D(L_10, L_16, NULL);
	}

IL_0071:
	{
		// }
		return;
	}
}
// System.Void Enemy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy__ctor_mB6697627910F785A971C20C671DEFBA9D921D933 (Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* __this, const RuntimeMethod* method) 
{
	{
		// public float rotationSpeed = 0.1f;
		__this->___rotationSpeed_9 = (0.100000001f);
		// public float moveSpeed = 0.1f;
		__this->___moveSpeed_10 = (0.100000001f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Resource::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Resource__ctor_mD9FC18750A7772A0360201303D4AEDF1B4CF3F11 (Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_Start_m113F392674AB08A26877728CD36F06332E869080 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	{
		// current_res = new Vector2(Screen.width, Screen.height);
		int32_t L_0;
		L_0 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		int32_t L_1;
		L_1 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_2), ((float)L_0), ((float)L_1), /*hidden argument*/NULL);
		__this->___current_res_9 = L_2;
		// updateResolution();
		UIManager_updateResolution_m268488AC60B2A2A8282D9955A2613AB941C72839(__this, NULL);
		// cameraOriginalPos = cam.transform.position;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___cam_4;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_3, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_4, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		L_6 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_5, NULL);
		__this->___cameraOriginalPos_5 = L_6;
		// bldToPlace = rootTile;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___rootTile_15;
		__this->___bldToPlace_17 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bldToPlace_17), (void*)L_7);
		// }
		return;
	}
}
// System.Void UIManager::gameOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_gameOver_m3B1C99259E72CD43C2586E28A93B855C39657E70 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	{
		// gameOverScreen.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___gameOverScreen_18;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void UIManager::ResetGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_ResetGame_m87E2674E6E48B1D9A5D35C38FC77DD6C6FE756EF (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Scene_tA1DC762B79745EB5140F054C884855B922318356 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		Scene_tA1DC762B79745EB5140F054C884855B922318356 L_0;
		L_0 = SceneManager_GetActiveScene_m0B320EC4302F51A71495D1CCD1A0FF9C2ED1FDC8(NULL);
		V_0 = L_0;
		String_t* L_1;
		L_1 = Scene_get_name_m3C818DFA663E159274DAD823B780C7616C5E2A8C((&V_0), NULL);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(L_1, NULL);
		// }
		return;
	}
}
// System.Void UIManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_Update_m95D2E80B8F461F15C1B9BD6DB0811F5CC18571AB (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	{
		// handleCanvasResizing();
		UIManager_handleCanvasResizing_m6BCF100C3BEF15D5AAC823E069AE594F4E313CD0(__this, NULL);
		// handleMousePositioning();
		UIManager_handleMousePositioning_m2C6C95E30F8DA4E45E71451997FC85742695FEB7(__this, NULL);
		// handleMouseScrolling();
		UIManager_handleMouseScrolling_mB1B9E856F356252E3D197F35BB5784F1EB2638B2(__this, NULL);
		// }
		return;
	}
}
// System.String UIManager::compressResourceText(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, int32_t ___0_val, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral37A50091974FE11FAC57C870272F76245820AA18);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC1771FD048FA0C5283A6D1085A6C3493F05C1302);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC613D4D2FE3F5D74727D376F793286A2BCBB1391);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	String_t* G_B6_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		// if (val >= 1000000) {
		int32_t L_0 = ___0_val;
		if ((((int32_t)L_0) < ((int32_t)((int32_t)1000000))))
		{
			goto IL_0028;
		}
	}
	{
		// return ((float) val / 1000000).ToString("F1") + "m";
		int32_t L_1 = ___0_val;
		V_0 = ((float)(((float)L_1)/(1000000.0f)));
		String_t* L_2;
		L_2 = Single_ToString_m3F2C4433B6ADFA5ED8E3F14ED19CD23014E5179D((&V_0), _stringLiteralC613D4D2FE3F5D74727D376F793286A2BCBB1391, NULL);
		String_t* L_3;
		L_3 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_2, _stringLiteralC1771FD048FA0C5283A6D1085A6C3493F05C1302, NULL);
		return L_3;
	}

IL_0028:
	{
		// } else if (val >= 1000) {
		int32_t L_4 = ___0_val;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)1000))))
		{
			goto IL_0050;
		}
	}
	{
		// return ((float) val / 1000).ToString("F1") + "k";
		int32_t L_5 = ___0_val;
		V_0 = ((float)(((float)L_5)/(1000.0f)));
		String_t* L_6;
		L_6 = Single_ToString_m3F2C4433B6ADFA5ED8E3F14ED19CD23014E5179D((&V_0), _stringLiteralC613D4D2FE3F5D74727D376F793286A2BCBB1391, NULL);
		String_t* L_7;
		L_7 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_6, _stringLiteral37A50091974FE11FAC57C870272F76245820AA18, NULL);
		return L_7;
	}

IL_0050:
	{
		// return val + "";
		String_t* L_8;
		L_8 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&___0_val), NULL);
		String_t* L_9 = L_8;
		G_B5_0 = L_9;
		if (L_9)
		{
			G_B6_0 = L_9;
			goto IL_0060;
		}
	}
	{
		G_B6_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0060:
	{
		return G_B6_0;
	}
}
// System.Void UIManager::updateResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_updateResources_m1EBF6B83B71F6700912C6146A2E9828CC918D496 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5E7E6077E71E42D4DFCC7DD7E24819AAA53A939);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF2A62543E7D1614AA84BFB81CDF2C39038A8A55A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// waterText.text = string.Format("Water: {0} ({1}/s)", compressResourceText(world.water), compressResourceText(world.waterPerS));
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_0 = __this->___waterText_10;
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_1 = __this->___world_7;
		int32_t L_2 = L_1->___water_5;
		String_t* L_3;
		L_3 = UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787(__this, L_2, NULL);
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_4 = __this->___world_7;
		int32_t L_5 = L_4->___waterPerS_7;
		String_t* L_6;
		L_6 = UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787(__this, L_5, NULL);
		String_t* L_7;
		L_7 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(_stringLiteralE5E7E6077E71E42D4DFCC7DD7E24819AAA53A939, L_3, L_6, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_0, L_7);
		// nText.text = string.Format("Nitrogen: {0} ({1}/s)", compressResourceText(world.nitrogen), compressResourceText(world.nitrogenPerS));
		TMP_Text_tE8D677872D43AD4B2AAF0D6101692A17D0B251A9* L_8 = __this->___nText_11;
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_9 = __this->___world_7;
		int32_t L_10 = L_9->___nitrogen_6;
		String_t* L_11;
		L_11 = UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787(__this, L_10, NULL);
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_12 = __this->___world_7;
		int32_t L_13 = L_12->___nitrogenPerS_8;
		String_t* L_14;
		L_14 = UIManager_compressResourceText_mB25EDA8A0B200594934B56925B7EC1EE51B35787(__this, L_13, NULL);
		String_t* L_15;
		L_15 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(_stringLiteralF2A62543E7D1614AA84BFB81CDF2C39038A8A55A, L_11, L_14, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_8, L_15);
		// }
		return;
	}
}
// System.Void UIManager::gameCanvasClicked()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_gameCanvasClicked_mCF5E1E7B77DB1A447609F7CF09684A78D91AC5C3 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	{
		// world.placeBuilding(selectedTile.x, selectedTile.y, bldToPlace);
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_0 = __this->___world_7;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_1 = (&__this->___selectedTile_14);
		int32_t L_2;
		L_2 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_1, NULL);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_3 = (&__this->___selectedTile_14);
		int32_t L_4;
		L_4 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_3, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___bldToPlace_17;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = World_placeBuilding_m8694409FFA3866A65D502957536C52B7E9830098(L_0, L_2, L_4, L_5, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void UIManager::changeBldToPlace(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_changeBldToPlace_m3120C16C2DA7D62595DFD7B28CBD2AF5EC2FBF5A (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___0_bld, const RuntimeMethod* method) 
{
	{
		// bldToPlace = bld;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___0_bld;
		__this->___bldToPlace_17 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bldToPlace_17), (void*)L_0);
		// }
		return;
	}
}
// System.Void UIManager::handleMouseScrolling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_handleMouseScrolling_mB1B9E856F356252E3D197F35BB5784F1EB2638B2 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// float scroll = -Input.mouseScrollDelta.y * 1.5f;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Input_get_mouseScrollDelta_mD112408E9182AA0F529179FF31E21D8DCD5CFA74(NULL);
		float L_1 = L_0.___y_1;
		V_0 = ((float)il2cpp_codegen_multiply(((-L_1)), (1.5f)));
		// if (scroll != 0) {
		float L_2 = V_0;
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_00b0;
		}
	}
	{
		// var originalCamOffset = camOffsetY;
		float L_3 = __this->___camOffsetY_6;
		// camOffsetY += scroll;
		float L_4 = __this->___camOffsetY_6;
		float L_5 = V_0;
		__this->___camOffsetY_6 = ((float)il2cpp_codegen_add(L_4, L_5));
		// int maxCamOffset = world.maxBldY - 1;
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_6 = __this->___world_7;
		int32_t L_7 = L_6->___maxBldY_38;
		V_1 = ((int32_t)il2cpp_codegen_subtract(L_7, 1));
		// if (camOffsetY > maxCamOffset) {
		float L_8 = __this->___camOffsetY_6;
		int32_t L_9 = V_1;
		if ((!(((float)L_8) > ((float)((float)L_9)))))
		{
			goto IL_0052;
		}
	}
	{
		// camOffsetY = maxCamOffset;
		int32_t L_10 = V_1;
		__this->___camOffsetY_6 = ((float)L_10);
	}

IL_0052:
	{
		// if (camOffsetY < 0) {
		float L_11 = __this->___camOffsetY_6;
		if ((!(((float)L_11) < ((float)(0.0f)))))
		{
			goto IL_006a;
		}
	}
	{
		// camOffsetY = 0;
		__this->___camOffsetY_6 = (0.0f);
	}

IL_006a:
	{
		// cam.transform.position = cameraOriginalPos + new Vector2(0, (int) -camOffsetY);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___cam_4;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_13;
		L_13 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_12, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14 = __this->___cameraOriginalPos_5;
		float L_15 = __this->___camOffsetY_6;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_16), (0.0f), ((float)il2cpp_codegen_cast_double_to_int<int32_t>(((-L_15)))), /*hidden argument*/NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_17;
		L_17 = Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline(L_14, L_16, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		L_18 = Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline(L_17, NULL);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_13, L_18, NULL);
		// world.generateMoreBackground((int) camOffsetY);
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_19 = __this->___world_7;
		float L_20 = __this->___camOffsetY_6;
		World_generateMoreBackground_m3D1A2A3203DFECB880EB4934579E241F8EBC30A0(L_19, ((float)il2cpp_codegen_cast_double_to_int<int32_t>(L_20)), NULL);
	}

IL_00b0:
	{
		// }
		return;
	}
}
// System.Void UIManager::handleMousePositioning()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_handleMousePositioning_m2C6C95E30F8DA4E45E71451997FC85742695FEB7 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	{
		// oldMousePos = mousePos;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___mousePos_12;
		__this->___oldMousePos_13 = L_0;
		// mousePos = Input.mousePosition;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Input_get_mousePosition_mFF21FBD2647DAE2A23BD4C45571CA95D05A0A42C(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		L_2 = Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline(L_1, NULL);
		__this->___mousePos_12 = L_2;
		// if (oldMousePos != null && oldMousePos != mousePos) {
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = __this->___oldMousePos_13;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = __this->___oldMousePos_13;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = __this->___mousePos_12;
		bool L_6;
		L_6 = Vector2_op_Inequality_mBEA93B5A0E954FEFB863DC61CB209119980EC713_inline(L_4, L_5, NULL);
		if (!L_6)
		{
			goto IL_0116;
		}
	}
	{
		// mousePos.y = Screen.height - mousePos.y; // for some reason it starts from the bottom
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_7 = (&__this->___mousePos_12);
		int32_t L_8;
		L_8 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_9 = (&__this->___mousePos_12);
		float L_10 = L_9->___y_1;
		L_7->___y_1 = ((float)il2cpp_codegen_subtract(((float)L_8), L_10));
		// mousePos.x = (mousePos.x / Screen.width) * reference_res.x;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_11 = (&__this->___mousePos_12);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_12 = (&__this->___mousePos_12);
		float L_13 = L_12->___x_0;
		int32_t L_14;
		L_14 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_15 = (&__this->___reference_res_8);
		float L_16 = L_15->___x_0;
		L_11->___x_0 = ((float)il2cpp_codegen_multiply(((float)(L_13/((float)L_14))), L_16));
		// mousePos.y = (mousePos.y / Screen.height) * reference_res.y;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_17 = (&__this->___mousePos_12);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_18 = (&__this->___mousePos_12);
		float L_19 = L_18->___y_1;
		int32_t L_20;
		L_20 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_21 = (&__this->___reference_res_8);
		float L_22 = L_21->___y_1;
		L_17->___y_1 = ((float)il2cpp_codegen_multiply(((float)(L_19/((float)L_20))), L_22));
		// selectedTile = new Vector2Int((int) (mousePos.x / 32), (int) (mousePos.y / 32));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_23 = (&__this->___mousePos_12);
		float L_24 = L_23->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_25 = (&__this->___mousePos_12);
		float L_26 = L_25->___y_1;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_27;
		memset((&L_27), 0, sizeof(L_27));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_27), il2cpp_codegen_cast_double_to_int<int32_t>(((float)(L_24/(32.0f)))), il2cpp_codegen_cast_double_to_int<int32_t>(((float)(L_26/(32.0f)))), /*hidden argument*/NULL);
		__this->___selectedTile_14 = L_27;
		// selectedTile.y += Mathf.Abs((int) camOffsetY);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_28 = (&__this->___selectedTile_14);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_29 = L_28;
		int32_t L_30;
		L_30 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_29, NULL);
		float L_31 = __this->___camOffsetY_6;
		int32_t L_32;
		L_32 = Mathf_Abs_mD945EDDEA0D62D21BFDBAB7B1C0F18DFF1CEC905_inline(il2cpp_codegen_cast_double_to_int<int32_t>(L_31), NULL);
		Vector2Int_set_y_mF81881204EEE272BA409728C7EBFDE3A979DDF6A_inline(L_29, ((int32_t)il2cpp_codegen_add(L_30, L_32)), NULL);
		// world.moveCursor(selectedTile.x, selectedTile.y);
		World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* L_33 = __this->___world_7;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_34 = (&__this->___selectedTile_14);
		int32_t L_35;
		L_35 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_34, NULL);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_36 = (&__this->___selectedTile_14);
		int32_t L_37;
		L_37 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_36, NULL);
		World_moveCursor_mC133A148D958D0EC03ECE8A6BD12745873CCC3F1(L_33, L_35, L_37, NULL);
	}

IL_0116:
	{
		// }
		return;
	}
}
// System.Void UIManager::handleCanvasResizing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_handleCanvasResizing_m6BCF100C3BEF15D5AAC823E069AE594F4E313CD0 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector2 old_res = current_res;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___current_res_9;
		V_0 = L_0;
		// current_res = new Vector2(Screen.width, Screen.height);
		int32_t L_1;
		L_1 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		int32_t L_2;
		L_2 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_3), ((float)L_1), ((float)L_2), /*hidden argument*/NULL);
		__this->___current_res_9 = L_3;
		// if (old_res != null && current_res != old_res) {
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = __this->___current_res_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = V_0;
		bool L_6;
		L_6 = Vector2_op_Inequality_mBEA93B5A0E954FEFB863DC61CB209119980EC713_inline(L_4, L_5, NULL);
		if (!L_6)
		{
			goto IL_0032;
		}
	}
	{
		// updateResolution();
		UIManager_updateResolution_m268488AC60B2A2A8282D9955A2613AB941C72839(__this, NULL);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void UIManager::updateResolution()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_updateResolution_m268488AC60B2A2A8282D9955A2613AB941C72839 (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	{
		// transform.localScale = new Vector3((float) current_res.x / reference_res.x, (float) current_res.y / reference_res.y, 1);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_1 = (&__this->___current_res_9);
		float L_2 = L_1->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_3 = (&__this->___reference_res_8);
		float L_4 = L_3->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_5 = (&__this->___current_res_9);
		float L_6 = L_5->___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_7 = (&__this->___reference_res_8);
		float L_8 = L_7->___y_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)(((float)L_2)/L_4)), ((float)(((float)L_6)/L_8)), (1.0f), /*hidden argument*/NULL);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_0, L_9, NULL);
		// }
		return;
	}
}
// System.Void UIManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager__ctor_mC9DC2B8984E76F424E73C1860AD4BD3DEBF6573F (UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void World::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_Start_mFF594A74C1DE2C5CDF4C18583DCA40EC7924F24F (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m4AABA65DE32F60D9255F00B071978999DD6FDFF6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mAF6F73F9A891EF55C0B1EB63C20821FDFC88CD37_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21B94FD503EEBB29CEFABCEE47FF2025A04C747F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60162A6CFC35BDA57A4CCB3D82374ABE78EEF722);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bldList = new List<Building>();
		List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_0 = (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*)il2cpp_codegen_object_new(List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100_il2cpp_TypeInfo_var);
		List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2(L_0, List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2_RuntimeMethod_var);
		__this->___bldList_31 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bldList_31), (void*)L_0);
		// bgMap = new Dictionary<Vector2Int, Resource>();
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_1 = (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB*)il2cpp_codegen_object_new(Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mAF6F73F9A891EF55C0B1EB63C20821FDFC88CD37(L_1, Dictionary_2__ctor_mAF6F73F9A891EF55C0B1EB63C20821FDFC88CD37_RuntimeMethod_var);
		__this->___bgMap_33 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bgMap_33), (void*)L_1);
		// oreMap = new Dictionary<Vector2Int, Resource>();
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_2 = (Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB*)il2cpp_codegen_object_new(Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mAF6F73F9A891EF55C0B1EB63C20821FDFC88CD37(L_2, Dictionary_2__ctor_mAF6F73F9A891EF55C0B1EB63C20821FDFC88CD37_RuntimeMethod_var);
		__this->___oreMap_34 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___oreMap_34), (void*)L_2);
		// bldMap = new Dictionary<Vector2Int, Building>();
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_3 = (Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6*)il2cpp_codegen_object_new(Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4AABA65DE32F60D9255F00B071978999DD6FDFF6(L_3, Dictionary_2__ctor_m4AABA65DE32F60D9255F00B071978999DD6FDFF6_RuntimeMethod_var);
		__this->___bldMap_35 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bldMap_35), (void*)L_3);
		// randomRange = new Vector2(100, 100000);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_4), (100.0f), (100000.0f), /*hidden argument*/NULL);
		__this->___randomRange_16 = L_4;
		// bgPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_5 = (&__this->___randomRange_16);
		float L_6 = L_5->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_7 = (&__this->___randomRange_16);
		float L_8 = L_7->___y_1;
		float L_9;
		L_9 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_6, L_8, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_10 = (&__this->___randomRange_16);
		float L_11 = L_10->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_12 = (&__this->___randomRange_16);
		float L_13 = L_12->___y_1;
		float L_14;
		L_14 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_11, L_13, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_15), L_9, L_14, /*hidden argument*/NULL);
		__this->___bgPerlinOffset_22 = L_15;
		// oreNPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_16 = (&__this->___randomRange_16);
		float L_17 = L_16->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_18 = (&__this->___randomRange_16);
		float L_19 = L_18->___y_1;
		float L_20;
		L_20 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_17, L_19, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_21 = (&__this->___randomRange_16);
		float L_22 = L_21->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_23 = (&__this->___randomRange_16);
		float L_24 = L_23->___y_1;
		float L_25;
		L_25 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_22, L_24, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_26;
		memset((&L_26), 0, sizeof(L_26));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_26), L_20, L_25, /*hidden argument*/NULL);
		__this->___oreNPerlinOffset_23 = L_26;
		// orePPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_27 = (&__this->___randomRange_16);
		float L_28 = L_27->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_29 = (&__this->___randomRange_16);
		float L_30 = L_29->___y_1;
		float L_31;
		L_31 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_28, L_30, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_32 = (&__this->___randomRange_16);
		float L_33 = L_32->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_34 = (&__this->___randomRange_16);
		float L_35 = L_34->___y_1;
		float L_36;
		L_36 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_33, L_35, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_37;
		memset((&L_37), 0, sizeof(L_37));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_37), L_31, L_36, /*hidden argument*/NULL);
		__this->___orePPerlinOffset_24 = L_37;
		// oreKPerlinOffset = new Vector2(Random.Range(randomRange.x, randomRange.y), Random.Range(randomRange.x, randomRange.y));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_38 = (&__this->___randomRange_16);
		float L_39 = L_38->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_40 = (&__this->___randomRange_16);
		float L_41 = L_40->___y_1;
		float L_42;
		L_42 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_39, L_41, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_43 = (&__this->___randomRange_16);
		float L_44 = L_43->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_45 = (&__this->___randomRange_16);
		float L_46 = L_45->___y_1;
		float L_47;
		L_47 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494(L_44, L_46, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_48;
		memset((&L_48), 0, sizeof(L_48));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_48), L_42, L_47, /*hidden argument*/NULL);
		__this->___oreKPerlinOffset_25 = L_48;
		// oreNPerlinScale = new Vector2(0.3f, 0.5f);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_49;
		memset((&L_49), 0, sizeof(L_49));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_49), (0.300000012f), (0.5f), /*hidden argument*/NULL);
		__this->___oreNPerlinScale_18 = L_49;
		// oreNtreshold = 0.7f;
		__this->___oreNtreshold_21 = (0.699999988f);
		// buildBackground();
		World_buildBackground_m9572A9FC3188862D4B440E02BA3F338A472B3B04(__this, NULL);
		// var originalBld = placeBuilding((int) screenGridSize.x / 2, 0, originalTile, true);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_50 = (&__this->___screenGridSize_9);
		int32_t L_51;
		L_51 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_50, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_52 = __this->___originalTile_26;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_53;
		L_53 = World_placeBuilding_m8694409FFA3866A65D502957536C52B7E9830098(__this, ((int32_t)(L_51/2)), 0, L_52, (bool)1, NULL);
		// originalBld.GetComponent<Building>().dontAutodestroy = true; // make sure a failed lifecheck doesn't destroy us
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_54;
		L_54 = GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C(L_53, GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var);
		L_54->___dontAutodestroy_17 = (bool)1;
		// InvokeRepeating("resourceUpdate", 1, 1);
		MonoBehaviour_InvokeRepeating_mF208501E0E4918F9168BBBA5FC50D8F80D01514D(__this, _stringLiteral21B94FD503EEBB29CEFABCEE47FF2025A04C747F, (1.0f), (1.0f), NULL);
		// uiManager.updateResources();
		UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* L_55 = __this->___uiManager_4;
		UIManager_updateResources_m1EBF6B83B71F6700912C6146A2E9828CC918D496(L_55, NULL);
		// InvokeRepeating("handleEnemies", 1, 1);
		MonoBehaviour_InvokeRepeating_mF208501E0E4918F9168BBBA5FC50D8F80D01514D(__this, _stringLiteral60162A6CFC35BDA57A4CCB3D82374ABE78EEF722, (1.0f), (1.0f), NULL);
		// }
		return;
	}
}
// System.Void World::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_Update_m19AA7041DF46C2C714D5E3499A3AF618979CFBCC (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	{
		// if (Input.GetKeyDown(KeyCode.R)) {
		bool L_0;
		L_0 = Input_GetKeyDown_mB237DEA6244132670D38990BAB77D813FBB028D2(((int32_t)114), NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// uiManager.ResetGame();
		UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* L_1 = __this->___uiManager_4;
		UIManager_ResetGame_m87E2674E6E48B1D9A5D35C38FC77DD6C6FE756EF(L_1, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void World::enemyDestroyed(Enemy)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_enemyDestroyed_m3FB2E482EE7DF4CDC24070A2E0A4E5858EC8FB93 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* ___0_e, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m84DCF90974973450ABD8110FD9B80101AD2FBDF4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// enemyList.Remove(e);
		List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* L_0 = __this->___enemyList_32;
		Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_1 = ___0_e;
		bool L_2;
		L_2 = List_1_Remove_m84DCF90974973450ABD8110FD9B80101AD2FBDF4(L_0, L_1, List_1_Remove_m84DCF90974973450ABD8110FD9B80101AD2FBDF4_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void World::buildingDestroyed(Building)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_buildingDestroyed_mF07CB8463394DD626C024B5B66C935FD901BA8B1 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, Building_t950D5394E080624D7E96B158EF852EA16ADB3650* ___0_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Remove_mB433D0E9F8D7BD254D407A727943F2E4AF06903A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m2AE956CB99C7BB4884AEC1401F26720C40069AFE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_mE25E8E89562CD7120066447B2DC1F0D8DCBD26E5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m8B9D30B8AD9D2A8297CBE27DBB8144B406427EC5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_0;
	memset((&V_0), 0, sizeof(V_0));
	List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* V_1 = NULL;
	Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_5;
	memset((&V_5), 0, sizeof(V_5));
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_9 = NULL;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_12;
	memset((&V_12), 0, sizeof(V_12));
	Building_t950D5394E080624D7E96B158EF852EA16ADB3650* V_13 = NULL;
	Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC V_14;
	memset((&V_14), 0, sizeof(V_14));
	Building_t950D5394E080624D7E96B158EF852EA16ADB3650* V_15 = NULL;
	{
		// bldList.Remove(b);
		List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_0 = __this->___bldList_31;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_1 = ___0_b;
		bool L_2;
		L_2 = List_1_Remove_mE25E8E89562CD7120066447B2DC1F0D8DCBD26E5(L_0, L_1, List_1_Remove_mE25E8E89562CD7120066447B2DC1F0D8DCBD26E5_RuntimeMethod_var);
		// Vector2Int coords = new Vector2Int(b.x, b.y);
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_3 = ___0_b;
		int32_t L_4 = L_3->___x_14;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_5 = ___0_b;
		int32_t L_6 = L_5->___y_15;
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&V_0), L_4, L_6, NULL);
		// foreach (var coordShape in b.bldShape) {
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_7 = ___0_b;
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_8 = L_7->___bldShape_6;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_9;
		L_9 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_8, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_2 = L_9;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0054:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_2), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0049_1;
			}

IL_002e_1:
			{
				// foreach (var coordShape in b.bldShape) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_10;
				L_10 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_2), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_3 = L_10;
				// bldMap.Remove(coords + coordShape);
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_11 = __this->___bldMap_35;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_12 = V_0;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_13 = V_3;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_14;
				L_14 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_12, L_13, NULL);
				bool L_15;
				L_15 = Dictionary_2_Remove_mB433D0E9F8D7BD254D407A727943F2E4AF06903A(L_11, L_14, Dictionary_2_Remove_mB433D0E9F8D7BD254D407A727943F2E4AF06903A_RuntimeMethod_var);
			}

IL_0049_1:
			{
				// foreach (var coordShape in b.bldShape) {
				bool L_16;
				L_16 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_2), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_16)
				{
					goto IL_002e_1;
				}
			}
			{
				goto IL_0062;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0062:
	{
		// var neighbours = new List<Building>();
		List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_17 = (List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100*)il2cpp_codegen_object_new(List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100_il2cpp_TypeInfo_var);
		List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2(L_17, List_1__ctor_m58E667C28B2D4BE50420A1FFC9284269DAF486C2_RuntimeMethod_var);
		V_1 = L_17;
		// foreach (var coordShape in b.bldShape) {
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_18 = ___0_b;
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_19 = L_18->___bldShape_6;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_20;
		L_20 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_19, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_2 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_01a2:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_2), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0194_1;
			}

IL_0079_1:
			{
				// foreach (var coordShape in b.bldShape) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_21;
				L_21 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_2), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_4 = L_21;
				// var chunkCoord = coords + coordShape;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_22 = V_0;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_23 = V_4;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_24;
				L_24 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_22, L_23, NULL);
				V_5 = L_24;
				// foreach (var ox in new int[]{-1, 0, 1}) {
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_25 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)3);
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_26 = L_25;
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)(-1));
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_27 = L_26;
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (int32_t)1);
				V_6 = L_27;
				V_7 = 0;
				goto IL_0124_1;
			}

IL_00a4_1:
			{
				// foreach (var ox in new int[]{-1, 0, 1}) {
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_28 = V_6;
				int32_t L_29 = V_7;
				int32_t L_30 = L_29;
				int32_t L_31 = (L_28)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_30));
				V_8 = L_31;
				// foreach (var oy in new int[]{-1, 0, 1}) {
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_32 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)3);
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_33 = L_32;
				(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)(-1));
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_34 = L_33;
				(L_34)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (int32_t)1);
				V_9 = L_34;
				V_10 = 0;
				goto IL_0116_1;
			}

IL_00c0_1:
			{
				// foreach (var oy in new int[]{-1, 0, 1}) {
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_35 = V_9;
				int32_t L_36 = V_10;
				int32_t L_37 = L_36;
				int32_t L_38 = (L_35)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_37));
				V_11 = L_38;
				// var offset = new Vector2Int(ox, oy);
				int32_t L_39 = V_8;
				int32_t L_40 = V_11;
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&V_12), L_39, L_40, NULL);
				// if (bldMap.ContainsKey(chunkCoord + offset)) {
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_41 = __this->___bldMap_35;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_42 = V_5;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_43 = V_12;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_44;
				L_44 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_42, L_43, NULL);
				bool L_45;
				L_45 = Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C(L_41, L_44, Dictionary_2_ContainsKey_m2A95B8E2C47213354C470C7647BF938CBC73B38C_RuntimeMethod_var);
				if (!L_45)
				{
					goto IL_0110_1;
				}
			}
			{
				// var neighbour = bldMap[chunkCoord + offset];
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_46 = __this->___bldMap_35;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_47 = V_5;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_48 = V_12;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_49;
				L_49 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_47, L_48, NULL);
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_50;
				L_50 = Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014(L_46, L_49, Dictionary_2_get_Item_m44E8BDEB8750BFCB76055755F008C5998FE99014_RuntimeMethod_var);
				V_13 = L_50;
				// if (!neighbours.Contains(neighbour)) {
				List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_51 = V_1;
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_52 = V_13;
				bool L_53;
				L_53 = List_1_Contains_m2AE956CB99C7BB4884AEC1401F26720C40069AFE(L_51, L_52, List_1_Contains_m2AE956CB99C7BB4884AEC1401F26720C40069AFE_RuntimeMethod_var);
				if (L_53)
				{
					goto IL_0110_1;
				}
			}
			{
				// neighbours.Add(neighbour);
				List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_54 = V_1;
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_55 = V_13;
				List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_inline(L_54, L_55, List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_RuntimeMethod_var);
			}

IL_0110_1:
			{
				int32_t L_56 = V_10;
				V_10 = ((int32_t)il2cpp_codegen_add(L_56, 1));
			}

IL_0116_1:
			{
				// foreach (var oy in new int[]{-1, 0, 1}) {
				int32_t L_57 = V_10;
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_58 = V_9;
				if ((((int32_t)L_57) < ((int32_t)((int32_t)(((RuntimeArray*)L_58)->max_length)))))
				{
					goto IL_00c0_1;
				}
			}
			{
				int32_t L_59 = V_7;
				V_7 = ((int32_t)il2cpp_codegen_add(L_59, 1));
			}

IL_0124_1:
			{
				// foreach (var ox in new int[]{-1, 0, 1}) {
				int32_t L_60 = V_7;
				Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_61 = V_6;
				if ((((int32_t)L_60) < ((int32_t)((int32_t)(((RuntimeArray*)L_61)->max_length)))))
				{
					goto IL_00a4_1;
				}
			}
			{
				// foreach (var neighbour in neighbours) {
				List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_62 = V_1;
				Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC L_63;
				L_63 = List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C(L_62, List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C_RuntimeMethod_var);
				V_14 = L_63;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_0186_1:
					{// begin finally (depth: 2)
						Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C((&V_14), Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C_RuntimeMethod_var);
						return;
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						goto IL_017b_2;
					}

IL_0139_2:
					{
						// foreach (var neighbour in neighbours) {
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_64;
						L_64 = Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_inline((&V_14), Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_RuntimeMethod_var);
						V_15 = L_64;
						// if (!neighbour.lifeCheck(neighbour.x, neighbour.y, bgMap, oreMap, bldMap) && !neighbour.dontAutodestroy) {
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_65 = V_15;
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_66 = V_15;
						int32_t L_67 = L_66->___x_14;
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_68 = V_15;
						int32_t L_69 = L_68->___y_15;
						Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_70 = __this->___bgMap_33;
						Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_71 = __this->___oreMap_34;
						Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_72 = __this->___bldMap_35;
						bool L_73;
						L_73 = Building_lifeCheck_mA5CC9AFDC20E50645D5DC250612391A16DAEE268(L_65, L_67, L_69, L_70, L_71, L_72, NULL);
						if (L_73)
						{
							goto IL_017b_2;
						}
					}
					{
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_74 = V_15;
						bool L_75 = L_74->___dontAutodestroy_17;
						if (L_75)
						{
							goto IL_017b_2;
						}
					}
					{
						// neighbour.destroy();
						Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_76 = V_15;
						Building_destroy_m1BDC7A8FBECE0B0B13C491541AE704D9AF47EF2A(L_76, NULL);
					}

IL_017b_2:
					{
						// foreach (var neighbour in neighbours) {
						bool L_77;
						L_77 = Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3((&V_14), Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3_RuntimeMethod_var);
						if (L_77)
						{
							goto IL_0139_2;
						}
					}
					{
						goto IL_0194_1;
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_0194_1:
			{
				// foreach (var coordShape in b.bldShape) {
				bool L_78;
				L_78 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_2), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_78)
				{
					goto IL_0079_1;
				}
			}
			{
				goto IL_01b0;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_01b0:
	{
		// if (bldList.Count == 0) {
		List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_79 = __this->___bldList_31;
		int32_t L_80;
		L_80 = List_1_get_Count_m8B9D30B8AD9D2A8297CBE27DBB8144B406427EC5_inline(L_79, List_1_get_Count_m8B9D30B8AD9D2A8297CBE27DBB8144B406427EC5_RuntimeMethod_var);
		if (L_80)
		{
			goto IL_01c8;
		}
	}
	{
		// uiManager.gameOver();
		UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* L_81 = __this->___uiManager_4;
		UIManager_gameOver_m3B1C99259E72CD43C2586E28A93B855C39657E70(L_81, NULL);
	}

IL_01c8:
	{
		// }
		return;
	}
}
// System.Void World::handleEnemies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_handleEnemies_mB61C1F9EC96BDE45520EE977D449BDED0C63C5FB (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisEnemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB_m88CA17308ECE2A8FD72E8EABE0DA90718A0FFA2F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m5212A0E073717BF79B93BBA707AB20B862DEC2C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* V_1 = NULL;
	{
		// var roll = Random.Range(0.0f, 1.0f);
		float L_0;
		L_0 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494((0.0f), (1.0f), NULL);
		// if (roll <= enemySpawnChance) {
		float L_1 = __this->___enemySpawnChance_39;
		if ((!(((float)L_0) <= ((float)L_1))))
		{
			goto IL_0081;
		}
	}
	{
		// var coords = new Vector2(Random.Range(1.0f, screenGridSize.x - 1), - (maxBldY + 5));
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_2 = (&__this->___screenGridSize_9);
		int32_t L_3;
		L_3 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_2, NULL);
		float L_4;
		L_4 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494((1.0f), ((float)((int32_t)il2cpp_codegen_subtract(L_3, 1))), NULL);
		int32_t L_5 = __this->___maxBldY_38;
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_0), L_4, ((float)((-((int32_t)il2cpp_codegen_add(L_5, 5))))), NULL);
		// GameObject obj = Instantiate(enemyBasic, coords, Quaternion.identity, enemiesParent);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___enemyBasic_40;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline(L_7, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_9;
		L_9 = Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline(NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_10 = __this->___enemiesParent_14;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11;
		L_11 = Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181(L_6, L_8, L_9, L_10, Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181_RuntimeMethod_var);
		// var e = obj.GetComponent<Enemy>();
		Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_12;
		L_12 = GameObject_GetComponent_TisEnemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB_m88CA17308ECE2A8FD72E8EABE0DA90718A0FFA2F(L_11, GameObject_GetComponent_TisEnemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB_m88CA17308ECE2A8FD72E8EABE0DA90718A0FFA2F_RuntimeMethod_var);
		V_1 = L_12;
		// e.bldMap = bldMap;
		Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_13 = V_1;
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_14 = __this->___bldMap_35;
		L_13->___bldMap_5 = L_14;
		Il2CppCodeGenWriteBarrier((void**)(&L_13->___bldMap_5), (void*)L_14);
		// e.world = this;
		Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_15 = V_1;
		L_15->___world_4 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_15->___world_4), (void*)__this);
		// enemyList.Add(e);
		List_1_tAF2833BCF7AB13A184BFFC2F17FA72478CA1686B* L_16 = __this->___enemyList_32;
		Enemy_t10DB314C96B1CE78B8D967CD3B39F05126409BBB* L_17 = V_1;
		List_1_Add_m5212A0E073717BF79B93BBA707AB20B862DEC2C0_inline(L_16, L_17, List_1_Add_m5212A0E073717BF79B93BBA707AB20B862DEC2C0_RuntimeMethod_var);
	}

IL_0081:
	{
		// }
		return;
	}
}
// UnityEngine.GameObject World::placeTile(System.Int32,System.Int32,UnityEngine.GameObject,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* World_placeTile_m52587F1851930889B959CBB6053600F82237A511 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___2_buildingPrefab, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___3_parent, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// y = -y;
		int32_t L_0 = ___1_y;
		___1_y = ((-L_0));
		// return Instantiate(buildingPrefab, new Vector3(x, y), Quaternion.identity, parent);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = ___2_buildingPrefab;
		int32_t L_2 = ___0_x;
		int32_t L_3 = ___1_y;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m5F87930F9B0828E5652E2D9D01ED907C01122C86_inline((&L_4), ((float)L_2), ((float)L_3), /*hidden argument*/NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline(NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_6 = ___3_parent;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181(L_1, L_4, L_5, L_6, Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mD136E37F696C00A3A1D4F65724ACAE903E385181_RuntimeMethod_var);
		return L_7;
	}
}
// UnityEngine.GameObject World::placeBuilding(System.Int32,System.Int32,UnityEngine.GameObject,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* World_placeBuilding_m8694409FFA3866A65D502957536C52B7E9830098 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___2_buildingPrefab, bool ___3_force, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m18C98B2C5838EFF440390391FFCEB7AE47933032_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral66AB9EE3DACF3D9819CA98FB792066831612BC34);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBDD9E455CEE99D40D2BEDCEB2AE73A3D2753B9CD);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_0 = NULL;
	Building_t950D5394E080624D7E96B158EF852EA16ADB3650* V_1 = NULL;
	bool V_2 = false;
	Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// GameObject obj = null;
		V_0 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)NULL;
		// var b = buildingPrefab.GetComponent<Building>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___2_buildingPrefab;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_1;
		L_1 = GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C(L_0, GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var);
		V_1 = L_1;
		// bool canPlace = b.canPlace(x, y, bgMap, oreMap, bldMap);
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_2 = V_1;
		int32_t L_3 = ___0_x;
		int32_t L_4 = ___1_y;
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_5 = __this->___bgMap_33;
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_6 = __this->___oreMap_34;
		Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_7 = __this->___bldMap_35;
		bool L_8;
		L_8 = Building_canPlace_m7EE85613F2F93E783FB18D6E537350C16158C9CF(L_2, L_3, L_4, L_5, L_6, L_7, NULL);
		V_2 = L_8;
		// if (force || canPlace) {
		bool L_9 = ___3_force;
		bool L_10 = V_2;
		if (!((int32_t)((int32_t)L_9|(int32_t)L_10)))
		{
			goto IL_0125;
		}
	}
	{
		// if (force || (water >= b.waterCost && nitrogen >= b.nCost)) {
		bool L_11 = ___3_force;
		if (L_11)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_12 = __this->___water_5;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_13 = V_1;
		int32_t L_14 = L_13->___waterCost_9;
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0119;
		}
	}
	{
		int32_t L_15 = __this->___nitrogen_6;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_16 = V_1;
		int32_t L_17 = L_16->___nCost_10;
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0119;
		}
	}

IL_0053:
	{
		// if (!force) {
		bool L_18 = ___3_force;
		if (L_18)
		{
			goto IL_007d;
		}
	}
	{
		// water -= b.waterCost;
		int32_t L_19 = __this->___water_5;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_20 = V_1;
		int32_t L_21 = L_20->___waterCost_9;
		__this->___water_5 = ((int32_t)il2cpp_codegen_subtract(L_19, L_21));
		// nitrogen -= b.nCost;
		int32_t L_22 = __this->___nitrogen_6;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_23 = V_1;
		int32_t L_24 = L_23->___nCost_10;
		__this->___nitrogen_6 = ((int32_t)il2cpp_codegen_subtract(L_22, L_24));
	}

IL_007d:
	{
		// obj = placeTile(x, y, buildingPrefab, buildingsParent);
		int32_t L_25 = ___0_x;
		int32_t L_26 = ___1_y;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27 = ___2_buildingPrefab;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_28 = __this->___buildingsParent_13;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29;
		L_29 = World_placeTile_m52587F1851930889B959CBB6053600F82237A511(__this, L_25, L_26, L_27, L_28, NULL);
		V_0 = L_29;
		// b = obj.GetComponent<Building>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30 = V_0;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_31;
		L_31 = GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C(L_30, GameObject_GetComponent_TisBuilding_t950D5394E080624D7E96B158EF852EA16ADB3650_m5BD940A7BA4550D0BFABEEE95DE8BF555726484C_RuntimeMethod_var);
		V_1 = L_31;
		// b.x = x;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_32 = V_1;
		int32_t L_33 = ___0_x;
		L_32->___x_14 = L_33;
		// b.y = y;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_34 = V_1;
		int32_t L_35 = ___1_y;
		L_34->___y_15 = L_35;
		// b.world = this;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_36 = V_1;
		L_36->___world_4 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_36->___world_4), (void*)__this);
		// maxBldY = Mathf.Max(maxBldY, y);
		int32_t L_37 = __this->___maxBldY_38;
		int32_t L_38 = ___1_y;
		int32_t L_39;
		L_39 = Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline(L_37, L_38, NULL);
		__this->___maxBldY_38 = L_39;
		// bldList.Add(b);
		List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_40 = __this->___bldList_31;
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_41 = V_1;
		List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_inline(L_40, L_41, List_1_Add_mB532C7E9C5CC5ECAD9FAFC9B772BAE62494CC485_RuntimeMethod_var);
		// foreach (var coord in b.bldShape) {
		Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_42 = V_1;
		List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* L_43 = L_42->___bldShape_6;
		Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258 L_44;
		L_44 = List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321(L_43, List_1_GetEnumerator_m039302BD172C3288503DB73B6E2B27B8D8BC0321_RuntimeMethod_var);
		V_3 = L_44;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0103:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC((&V_3), Enumerator_Dispose_m2A96F62698864FA1E73292450EBF2019F7104BBC_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00f8_1;
			}

IL_00d5_1:
			{
				// foreach (var coord in b.bldShape) {
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_45;
				L_45 = Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_inline((&V_3), Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_RuntimeMethod_var);
				V_4 = L_45;
				// bldMap[new Vector2Int(x, y) + coord] = b;
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_46 = __this->___bldMap_35;
				int32_t L_47 = ___0_x;
				int32_t L_48 = ___1_y;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_49;
				memset((&L_49), 0, sizeof(L_49));
				Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_49), L_47, L_48, /*hidden argument*/NULL);
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_50 = V_4;
				Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_51;
				L_51 = Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline(L_49, L_50, NULL);
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_52 = V_1;
				Dictionary_2_set_Item_m18C98B2C5838EFF440390391FFCEB7AE47933032(L_46, L_51, L_52, Dictionary_2_set_Item_m18C98B2C5838EFF440390391FFCEB7AE47933032_RuntimeMethod_var);
			}

IL_00f8_1:
			{
				// foreach (var coord in b.bldShape) {
				bool L_53;
				L_53 = Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5((&V_3), Enumerator_MoveNext_mD6D16710D40F62D081A4973E4D8CA1614D1482B5_RuntimeMethod_var);
				if (L_53)
				{
					goto IL_00d5_1;
				}
			}
			{
				goto IL_0111;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0111:
	{
		// resPerSUpdate();
		World_resPerSUpdate_mA57CE2A19904E03F3464CE96E6F429791806D772(__this, NULL);
		goto IL_012f;
	}

IL_0119:
	{
		// print("not enough resources");
		MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(_stringLiteralBDD9E455CEE99D40D2BEDCEB2AE73A3D2753B9CD, NULL);
		goto IL_012f;
	}

IL_0125:
	{
		// print("nope");
		MonoBehaviour_print_m9E6FF71C673B651F35DD418C293CFC50C46803B6(_stringLiteral66AB9EE3DACF3D9819CA98FB792066831612BC34, NULL);
	}

IL_012f:
	{
		// return obj;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_54 = V_0;
		return L_54;
	}
}
// System.Void World::resPerSUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_resPerSUpdate_mA57CE2A19904E03F3464CE96E6F429791806D772 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mAEDD6BBEE1B37BC5E1D803803352FBE4CF4D3D7E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral42068B0B535BEE0AD0CBD0E4D92D9B191EDBB05B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC V_0;
	memset((&V_0), 0, sizeof(V_0));
	Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* V_1 = NULL;
	{
		// waterPerS = 2;
		__this->___waterPerS_7 = 2;
		// nitrogenPerS = 2;
		__this->___nitrogenPerS_8 = 2;
		// foreach (var b in bldList) {
		List_1_t3454872F75CE0819AC9481FA8D61EBEEE0E65100* L_0 = __this->___bldList_31;
		Enumerator_tB001C6682F66AC73B211EF972E38B4DC813220CC L_1;
		L_1 = List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C(L_0, List_1_GetEnumerator_m823E4340B66BA23B3D76D3A6AA50D037439DD26C_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0090:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C((&V_0), Enumerator_Dispose_m85DD232F0CA8B1E49200BC0E47B010153209399C_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0085_1;
			}

IL_001c_1:
			{
				// foreach (var b in bldList) {
				Building_t950D5394E080624D7E96B158EF852EA16ADB3650* L_2;
				L_2 = Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_inline((&V_0), Enumerator_get_Current_mFC1CA3AF29F29127B989FE165C49D6896D4581D4_RuntimeMethod_var);
				// var res = b.getResourceGeneration(bgMap, oreMap, bldMap);
				Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_3 = __this->___bgMap_33;
				Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_4 = __this->___oreMap_34;
				Dictionary_2_t9E3976EB50A9D5D477F3AE300911CE7139EAC1D6* L_5 = __this->___bldMap_35;
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_6;
				L_6 = Building_getResourceGeneration_m0FF5325BAB3A3792F7B2D5F4FFDF33F19C818EA7(L_2, L_3, L_4, L_5, NULL);
				V_1 = L_6;
				// if (res.ContainsKey("water")) { waterPerS += res["water"]; }
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_7 = V_1;
				bool L_8;
				L_8 = Dictionary_2_ContainsKey_mAEDD6BBEE1B37BC5E1D803803352FBE4CF4D3D7E(L_7, _stringLiteral42068B0B535BEE0AD0CBD0E4D92D9B191EDBB05B, Dictionary_2_ContainsKey_mAEDD6BBEE1B37BC5E1D803803352FBE4CF4D3D7E_RuntimeMethod_var);
				if (!L_8)
				{
					goto IL_0060_1;
				}
			}
			{
				// if (res.ContainsKey("water")) { waterPerS += res["water"]; }
				int32_t L_9 = __this->___waterPerS_7;
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_10 = V_1;
				int32_t L_11;
				L_11 = Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592(L_10, _stringLiteral42068B0B535BEE0AD0CBD0E4D92D9B191EDBB05B, Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592_RuntimeMethod_var);
				__this->___waterPerS_7 = ((int32_t)il2cpp_codegen_add(L_9, L_11));
			}

IL_0060_1:
			{
				// if (res.ContainsKey("n")) { nitrogenPerS += res["n"]; }
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_12 = V_1;
				bool L_13;
				L_13 = Dictionary_2_ContainsKey_mAEDD6BBEE1B37BC5E1D803803352FBE4CF4D3D7E(L_12, _stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6, Dictionary_2_ContainsKey_mAEDD6BBEE1B37BC5E1D803803352FBE4CF4D3D7E_RuntimeMethod_var);
				if (!L_13)
				{
					goto IL_0085_1;
				}
			}
			{
				// if (res.ContainsKey("n")) { nitrogenPerS += res["n"]; }
				int32_t L_14 = __this->___nitrogenPerS_8;
				Dictionary_2_t5C8F46F5D57502270DD9E1DA8303B23C7FE85588* L_15 = V_1;
				int32_t L_16;
				L_16 = Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592(L_15, _stringLiteral4C9ECEDF5B1FB9420A92A5B02A141FADFDF52ED6, Dictionary_2_get_Item_mE01750B88D9A581E9196F3444952F7592E862592_RuntimeMethod_var);
				__this->___nitrogenPerS_8 = ((int32_t)il2cpp_codegen_add(L_14, L_16));
			}

IL_0085_1:
			{
				// foreach (var b in bldList) {
				bool L_17;
				L_17 = Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3((&V_0), Enumerator_MoveNext_m992AFCDC194D018EC10B7F1BB98C3ACF4B1B15A3_RuntimeMethod_var);
				if (L_17)
				{
					goto IL_001c_1;
				}
			}
			{
				goto IL_009e;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_009e:
	{
		// uiManager.updateResources();
		UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* L_18 = __this->___uiManager_4;
		UIManager_updateResources_m1EBF6B83B71F6700912C6146A2E9828CC918D496(L_18, NULL);
		// }
		return;
	}
}
// System.Void World::resourceUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_resourceUpdate_m0F5E90EDEDF3CAE7AABBBBB8E96E303D18D34ADB (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	{
		// water += waterPerS;
		int32_t L_0 = __this->___water_5;
		int32_t L_1 = __this->___waterPerS_7;
		__this->___water_5 = ((int32_t)il2cpp_codegen_add(L_0, L_1));
		// nitrogen += nitrogenPerS;
		int32_t L_2 = __this->___nitrogen_6;
		int32_t L_3 = __this->___nitrogenPerS_8;
		__this->___nitrogen_6 = ((int32_t)il2cpp_codegen_add(L_2, L_3));
		// uiManager.updateResources();
		UIManager_t16825A2483574F37D7D47AB939A6FA639678B1F3* L_4 = __this->___uiManager_4;
		UIManager_updateResources_m1EBF6B83B71F6700912C6146A2E9828CC918D496(L_4, NULL);
		// }
		return;
	}
}
// System.Void World::perlinTest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_perlinTest_m2D0B92E83071F3D3F565BAD57AB456C2839199D2 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		// for (int iy = 0; iy < screenGridSize.y; iy++) {
		V_0 = 0;
		goto IL_0087;
	}

IL_0007:
	{
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		V_1 = 0;
		goto IL_0075;
	}

IL_000b:
	{
		// Transform obj = backgroundParent.GetChild(iy * screenGridSize.x + ix);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = __this->___backgroundParent_11;
		int32_t L_1 = V_0;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_2 = (&__this->___screenGridSize_9);
		int32_t L_3;
		L_3 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_2, NULL);
		int32_t L_4 = V_1;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5;
		L_5 = Transform_GetChild_mE686DF0C7AAC1F7AEF356967B1C04D8B8E240EAF(L_0, ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_1, L_3)), L_4)), NULL);
		// float noise = Mathf.PerlinNoise((float) ix * testPerlinScale.x + testPerlinOffset.x, (float) iy * testPerlinScale.y + testPerlinOffset.y);
		int32_t L_6 = V_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_7 = (&__this->___testPerlinScale_37);
		float L_8 = L_7->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_9 = (&__this->___testPerlinOffset_36);
		float L_10 = L_9->___x_0;
		int32_t L_11 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_12 = (&__this->___testPerlinScale_37);
		float L_13 = L_12->___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_14 = (&__this->___testPerlinOffset_36);
		float L_15 = L_14->___y_1;
		float L_16;
		L_16 = Mathf_PerlinNoise_mAB0E53C29FE95469CF303364910AD0D8662A9A6A(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)L_6), L_8)), L_10)), ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)L_11), L_13)), L_15)), NULL);
		V_2 = L_16;
		// obj.GetComponent<SpriteRenderer>().color = new Color(noise, noise, noise);
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_17;
		L_17 = Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45(L_5, Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		float L_18 = V_2;
		float L_19 = V_2;
		float L_20 = V_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_21;
		memset((&L_21), 0, sizeof(L_21));
		Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline((&L_21), L_18, L_19, L_20, /*hidden argument*/NULL);
		SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546(L_17, L_21, NULL);
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		int32_t L_22 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_22, 1));
	}

IL_0075:
	{
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		int32_t L_23 = V_1;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_24 = (&__this->___screenGridSize_9);
		int32_t L_25;
		L_25 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_24, NULL);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_000b;
		}
	}
	{
		// for (int iy = 0; iy < screenGridSize.y; iy++) {
		int32_t L_26 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_26, 1));
	}

IL_0087:
	{
		// for (int iy = 0; iy < screenGridSize.y; iy++) {
		int32_t L_27 = V_0;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_28 = (&__this->___screenGridSize_9);
		int32_t L_29;
		L_29 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_28, NULL);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Void World::buildBackground()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_buildBackground_m9572A9FC3188862D4B440E02BA3F338A472B3B04 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// foreach (Transform child in backgroundParent) {
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = __this->___backgroundParent_11;
		RuntimeObject* L_1;
		L_1 = Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404(L_0, NULL);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_002d:
			{// begin finally (depth: 1)
				{
					RuntimeObject* L_2 = V_0;
					V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_2, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var));
					RuntimeObject* L_3 = V_1;
					if (!L_3)
					{
						goto IL_003d;
					}
				}
				{
					RuntimeObject* L_4 = V_1;
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_4);
				}

IL_003d:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0023_1;
			}

IL_000e_1:
			{
				// foreach (Transform child in backgroundParent) {
				RuntimeObject* L_5 = V_0;
				RuntimeObject* L_6;
				L_6 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_5);
				// Destroy(child.gameObject);
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
				L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(((Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*)CastclassClass((RuntimeObject*)L_6, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var)), NULL);
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(L_7, NULL);
			}

IL_0023_1:
			{
				// foreach (Transform child in backgroundParent) {
				RuntimeObject* L_8 = V_0;
				bool L_9;
				L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_8);
				if (L_9)
				{
					goto IL_000e_1;
				}
			}
			{
				goto IL_003e;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003e:
	{
		// foreach (Transform child in oresParent) {
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_10 = __this->___oresParent_12;
		RuntimeObject* L_11;
		L_11 = Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404(L_10, NULL);
		V_0 = L_11;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_006b:
			{// begin finally (depth: 1)
				{
					RuntimeObject* L_12 = V_0;
					V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_12, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var));
					RuntimeObject* L_13 = V_1;
					if (!L_13)
					{
						goto IL_007b;
					}
				}
				{
					RuntimeObject* L_14 = V_1;
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_14);
				}

IL_007b:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0061_1;
			}

IL_004c_1:
			{
				// foreach (Transform child in oresParent) {
				RuntimeObject* L_15 = V_0;
				RuntimeObject* L_16;
				L_16 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_15);
				// Destroy(child.gameObject);
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_17;
				L_17 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(((Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*)CastclassClass((RuntimeObject*)L_16, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var)), NULL);
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(L_17, NULL);
			}

IL_0061_1:
			{
				// foreach (Transform child in oresParent) {
				RuntimeObject* L_18 = V_0;
				bool L_19;
				L_19 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_18);
				if (L_19)
				{
					goto IL_004c_1;
				}
			}
			{
				goto IL_007c;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_007c:
	{
		// for (int iy = 0; iy < screenGridSize.y; iy++) {
		V_2 = 0;
		goto IL_00a2;
	}

IL_0080:
	{
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		V_3 = 0;
		goto IL_0090;
	}

IL_0084:
	{
		// placeBackgroundTile(ix, iy);
		int32_t L_20 = V_3;
		int32_t L_21 = V_2;
		World_placeBackgroundTile_m60AD5694F95AB3385104B0A362D170FA08110CAE(__this, L_20, L_21, NULL);
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		int32_t L_22 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_22, 1));
	}

IL_0090:
	{
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		int32_t L_23 = V_3;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_24 = (&__this->___screenGridSize_9);
		int32_t L_25;
		L_25 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_24, NULL);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_0084;
		}
	}
	{
		// for (int iy = 0; iy < screenGridSize.y; iy++) {
		int32_t L_26 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_26, 1));
	}

IL_00a2:
	{
		// for (int iy = 0; iy < screenGridSize.y; iy++) {
		int32_t L_27 = V_2;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_28 = (&__this->___screenGridSize_9);
		int32_t L_29;
		L_29 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_28, NULL);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_0080;
		}
	}
	{
		// lastBgLayerDrawn = screenGridSize.y;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_30 = (&__this->___screenGridSize_9);
		int32_t L_31;
		L_31 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_30, NULL);
		__this->___lastBgLayerDrawn_15 = L_31;
		// }
		return;
	}
}
// System.Void World::placeBackgroundTile(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_placeBackgroundTile_m60AD5694F95AB3385104B0A362D170FA08110CAE (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mCC2518456C91884B2443C580C017215822081CC7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_0 = NULL;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* V_3 = NULL;
	{
		// GameObject obj = placeTile(x, y, dirtTile, backgroundParent);
		int32_t L_0 = ___0_x;
		int32_t L_1 = ___1_y;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___dirtTile_27;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3 = __this->___backgroundParent_11;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = World_placeTile_m52587F1851930889B959CBB6053600F82237A511(__this, L_0, L_1, L_2, L_3, NULL);
		V_0 = L_4;
		// bgMap[new Vector2Int(x, y)] = obj.GetComponent<Resource>();
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_5 = __this->___bgMap_33;
		int32_t L_6 = ___0_x;
		int32_t L_7 = ___1_y;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_8), L_6, L_7, /*hidden argument*/NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = V_0;
		Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_10;
		L_10 = GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59(L_9, GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59_RuntimeMethod_var);
		Dictionary_2_set_Item_mCC2518456C91884B2443C580C017215822081CC7(L_5, L_8, L_10, Dictionary_2_set_Item_mCC2518456C91884B2443C580C017215822081CC7_RuntimeMethod_var);
		// float noise = Mathf.PerlinNoise((float) x * bgPerlinScale.x + bgPerlinOffset.x, (float) y * bgPerlinScale.y + bgPerlinOffset.y);
		int32_t L_11 = ___0_x;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_12 = (&__this->___bgPerlinScale_17);
		float L_13 = L_12->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_14 = (&__this->___bgPerlinOffset_22);
		float L_15 = L_14->___x_0;
		int32_t L_16 = ___1_y;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_17 = (&__this->___bgPerlinScale_17);
		float L_18 = L_17->___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_19 = (&__this->___bgPerlinOffset_22);
		float L_20 = L_19->___y_1;
		float L_21;
		L_21 = Mathf_PerlinNoise_mAB0E53C29FE95469CF303364910AD0D8662A9A6A(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)L_11), L_13)), L_15)), ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)L_16), L_18)), L_20)), NULL);
		// int resource = (int) (noise * 4.99);
		V_1 = il2cpp_codegen_cast_double_to_int<int32_t>(((double)il2cpp_codegen_multiply(((double)L_21), (4.9900000000000002))));
		// obj.GetComponent<Resource>().stored = resource;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = V_0;
		Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_23;
		L_23 = GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59(L_22, GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59_RuntimeMethod_var);
		int32_t L_24 = V_1;
		L_23->___stored_5 = L_24;
		// float opacity = 1 - ((float) resource / 4) * 0.4f;
		int32_t L_25 = V_1;
		V_2 = ((float)il2cpp_codegen_subtract((1.0f), ((float)il2cpp_codegen_multiply(((float)(((float)L_25)/(4.0f))), (0.400000006f)))));
		// obj.GetComponent<SpriteRenderer>().color = new Color(opacity, opacity, opacity);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26 = V_0;
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_27;
		L_27 = GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9(L_26, GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var);
		float L_28 = V_2;
		float L_29 = V_2;
		float L_30 = V_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline((&L_31), L_28, L_29, L_30, /*hidden argument*/NULL);
		SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546(L_27, L_31, NULL);
		// noise = Mathf.PerlinNoise((float) x * oreNPerlinScale.x + oreNPerlinOffset.x, (float) y * oreNPerlinScale.y + oreNPerlinOffset.y);
		int32_t L_32 = ___0_x;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_33 = (&__this->___oreNPerlinScale_18);
		float L_34 = L_33->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_35 = (&__this->___oreNPerlinOffset_23);
		float L_36 = L_35->___x_0;
		int32_t L_37 = ___1_y;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_38 = (&__this->___oreNPerlinScale_18);
		float L_39 = L_38->___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_40 = (&__this->___oreNPerlinOffset_23);
		float L_41 = L_40->___y_1;
		float L_42;
		L_42 = Mathf_PerlinNoise_mAB0E53C29FE95469CF303364910AD0D8662A9A6A(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)L_32), L_34)), L_36)), ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)L_37), L_39)), L_41)), NULL);
		// if (noise >= oreNtreshold) {
		float L_43 = __this->___oreNtreshold_21;
		if ((!(((float)L_42) >= ((float)L_43))))
		{
			goto IL_012c;
		}
	}
	{
		// obj = placeTile(x, y, oreNTile, oresParent);
		int32_t L_44 = ___0_x;
		int32_t L_45 = ___1_y;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_46 = __this->___oreNTile_28;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_47 = __this->___oresParent_12;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_48;
		L_48 = World_placeTile_m52587F1851930889B959CBB6053600F82237A511(__this, L_44, L_45, L_46, L_47, NULL);
		V_0 = L_48;
		// var r = obj.GetComponent<Resource>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_49 = V_0;
		Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_50;
		L_50 = GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59(L_49, GameObject_GetComponent_TisResource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C_m9C1A5D298598558BE909A90858820C19124C5E59_RuntimeMethod_var);
		V_3 = L_50;
		// r.stored = Random.Range(500, 1000);
		Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_51 = V_3;
		int32_t L_52;
		L_52 = Random_Range_m6763D9767F033357F88B6637F048F4ACA4123B68(((int32_t)500), ((int32_t)1000), NULL);
		L_51->___stored_5 = L_52;
		// oreMap[new Vector2Int(x, y)] = r;
		Dictionary_2_tC11C5ACB0B1BA749F818EC1CE3BB893851C3E1EB* L_53 = __this->___oreMap_34;
		int32_t L_54 = ___0_x;
		int32_t L_55 = ___1_y;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_56;
		memset((&L_56), 0, sizeof(L_56));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_56), L_54, L_55, /*hidden argument*/NULL);
		Resource_t52FBFFEBF62DCCCFBCAB94BE3316B88569FE8C2C* L_57 = V_3;
		Dictionary_2_set_Item_mCC2518456C91884B2443C580C017215822081CC7(L_53, L_56, L_57, Dictionary_2_set_Item_mCC2518456C91884B2443C580C017215822081CC7_RuntimeMethod_var);
	}

IL_012c:
	{
		// }
		return;
	}
}
// System.Void World::generateMoreBackground(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_generateMoreBackground_m3D1A2A3203DFECB880EB4934579E241F8EBC30A0 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, float ___0_cameraOffset, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// int maxVisibleLayer = screenGridSize.y + (int) Mathf.Ceil(cameraOffset);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_0 = (&__this->___screenGridSize_9);
		int32_t L_1;
		L_1 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_0, NULL);
		float L_2 = ___0_cameraOffset;
		float L_3;
		L_3 = ceilf(L_2);
		V_0 = ((int32_t)il2cpp_codegen_add(L_1, il2cpp_codegen_cast_double_to_int<int32_t>(L_3)));
		// int layersToRender = maxVisibleLayer - lastBgLayerDrawn;
		int32_t L_4 = V_0;
		int32_t L_5 = __this->___lastBgLayerDrawn_15;
		V_1 = ((int32_t)il2cpp_codegen_subtract(L_4, L_5));
		// if (layersToRender > 0) {
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		// for (int iy = 0; iy < layersToRender; iy++) {
		V_2 = 0;
		goto IL_004e;
	}

IL_0025:
	{
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		V_3 = 0;
		goto IL_003c;
	}

IL_0029:
	{
		// placeBackgroundTile(ix, lastBgLayerDrawn + iy);
		int32_t L_7 = V_3;
		int32_t L_8 = __this->___lastBgLayerDrawn_15;
		int32_t L_9 = V_2;
		World_placeBackgroundTile_m60AD5694F95AB3385104B0A362D170FA08110CAE(__this, L_7, ((int32_t)il2cpp_codegen_add(L_8, L_9)), NULL);
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		int32_t L_10 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_10, 1));
	}

IL_003c:
	{
		// for (int ix = 0; ix < screenGridSize.x; ix++) {
		int32_t L_11 = V_3;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_12 = (&__this->___screenGridSize_9);
		int32_t L_13;
		L_13 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_12, NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0029;
		}
	}
	{
		// for (int iy = 0; iy < layersToRender; iy++) {
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_004e:
	{
		// for (int iy = 0; iy < layersToRender; iy++) {
		int32_t L_15 = V_2;
		int32_t L_16 = V_1;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0025;
		}
	}
	{
		// lastBgLayerDrawn = maxVisibleLayer;
		int32_t L_17 = V_0;
		__this->___lastBgLayerDrawn_15 = L_17;
	}

IL_0059:
	{
		// }
		return;
	}
}
// System.Void World::moveCursor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World_moveCursor_mC133A148D958D0EC03ECE8A6BD12745873CCC3F1 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, int32_t ___0_x, int32_t ___1_y, const RuntimeMethod* method) 
{
	{
		// y = -y;
		int32_t L_0 = ___1_y;
		___1_y = ((-L_0));
		// cursor.transform.localPosition = new Vector2(x, y);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___cursor_10;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2;
		L_2 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_1, NULL);
		int32_t L_3 = ___0_x;
		int32_t L_4 = ___1_y;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_5), ((float)L_3), ((float)L_4), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline(L_5, NULL);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_2, L_6, NULL);
		// }
		return;
	}
}
// System.Void World::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void World__ctor_mAFAEE7FBF3A190572FB70B6BFCFEEC8655B5E354 (World_t7E641366D3E68AEADB5A0B3409E31C580A24BC6A* __this, const RuntimeMethod* method) 
{
	{
		// public int nitrogen = 100;
		__this->___nitrogen_6 = ((int32_t)100);
		// public float enemySpawnChance = 0.9f;
		__this->___enemySpawnChance_39 = (0.899999976f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Implicit_mE8EBEE9291F11BB02F062D6E000F4798968CBD96_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_v, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_v;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___0_v;
		float L_3 = L_2.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Distance_mBACBB1609E1894D68F882D86A93519E311810C89_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_a;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___1_b;
		float L_3 = L_2.___x_0;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___0_a;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_b;
		float L_7 = L_6.___y_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_12;
		L_12 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_8, L_9)), ((float)il2cpp_codegen_multiply(L_10, L_11))))));
		V_2 = ((float)L_12);
		goto IL_002e;
	}

IL_002e:
	{
		float L_13 = V_2;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, int32_t ___0_x, int32_t ___1_y, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_x;
		__this->___m_X_0 = L_0;
		int32_t L_1 = ___1_y;
		__this->___m_Y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A Vector2Int_op_Addition_m6358133A28BA913D2080FD44472D1FD1CE1AC28F_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_a, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___1_b, const RuntimeMethod* method) 
{
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0;
		L_0 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline((&___0_a), NULL);
		int32_t L_1;
		L_1 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline((&___1_b), NULL);
		int32_t L_2;
		L_2 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline((&___0_a), NULL);
		int32_t L_3;
		L_3 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline((&___1_b), NULL);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_4), ((int32_t)il2cpp_codegen_add(L_0, L_1)), ((int32_t)il2cpp_codegen_add(L_2, L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0027;
	}

IL_0027:
	{
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___m_X_0;
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___m_Y_1;
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_0 = L_0;
		float L_1 = ___1_y;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_a;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___1_b;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___0_a;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_b;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Subtraction_m44475FCDAD2DA2F98D78A6625EC2DCDFE8803837_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_a;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___1_b;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___0_a;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_b;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_subtract(L_1, L_3)), ((float)il2cpp_codegen_subtract(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_SignedAngle_mAE9940DA6BC6B2182BA95C299B2EC19967B7D438_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_from, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_to, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_from;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___1_to;
		float L_2;
		L_2 = Vector2_Angle_mD94AAEA690169FE5882D60F8489C8BF63300C221_inline(L_0, L_1, NULL);
		V_0 = L_2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___0_from;
		float L_4 = L_3.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = ___1_to;
		float L_6 = L_5.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = ___0_from;
		float L_8 = L_7.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = ___1_to;
		float L_10 = L_9.___x_0;
		float L_11;
		L_11 = Mathf_Sign_m42EE1F0BC041AF14F89DED7F762BE996E2C50D8A_inline(((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_4, L_6)), ((float)il2cpp_codegen_multiply(L_8, L_10)))), NULL);
		V_1 = L_11;
		float L_12 = V_0;
		float L_13 = V_1;
		V_2 = ((float)il2cpp_codegen_multiply(L_12, L_13));
		goto IL_0030;
	}

IL_0030:
	{
		float L_14 = V_2;
		return L_14;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m9262AB29E3E9CE94EF71051F38A28E82AEC73F90_inline (float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___0_x;
		float L_1 = ___1_y;
		float L_2 = ___2_z;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_3, (0.0174532924f), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_Internal_FromEulerRad_m66D4475341F53949471E6870FB5C5E4A5E9BA93E(L_4, NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		float L_2 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___0_a;
		float L_4 = L_3.___y_3;
		float L_5 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_a;
		float L_7 = L_6.___z_4;
		float L_8 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_b;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_a;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_b;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_a;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_b;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), ((float)il2cpp_codegen_add(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_SmoothDamp_m305987D454BC6672AC765DB56A2843D4951865AD_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_current, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_target, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___2_currentVelocity, float ___3_smoothTime, float ___4_maxSpeed, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		float L_0;
		L_0 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		V_0 = L_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___0_current;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___1_target;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_3 = ___2_currentVelocity;
		float L_4 = ___3_smoothTime;
		float L_5 = ___4_maxSpeed;
		float L_6 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7;
		L_7 = Vector2_SmoothDamp_m6294700C7D9CDEACDB21858E25AC703B92CED4CC(L_1, L_2, L_3, L_4, L_5, L_6, NULL);
		V_1 = L_7;
		goto IL_0016;
	}

IL_0016:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8 = V_1;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_v, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_v;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___0_v;
		float L_3 = L_2.___y_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_RotateTowards_m50EF9D609C80CD423CDA856EA3481DE2004633A3_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_from, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_to, float ___2_maxDegreesDelta, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_from;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = ___1_to;
		float L_2;
		L_2 = Quaternion_Angle_mAADDBB3C30736B4C7B75CF3A241C1CF5E0386C26_inline(L_0, L_1, NULL);
		V_0 = L_2;
		float L_3 = V_0;
		V_1 = (bool)((((float)L_3) == ((float)(0.0f)))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0019;
		}
	}
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5 = ___1_to;
		V_2 = L_5;
		goto IL_0030;
	}

IL_0019:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = ___0_from;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_7 = ___1_to;
		float L_8 = ___2_maxDegreesDelta;
		float L_9 = V_0;
		float L_10;
		L_10 = Mathf_Min_m747CA71A9483CDB394B13BD0AD048EE17E48FFE4_inline((1.0f), ((float)(L_8/L_9)), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_11;
		L_11 = Quaternion_SlerpUnclamped_mAE7F4DF2F239831CCAA1DFB52F313E5AED52D32D(L_6, L_7, L_10, NULL);
		V_2 = L_11;
		goto IL_0030;
	}

IL_0030:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_12 = V_2;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mBEA93B5A0E954FEFB863DC61CB209119980EC713_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_lhs;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___1_rhs;
		bool L_2;
		L_2 = Vector2_op_Equality_m6F2E069A50E787D131261E5CB25FC9E03F95B5E1_inline(L_0, L_1, NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Abs_mD945EDDEA0D62D21BFDBAB7B1C0F18DFF1CEC905_inline (int32_t ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___0_value;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = il2cpp_codegen_abs(L_0);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int_set_y_mF81881204EEE272BA409728C7EBFDE3A979DDF6A_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___m_Y_1 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_2 = L_0;
		float L_1 = ___1_y;
		__this->___y_3 = L_1;
		float L_2 = ___2_z;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ((Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var))->___identityQuaternion_4;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m5F87930F9B0828E5652E2D9D01ED907C01122C86_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_2 = L_0;
		float L_1 = ___1_y;
		__this->___y_3 = L_1;
		__this->___z_4 = (0.0f);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___0_a;
		int32_t L_1 = ___1_b;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_2 = ___1_b;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_3 = ___0_a;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_r;
		__this->___r_0 = L_0;
		float L_1 = ___1_g;
		__this->___g_1 = L_1;
		float L_2 = ___2_b;
		__this->___b_2 = L_2;
		__this->___a_3 = (1.0f);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A Enumerator_get_Current_m87245A61572727EBCD1642C4A2BD99B11CE9FA8A_gshared_inline (Enumerator_t4BD1B5394A2E9960562CC364C2CCE4FCBD1AC258* __this, const RuntimeMethod* method) 
{
	{
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_0 = (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m771AC7A01DFC931CCCFCCF949C1F4D56B5E98A1B_gshared_inline (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___0_item, const RuntimeMethod* method) 
{
	Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534* L_1 = (Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534* L_4 = V_0;
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		Vector2IntU5BU5D_tF9E2BDAC11B246DF7EEB9137B826A0CBEBD59534* L_6 = V_0;
		int32_t L_7 = V_1;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_8 = ___0_item;
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7), (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A)L_8);
		return;
	}

IL_0034:
	{
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_9 = ___0_item;
		((  void (*) (List_1_tB56F1028A724D2CE4E84861619D1CF68C68C983D*, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mBE60E91E0A30EA49AB81231986E0D5C3A5A94B98_gshared_inline (Enumerator_tB8107E6A622783453A40DD6097AB6AF6D76BEDC2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____currentValue_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___0_item;
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___0_item;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Angle_mD94AAEA690169FE5882D60F8489C8BF63300C221_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_from, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_to, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	float V_3 = 0.0f;
	{
		float L_0;
		L_0 = Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline((&___0_from), NULL);
		float L_1;
		L_1 = Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline((&___1_to), NULL);
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_2;
		L_2 = sqrt(((double)((float)il2cpp_codegen_multiply(L_0, L_1))));
		V_0 = ((float)L_2);
		float L_3 = V_0;
		V_2 = (bool)((((float)L_3) < ((float)(1.0E-15f)))? 1 : 0);
		bool L_4 = V_2;
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		V_3 = (0.0f);
		goto IL_0056;
	}

IL_002c:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = ___0_from;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_to;
		float L_7;
		L_7 = Vector2_Dot_mC1E68FDB4FB462A279A303C043B8FD0AC11C8458_inline(L_5, L_6, NULL);
		float L_8 = V_0;
		float L_9;
		L_9 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(((float)(L_7/L_8)), (-1.0f), (1.0f), NULL);
		V_1 = L_9;
		float L_10 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_11;
		L_11 = acos(((double)L_10));
		V_3 = ((float)il2cpp_codegen_multiply(((float)L_11), (57.2957802f)));
		goto IL_0056;
	}

IL_0056:
	{
		float L_12 = V_3;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Sign_m42EE1F0BC041AF14F89DED7F762BE996E2C50D8A_inline (float ___0_f, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___0_f;
		if ((((float)L_0) >= ((float)(0.0f))))
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = (-1.0f);
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = (1.0f);
	}

IL_0015:
	{
		V_0 = G_B3_0;
		goto IL_0018;
	}

IL_0018:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Quaternion_Angle_mAADDBB3C30736B4C7B75CF3A241C1CF5E0386C26_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_a, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_b, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_a;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = ___1_b;
		float L_2;
		L_2 = Quaternion_Dot_mF9D3BE33940A47979DADA7E81650AEB356D5D12B_inline(L_0, L_1, NULL);
		float L_3;
		L_3 = fabsf(L_2);
		float L_4;
		L_4 = Mathf_Min_m747CA71A9483CDB394B13BD0AD048EE17E48FFE4_inline(L_3, (1.0f), NULL);
		V_0 = L_4;
		float L_5 = V_0;
		bool L_6;
		L_6 = Quaternion_IsEqualUsingDot_m9C672201C918C2D1E739F559DBE4406F95997CBD_inline(L_5, NULL);
		if (L_6)
		{
			goto IL_0034;
		}
	}
	{
		float L_7 = V_0;
		float L_8;
		L_8 = acosf(L_7);
		G_B3_0 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_8, (2.0f))), (57.2957802f)));
		goto IL_0039;
	}

IL_0034:
	{
		G_B3_0 = (0.0f);
	}

IL_0039:
	{
		V_1 = G_B3_0;
		goto IL_003c;
	}

IL_003c:
	{
		float L_9 = V_1;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Min_m747CA71A9483CDB394B13BD0AD048EE17E48FFE4_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___0_a;
		float L_1 = ___1_b;
		if ((((float)L_0) < ((float)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		float L_2 = ___1_b;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		float L_3 = ___0_a;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m6F2E069A50E787D131261E5CB25FC9E03F95B5E1_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_lhs;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___1_rhs;
		float L_3 = L_2.___x_0;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___0_lhs;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_rhs;
		float L_7 = L_6.___y_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		V_2 = (bool)((((float)((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_8, L_9)), ((float)il2cpp_codegen_multiply(L_10, L_11))))) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_002e;
	}

IL_002e:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->___x_0;
		float L_1 = __this->___x_0;
		float L_2 = __this->___y_1;
		float L_3 = __this->___y_1;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_0, L_1)), ((float)il2cpp_codegen_multiply(L_2, L_3))));
		goto IL_001f;
	}

IL_001f:
	{
		float L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Dot_mC1E68FDB4FB462A279A303C043B8FD0AC11C8458_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_lhs;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___1_rhs;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___0_lhs;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_rhs;
		float L_7 = L_6.___y_1;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7))));
		goto IL_001f;
	}

IL_001f:
	{
		float L_8 = V_0;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline (float ___0_value, float ___1_min, float ___2_max, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		float L_0 = ___0_value;
		float L_1 = ___1_min;
		V_0 = (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		float L_3 = ___1_min;
		___0_value = L_3;
		goto IL_0019;
	}

IL_000e:
	{
		float L_4 = ___0_value;
		float L_5 = ___2_max;
		V_1 = (bool)((((float)L_4) > ((float)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0019;
		}
	}
	{
		float L_7 = ___2_max;
		___0_value = L_7;
	}

IL_0019:
	{
		float L_8 = ___0_value;
		V_2 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		float L_9 = V_2;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Quaternion_Dot_mF9D3BE33940A47979DADA7E81650AEB356D5D12B_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_a, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_b, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_a;
		float L_1 = L_0.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_2 = ___1_b;
		float L_3 = L_2.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_4 = ___0_a;
		float L_5 = L_4.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = ___1_b;
		float L_7 = L_6.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_8 = ___0_a;
		float L_9 = L_8.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_10 = ___1_b;
		float L_11 = L_10.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_12 = ___0_a;
		float L_13 = L_12.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_14 = ___1_b;
		float L_15 = L_14.___w_3;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11)))), ((float)il2cpp_codegen_multiply(L_13, L_15))));
		goto IL_003b;
	}

IL_003b:
	{
		float L_16 = V_0;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Quaternion_IsEqualUsingDot_m9C672201C918C2D1E739F559DBE4406F95997CBD_inline (float ___0_dot, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		float L_0 = ___0_dot;
		V_0 = (bool)((((float)L_0) > ((float)(0.999998987f)))? 1 : 0);
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
